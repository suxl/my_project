//package com.boot.proj.rabbitmq.sender;
//
//import com.boot.proj.rabbitmq.MQConst;
//import com.boot.proj.rabbitmq.RabbitMQConfig;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.amqp.rabbit.core.RabbitTemplate;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
///**
// * @author suxiaolin
// * @date 2019/11/12 10:08
// */
//@Service
//@Slf4j
//public class MQSender {
//    @Autowired
//    private RabbitTemplate rabbitTemplate;
//
//
//    public void sendMSG(Object message){
//        log.info("发送消息{}",message);
//
//        // Topic
//        rabbitTemplate.convertAndSend(MQConst.TOPIC_EXCHANGE, MQConst.TOPIC_KEYS, "from keys");
//        rabbitTemplate.convertAndSend(MQConst.TOPIC_EXCHANGE, MQConst.TOPIC_KEY1, "from key1");
//    }
//}
