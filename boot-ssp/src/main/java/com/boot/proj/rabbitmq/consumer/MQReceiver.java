//package com.boot.proj.rabbitmq.consumer;
//
//import com.boot.proj.rabbitmq.MQConst;
//import com.boot.proj.rabbitmq.RabbitMQConfig;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.amqp.rabbit.annotation.RabbitHandler;
//import org.springframework.amqp.rabbit.annotation.RabbitListener;
//import org.springframework.stereotype.Service;
//
///**
// * @author suxiaolin
// * @date 2019/11/12 10:10
// */
//@Service
//@Slf4j
//public class MQReceiver {
//    @RabbitListener(queues = MQConst.TOPIC_QUEUENAME1)
//    @RabbitHandler
//    public void process1(String message) {
//        System.err.println("queue:topic.message1,message:" + message);
//    }
//
//    @RabbitListener(queues = MQConst.TOPIC_QUEUENAME2)
//    @RabbitHandler
//    public void process2(String message) {
//        System.err.println("queue:topic.message2,message:" + message);
//    }
//
//}
