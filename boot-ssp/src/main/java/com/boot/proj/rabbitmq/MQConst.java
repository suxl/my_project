package com.boot.proj.rabbitmq;

/**
 * @author suxiaolin
 * @date 2019/11/12 10:49
 */
public class MQConst {

    // 队列名称
    public final static String TOPIC_QUEUENAME1 = "topic.message1";
    public final static String TOPIC_QUEUENAME2 = "topic.message2";
    // 交换器
    public final static String TOPIC_EXCHANGE = "topicExchange";

    public final static String TOPIC_KEY1 = "topicKey";
    public final static String TOPIC_KEYS = "topicKey.*";


    public final static String FANOUT_QUEUENAME1 = "fanout.messageA";
    public final static String FANOUT_QUEUENAME2 = "fanout.messageB";
    public final static String FANOUT_EXCHANGE = "fanoutExchange";
}
