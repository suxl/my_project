//package com.boot.proj.rabbitmq;
//
//import lombok.Data;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.amqp.core.Binding;
//import org.springframework.amqp.core.BindingBuilder;
//import org.springframework.amqp.core.Queue;
//import org.springframework.amqp.core.TopicExchange;
//import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
//import org.springframework.amqp.rabbit.connection.ConnectionFactory;
//import org.springframework.amqp.rabbit.core.RabbitTemplate;
//import org.springframework.beans.factory.config.ConfigurableBeanFactory;
//import org.springframework.boot.context.properties.ConfigurationProperties;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.Scope;
//
///**
// * Rabbit配置类
// * @author suxiaolin
// * @date 2019/11/12 10:04
// */
//@Configuration
//@ConfigurationProperties(prefix = "spring.rabbitmq")
//@Data
//@Slf4j
//public class RabbitMQConfig {
////    public static final String PAY = "PAY_QUEUE";
//
//    private String host;
//
//    private Integer port;
//
//    private String username;
//
//    private String password;
//
//    @Bean
//    public ConnectionFactory RbmqConnectionFactory(){
//        CachingConnectionFactory connectionFactory = new CachingConnectionFactory(host, port);
//        connectionFactory.setUsername(username);
//        connectionFactory.setPassword(password);
//        return connectionFactory;
//    }
//
//    // spring 中的bean默认是单例 也就是说这个bean只能创建一个实例 （如果这个bean是单例模式的话，则获取bean时都会取唯一的实例）
//    // 如果需要每次获取这个bean都是一个新的实例 需要加上这个注解  @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
//    @Bean
//    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
//    public RabbitTemplate rabbitTemplate(){
//        RabbitTemplate template = new RabbitTemplate(RbmqConnectionFactory());
//        return template;
//    }
//
//    // TOPIC交换机
//    @Bean
//    TopicExchange exchange() {
//        return new TopicExchange(MQConst.TOPIC_EXCHANGE);
//    }
//
//    @Bean
//    public Queue queueMessage1() {
//        // 队列的名称  是否要做队列的持久化
//        return new Queue(MQConst.TOPIC_QUEUENAME1,true);
//    }
//
//    @Bean
//    public Queue queueMessage2() {
//        // 队列的名称  是否要做队列的持久化
//        return new Queue(MQConst.TOPIC_QUEUENAME2,true);
//    }
//
//    //将消息队列注册到相应的交换机上并指定相应的匹配模式
//    @Bean
//    Binding bindingExchangeMessage(Queue queueMessage1, TopicExchange exchange) {
//        // 将队列1绑定到名为topicKey.A的routingKey
//        return BindingBuilder.bind(queueMessage1).to(exchange).with(MQConst.TOPIC_KEY1);
//    }
//
//    @Bean
//    Binding bindingExchangeMessages(Queue queueMessage2, TopicExchange exchange) {
//        // 将队列2绑定到所有topicKey.开头的routingKey
//        return BindingBuilder.bind(queueMessage2).to(exchange).with(MQConst.TOPIC_KEYS);
//    }
//
//
//
//}
