package com.boot.proj.quartz.quartz.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.boot.proj.quartz.quartz.entity.SysQuartzJob;
import org.apache.ibatis.annotations.Mapper;
/**
 * <p>
 * 定时任务 Mapper 接口
 * </p>
 *
 * @author 苏小林
 * @since 2019-05-29
 */
@Mapper
public interface SysQuartzJobMapper extends BaseMapper<SysQuartzJob> {

}
