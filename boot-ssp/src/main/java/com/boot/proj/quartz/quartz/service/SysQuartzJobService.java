package com.boot.proj.quartz.quartz.service;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.boot.proj.commons.base.BaseService;
import com.boot.proj.commons.utils.StringUtils;
import com.boot.proj.quartz.config.QuartzManager;
import com.boot.proj.quartz.quartz.dao.SysQuartzJobMapper;
import com.boot.proj.quartz.quartz.entity.SysQuartzJob;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * 定时任务 服务实现类
 * </p>
 *
 * @author 苏小林
 * @since 2019-05-29
 */
@Service
@Transactional(rollbackFor = Exception.class)
@CacheConfig(cacheNames = "SysQuartzJob")
public class SysQuartzJobService extends BaseService<SysQuartzJobMapper, SysQuartzJob> {

    @Autowired
    QuartzManager quartzManager;

    @Autowired
    private SysQuartzJobMapper dao;

    @Override
    @CacheEvict(cacheNames="SysQuartzJob", allEntries=true)
    public boolean save(SysQuartzJob entity) {
        if (StringUtils.isBlank(entity.getId())){
            entity.setState(1);//停止
        }
        String jobNameEn = entity.getJobNameEn();
        entity.setJobGroupName(jobNameEn+"Group");
        entity.setTriggerName(jobNameEn+"Trigger");
        entity.setTriggerGroupName(jobNameEn+"TriggerGroup");
        return super.save(entity);
    }

    @Override
    @Cacheable(key = "#id",sync = true)
    public SysQuartzJob getById(Serializable id) {
        return super.getById(id);
    }

    @Override
    @CacheEvict(cacheNames="SysQuartzJob", allEntries=true)
    public boolean removeById(Serializable id) {
        return super.removeById(id);
    }

    @Override
    @CacheEvict(cacheNames="SysQuartzJob", allEntries=true)
    public boolean removeByIds(Collection<? extends Serializable> idList) {
        return super.removeByIds(idList);
    }

    @Cacheable(key = "#data",sync = true)
    public IPage<SysQuartzJob> selectPage(Map<String,Object> data) {
        int currentPage=1;
        if (!Objects.isNull(data.get("page"))) {
            currentPage=(Integer) data.get("page");
        }
        int pageSize=20;
        if (!Objects.isNull(data.get("size"))) {
            pageSize=(Integer) data.get("size");
        }

        SysQuartzJob entity = JSON.parseObject(JSON.toJSONString(data), SysQuartzJob.class);
        Page<SysQuartzJob> page = new Page(currentPage,pageSize);
        QueryWrapper<SysQuartzJob> wrapper = new QueryWrapper(entity);
        return super.selectPage(page, wrapper);
    }

    @Override
    @Cacheable(value = "SysQuartzJob",sync = true)
    public List<SysQuartzJob> selectAll() {
        return super.selectAll();
    }

    @CacheEvict(cacheNames="SysQuartzJob", allEntries=true)
    public void switchState(Map<String, String> data) {
        String id = data.get("id");
        String state = data.get("state");
        SysQuartzJob sysQuartzJob = this.getById(id);
        if (Integer.valueOf(state) == 0){//开启
            //添加任务
            quartzManager.addJob(sysQuartzJob);
            sysQuartzJob.setState(0);
            this.updateById(sysQuartzJob);
        }else{//停止
            quartzManager.removeJob(sysQuartzJob);
            sysQuartzJob.setState(1);
            this.updateById(sysQuartzJob);
        }
    }
}
