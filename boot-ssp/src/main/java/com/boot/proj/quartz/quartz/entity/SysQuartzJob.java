package com.boot.proj.quartz.quartz.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.boot.proj.commons.base.BaseEntity;
import com.boot.proj.commons.utils.excel.annotation.ExcelField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * <p>
 * 定时任务
 * </p>
 *
 * @author 苏小林
 * @since 2019-05-29
 */
@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_quartz_job")
@Table(name="sys_quartz_job")
@org.hibernate.annotations.Table(appliesTo = "sys_quartz_job",comment="定时任务")
public class SysQuartzJob extends BaseEntity<SysQuartzJob> {

    /**
     * 任务名
     */
    @TableField("job_name_en")
    @ExcelField(title="任务名英文名", type = ExcelField.TYPE_IMPORT_EXPORT, align= ExcelField.ALIGN_CENTER, sort = 1)
    @Column(columnDefinition = "varchar(20) COMMENT '任务名英文名'")
    private String jobNameEn;
    /**
     * 任务名
     */
    @TableField("job_name_ch")
    @ExcelField(title="任务名中文名", type = ExcelField.TYPE_IMPORT_EXPORT, align= ExcelField.ALIGN_CENTER, sort = 1)
    @Column(columnDefinition = "varchar(20) COMMENT '任务名中文名'")
    private String jobNameCh;
    /**
     * 任务组名
     */
    @TableField("job_group_name")
    @ExcelField(title="任务组名", type = ExcelField.TYPE_IMPORT_EXPORT, align= ExcelField.ALIGN_CENTER, sort = 1)
    @Column(columnDefinition = "varchar(20) COMMENT '任务组名'")
    private String jobGroupName;
    /**
     * 触发器名
     */
    @TableField("trigger_name")
    @ExcelField(title="触发器名", type = ExcelField.TYPE_IMPORT_EXPORT, align= ExcelField.ALIGN_CENTER, sort = 1)
    @Column(columnDefinition = "varchar(20) COMMENT '触发器名'")
    private String triggerName;
    /**
     * 触发器组名
     */
    @TableField("trigger_group_name")
    @ExcelField(title="触发器组名", type = ExcelField.TYPE_IMPORT_EXPORT, align= ExcelField.ALIGN_CENTER, sort = 1)
    @Column(columnDefinition = "varchar(20) COMMENT '触发器组名'")
    private String triggerGroupName;
    /**
     * 任务处理
     */
    @TableField("job_class")
    @ExcelField(title="任务处理", type = ExcelField.TYPE_IMPORT_EXPORT, align= ExcelField.ALIGN_CENTER, sort = 1)
    @Column(columnDefinition = "varchar(100) COMMENT '任务处理'")
    private String jobClass;
    /**
     * cron表达式
     */
    @TableField("cron")
        @ExcelField(title="cron表达式", type = ExcelField.TYPE_IMPORT_EXPORT, align= ExcelField.ALIGN_CENTER, sort = 1)
    @Column(columnDefinition = "varchar(100) COMMENT 'cron表达式'")
    private String cron;
    /**
     * 状态  0 启用 1 停止
     */
    @TableField("state")
    @ExcelField(title="状态", type = ExcelField.TYPE_IMPORT_EXPORT, align= ExcelField.ALIGN_CENTER, sort = 1)
    @Column(columnDefinition = "integer COMMENT '状态'")
    private Integer state;


}
