package com.boot.proj.quartz.handle;

import com.boot.proj.quartz.quartz.entity.SysQuartzJob;
import com.boot.proj.quartz.quartz.service.SysQuartzJobService;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.quartz.QuartzJobBean;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

/**
 * @author suxiaolin
 * @date 2019/5/29 10:32
 * @DisallowConcurrentExecution : 此标记用在实现Job的类上面,意思是不允许并发执行.
 * 注org.quartz.threadPool.threadCount的数量有多个的情况,@DisallowConcurrentExecution才生效
 */
@DisallowConcurrentExecution
public class ButtonTimerJob extends QuartzJobBean {
    private static final Logger logger = LoggerFactory.getLogger(ButtonTimerJob.class);

    @Autowired
    private SysQuartzJobService sysQuartzJobService;

    @Autowired
    private SimpMessagingTemplate template;


    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        JobDataMap jobDataMap = jobExecutionContext.getJobDetail().getJobDataMap();
        String taskId= "";
        if (jobDataMap !=null) {
            Object taskIdObject = jobDataMap.get("taskId");
            if (!Objects.isNull(taskIdObject)) {
                taskId = taskIdObject.toString();
            }
        }

        SysQuartzJob sysQuartzJob = sysQuartzJobService.getById(taskId);
        System.err.println(sysQuartzJob);
        DateTimeFormatter dtf2 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        this.template.convertAndSend("/allMessage/test","好消息,定时任务已经执行。执行时间:"+dtf2.format(LocalDateTime.now()));
        logger.info("--------------定时任务执行逻辑---------------------");
    }
}
