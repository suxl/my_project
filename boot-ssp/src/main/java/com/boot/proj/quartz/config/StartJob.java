package com.boot.proj.quartz.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ContextRefreshedEvent;

/**
 * @author suxiaolin
 * @date 2019/5/29 10:34
 */
@Configuration
public class StartJob implements ApplicationListener<ContextRefreshedEvent> {
    @Autowired
    QuartzManager quartzManager;
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    public void run(){
        logger.info(">> 启动定时任务...");
        quartzManager.startJobs();
//        quartzManager.removeJob(
//                "SpecialPeriodJob",
//                "SpecialPeriodJobGroup",
//                "SpecialPeriodTrigger",
//                "SpecialPeriodTriggerGroup");

//        QuartzManager.startJobs();
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        System.out.println("启动定时任务......");
        run();
    }
}
