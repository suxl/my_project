package com.boot.proj.quartz.quartz.web;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.boot.proj.commons.utils.DateUtils;
import com.boot.proj.commons.utils.Result;
import com.boot.proj.commons.utils.excel.ExportExcel;
import com.boot.proj.quartz.quartz.entity.SysQuartzJob;
import com.boot.proj.quartz.quartz.service.SysQuartzJobService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * <p>
 * 定时任务 前端控制器
 * </p>
 *
 * @author 苏小林
 * @since 2019-05-29
 */
@RestController
@RequestMapping("/quartz/sysQuartzJob")
public class SysQuartzJobController {

	@Autowired
    public SysQuartzJobService service;




	@ApiOperation(value = "定时任务,保存/修改", notes = "定时任务,保存和修改方法")
	@RequestMapping(value = "/save", method = RequestMethod.POST)
    @PreAuthorize("hasAuthority('sys:quartz:job:add')")
	public Result save(@RequestBody SysQuartzJob data){
    	try {
    		service.save(data);
        	return new Result<>(Result.CODE_SUCCESS,"保存成功");
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
        	return new Result<>(Result.CODE_FAILED,"保存失败");
		}

	}

	@ApiOperation(value = "定时任务开启与停止", notes = "定时任务开启与停止")
	@RequestMapping(value = "/switchState", method = RequestMethod.POST)
	@CacheEvict(cacheNames="SysQuartzJob", allEntries=true)
	public Result switchState(@RequestBody Map<String,String> data){
		try {
			service.switchState(data);
			return new Result<>(Result.CODE_SUCCESS,"操作成功");
		} catch (Exception e) {
			e.printStackTrace();
			return new Result<>(Result.CODE_SUCCESS,"操作失败",e.getMessage());
		}
	}


	@ApiOperation(value = "定时任务删除", notes = "通过id删除定时任务信息。")
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
    @PreAuthorize("hasAuthority('sys:quartz:job:delete')")
	public Result delete(@RequestParam(value="id") String id) {
		try {
			service.removeById(id);
        	return new Result<>(Result.CODE_SUCCESS,"删除成功");
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
            return new Result<>(Result.CODE_FAILED,"删除失败");
		}

	}

	@ApiOperation(value = "批量删除", notes = "批量删除。")
	@RequestMapping(value = "/batchDelete", method = RequestMethod.POST)
	public Result batchDelete(@RequestBody List<String> list) {
		try {
			service.removeByIds(list);
			return new Result<>(Result.CODE_SUCCESS,"删除成功");
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
			return new Result<>(Result.CODE_FAILED,"删除失败");
		}
	}

	@ApiOperation(value = "校验重复", notes = "校验重复。")
	@RequestMapping(value = "/handleCheakRepeat", method = RequestMethod.GET)
	public Result handleCheakRepeat(@RequestParam("jobNameEn") String jobNameEn) {
		try {
			QueryWrapper<SysQuartzJob> queryWrapper = new QueryWrapper<>();
			queryWrapper.setEntity(null);
			queryWrapper.eq("job_name_en",jobNameEn);
			SysQuartzJob sysQuartzJob = service.getOne(queryWrapper);
			if (sysQuartzJob != null){
				return new Result<>(Result.CODE_SUCCESS,"",true);
			}else{
				return new Result<>(Result.CODE_SUCCESS,"",false);
			}

		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
			return new Result<>(Result.CODE_FAILED,"删除失败");
		}
	}


	@ApiOperation(value = "分页查询定时任务list", notes = "带参数查询,{\"page\": 1,\"size\": 5,......}。支持模糊查询")
	@RequestMapping(value="/findList",method = RequestMethod.POST)
    @PreAuthorize("hasAuthority('sys:quartz:job:view')")
	public Result findList(@RequestBody Map<String,Object> data) {

		try {
			IPage<SysQuartzJob> selectPage = service.selectPage(data);
			return new Result<>(Result.CODE_SUCCESS,"",selectPage.getRecords(),selectPage.getTotal());
		} catch (Exception e) {
			e.printStackTrace();
			return new Result<>(Result.CODE_FAILED,"查询异常",e.getMessage());
		}
	}

	@ApiOperation(value = "查询定时任务所有数据list", notes = "查询定时任务所有数据list。")
	@RequestMapping(value="/findAll",method = RequestMethod.POST)
    @PreAuthorize("hasAuthority('sys:quartz:job:view')")
	public Result findAll(){
        return new Result<>(Result.CODE_SUCCESS,"",service.selectAll());
	}


	@ApiOperation(value = "通过id获取定时任务实体类", notes = "通过id获取定时任务实体类。")
	@RequestMapping(value="/getById",method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('sys:quartz:job:view')")
	public Result getById(@RequestParam(value="id") String id) {
        return new Result<>(Result.CODE_SUCCESS,"",service.getById(id));
	}


	@ApiOperation(value = "导出定时任务excel", notes = "导出定时任务excel。")
	@RequestMapping(value="/export",method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('sys:quartz:job:export')")
	public String export(HttpServletRequest request, HttpServletResponse response){
        try {
        String fileName = "定时任务" + DateUtils.formatDateTime(new Date()) + ".xlsx";

        List<SysQuartzJob> list=service.selectAll();
        ExportExcel exportExcel =new ExportExcel("定时任务", SysQuartzJob.class);
        exportExcel.setDatePattern("yyyy-MM-dd HH:mm:ss");
        exportExcel.setDataList(list).write(response, fileName).dispose();

        return null;
        } catch (Exception e) {
        e.printStackTrace();
        }
        return null;
	}


}

