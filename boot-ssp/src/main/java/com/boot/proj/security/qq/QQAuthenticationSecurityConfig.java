//package com.boot.proj.security.qq;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.authentication.AuthenticationManager;
//import org.springframework.security.config.annotation.SecurityConfigurerAdapter;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.web.DefaultSecurityFilterChain;
//import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
//import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;
//import org.springframework.stereotype.Component;
//import org.springframework.web.client.RestTemplate;
//
///**
// * @author suxiaolin
// * @date 2019/7/13 13:04
// */
//@Component
//public class QQAuthenticationSecurityConfig extends SecurityConfigurerAdapter<DefaultSecurityFilterChain, HttpSecurity> {
//    @Autowired
//    private RestTemplate restTemplate;
//
//    @Autowired
//    private AuthenticationSuccessHandler authenticationSuccessHandler;
//
//    @Override
//    public void configure(HttpSecurity http) throws Exception {
//
//        QQAuthenticationFilter qqLoginFilter = new QQAuthenticationFilter("/login/qq", restTemplate);
//        qqLoginFilter.setAuthenticationSuccessHandler(authenticationSuccessHandler);
//        qqLoginFilter.setAuthenticationManager(http.getSharedObject(AuthenticationManager.class));
//        QQAuthenticationProvider qqAuthenticationProvider = new QQAuthenticationProvider(restTemplate);
//
//        http.authenticationProvider(qqAuthenticationProvider)
//                .addFilterBefore(qqLoginFilter, AbstractPreAuthenticatedProcessingFilter.class);
//    }
//}
