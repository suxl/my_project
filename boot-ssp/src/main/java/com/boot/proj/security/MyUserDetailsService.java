package com.boot.proj.security;

import com.boot.proj.commons.base.entity.LoginUser;
import com.boot.proj.commons.enums.CredentialType;
import com.boot.proj.system.user.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author suxiaolin
 * @date 2019/9/30 14:53
 */
@Service
public class MyUserDetailsService implements UserDetailsService {

//    @Autowired
//    private UpmClient upmClient;

    @Autowired
    private SysUserService sysUserService;

//    private List<IntegrationAuthenticator> authenticators;
//
//    @Autowired(required = false)
//    public void setIntegrationAuthenticators(List<IntegrationAuthenticator> authenticators) {
//        this.authenticators = authenticators;
//    }

    @Override
    public LoginUser loadUserByUsername(String username) throws UsernameNotFoundException {
        IntegrationAuthentication integrationAuthentication = IntegrationAuthenticationContext.get();
        LoginUser loginAppUser = null;
        String authType = integrationAuthentication.getAuthType();
        // 密码登录
        if (CredentialType.password.equals(authType)) {
            loginAppUser = sysUserService.getByUserName(username);
        }

        // 手机验证码登录
        if (CredentialType.sms.equals(authType)) {
            loginAppUser = sysUserService.getByPhone(username);
        }
        return loginAppUser;
    }

    /**
     * 设置授权信息
     *
     * @param user
     */
//    public void setAuthorize(User user) {
//        Authorize authorize = this.upmClient.getAuthorize(user.getId());
//        user.setRoles(authorize.getRoles());
//        user.setResources(authorize.getResources());
//    }
//
//    private UserVO authenticate(IntegrationAuthentication integrationAuthentication) {
//        if (this.authenticators != null) {
//            for (IntegrationAuthenticator authenticator : authenticators) {
//                if (authenticator.support(integrationAuthentication)) {
//                    return authenticator.authenticate(integrationAuthentication);
//                }
//            }
//        }
//        return null;
//    }
}
