package com.boot.proj.security.oauth;

/**
 * @Auther: 苏小林
 * @Date: 2018/12/17 09:05
 * @Description:
 */
public interface ClientInfo {
    String CLIENT_ID = "system";
    String CLIENT_SECRET = "system";
    String CLIENT_SCOPE = "app";
    String GRANT_TYPE = "password";
}
