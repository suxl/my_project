package com.boot.proj.security;

import lombok.Data;

import java.util.Map;

/**
 * @author suxiaolin
 * @date 2019/9/30 14:43
 */
@Data
public class IntegrationAuthentication {

    private String authType;
    private String username;
    private Map<String,String[]> authParameters;

    public String getAuthParameter(String paramter){
        String[] values = this.authParameters.get(paramter);
        if(values != null && values.length > 0){
            return values[0];
        }
        return null;
    }
}
