//package com.boot.proj.security.qq;
//
//import com.alibaba.fastjson.JSON;
//import com.alibaba.fastjson.JSONObject;
//import com.google.common.collect.Lists;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.security.authentication.AuthenticationProvider;
//import org.springframework.security.authentication.InternalAuthenticationServiceException;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.AuthenticationException;
//import org.springframework.security.core.GrantedAuthority;
//import org.springframework.security.core.authority.AuthorityUtils;
//import org.springframework.web.client.RestTemplate;
//
//import java.util.List;
//
///**
// * @author suxiaolin
// * @date 2019/7/13 12:49
// */
//@Slf4j
//public class QQAuthenticationProvider implements AuthenticationProvider {
//    /**
//     * 获取QQ的用户信息
//     */
//    private static final String GET_USER_INFO = "https://graph.qq.com/user/get_user_info?access_token=%s&oauth_consumer_key=%s&openid=%s";
//
//    private RestTemplate restTemplate;
//
//    public QQAuthenticationProvider(RestTemplate restTemplate) {
//        this.restTemplate = restTemplate;
//    }
//
//    @Override
//    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
//        QQAuthenticationToken qqLoginToken = (QQAuthenticationToken) authentication;
//        String openId = (String) qqLoginToken.getPrincipal();
//        String accessToken = qqLoginToken.getAccessToken();
//        String oauthConsumerKey = qqLoginToken.getClientId();
//        String userInfo = restTemplate.getForObject(String.format(GET_USER_INFO, accessToken, oauthConsumerKey, openId), String.class);
//        log.info("获取到的qq登录信息为:{}", userInfo);
////        Gson gson = new Gson();
//        JSONObject parse = (JSONObject) JSON.parse(userInfo);
//        QQUserInfo qqUserInfo = JSONObject.toJavaObject(parse, QQUserInfo.class);
////        QQUserInfo qqUserInfo = gson.fromJson(userInfo, QQUserInfo.class);
//        if (qqUserInfo.getRet() < 0) {
//            throw new InternalAuthenticationServiceException(qqUserInfo.getMsg());
//        }
//        User securityUser = new User(qqUserInfo.getNickname(), AuthorityUtils.createAuthorityList("ROLE_USER"));
//        QQAuthenticationToken token = new QQAuthenticationToken(securityUser, qqLoginToken.getAccessToken(), qqLoginToken.getClientId(), securityUser.getAuthorities());
//        return token;
//    }
//
//    @Override
//    public boolean supports(Class<?> authentication) {
//        return QQAuthenticationToken.class.isAssignableFrom(authentication);
//    }
//
//
//    class User{
//        private String nickName;
//        private List<GrantedAuthority> authorities = Lists.newArrayList();
//
//        public String getNickName() {
//            return nickName;
//        }
//
//        public void setNickName(String nickName) {
//            this.nickName = nickName;
//        }
//
//        public List<GrantedAuthority> getAuthorities() {
//            return authorities;
//        }
//
//        public void setAuthorities(List<GrantedAuthority> authorities) {
//            this.authorities = authorities;
//        }
//
//        public User(String nickName, List<GrantedAuthority> authorities) {
//            this.nickName = nickName;
//            this.authorities = authorities;
//        }
//
//        public User() {
//        }
//    }
//}
