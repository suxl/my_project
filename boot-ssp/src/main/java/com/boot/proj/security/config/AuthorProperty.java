package com.boot.proj.security.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 认证相关配置类
 * @author suxiaolin
 * @date 2019/7/13 9:24
 */
@Component
@Data
public class AuthorProperty {
    @Value("${security.use-jwt}")
    private Boolean useJwt;

    @Value("${security.jwt-signing-key}")
    private String jwtSigningKey;


}
