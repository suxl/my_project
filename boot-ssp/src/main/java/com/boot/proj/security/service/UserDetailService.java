//package com.boot.proj.security.service;
//
//import com.boot.proj.commons.base.entity.LoginUser;
//import com.boot.proj.commons.enums.CredentialType;
//import com.boot.proj.system.user.service.SysUserService;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
//import org.springframework.security.authentication.DisabledException;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.core.userdetails.UsernameNotFoundException;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//import org.springframework.social.security.SocialUserDetails;
//import org.springframework.social.security.SocialUserDetailsService;
//import org.springframework.stereotype.Service;
//
///**
// * 根据用户名获取用户<br>
// * <p>
// * 密码校验请看下面两个类
// *
// * @see org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider
// * @see org.springframework.security.authentication.dao.DaoAuthenticationProvider
// */
//@Slf4j
//@Service
//public class UserDetailService implements UserDetailsService, SocialUserDetailsService {
//
//    @Autowired
//    private SysUserService sysUserService;
//    @Autowired
//    private BCryptPasswordEncoder passwordEncoder;
////    @Autowired
////    private SmsClient smsClient;
//
//    @Override
//    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//
////        String[] params = username.split("\\|");
////        username = params[0];// 真正的用户名
//        LoginUser loginAppUser = null;
////        CredentialType credentialType = CredentialType.valueOf(params[1]);
////        // 用户名密码登录
////        if (credentialType == CredentialType.USERNAME) {
//            loginAppUser = sysUserService.getByUserName(username);
//            if (loginAppUser == null) {
//                throw new AuthenticationCredentialsNotFoundException("用户不存在");
//            } else if (!loginAppUser.isEnabled()) {
//                throw new DisabledException("用户已作废");
//            }
////        }
//
//        // 短信验证码登录
////        if (credentialType == CredentialType.PHONE) {
////            loginAppUser = sysUserService.getByPhone(username);
////            if (loginAppUser == null) {
////                throw new AuthenticationCredentialsNotFoundException("手机号码不存在");
////            } else if (!loginAppUser.isEnabled()) {
////                throw new DisabledException("用户已作废");
////            }
////        }
//
//        System.err.println(loginAppUser);
//        return loginAppUser;
//    }
//
//    @Override
//    public SocialUserDetails loadUserByUserId(String s) throws UsernameNotFoundException {
//        return null;
//    }
//}
