//package com.boot.proj.commons.utils;
//
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
//import org.springframework.web.bind.annotation.ControllerAdvice;
//import org.springframework.web.bind.annotation.ExceptionHandler;
//import org.springframework.web.bind.annotation.RestControllerAdvice;
//
//import javax.servlet.http.HttpServletRequest;
//import java.util.HashMap;
//import java.util.Map;
//
///**
// * @author suxiaolin
// * @date 2019/10/8 14:03
// */
//@Slf4j
////@ControllerAdvice
//@RestControllerAdvice
//public class GlobalExceptionHandler {
//
//    /**
//     * Exception 需要处理的异常
//     * @Description
//     * @author hedong
//     * @date 2018年12月13日 下午11:00:55
//     * @modifyNote
//     * @param ex
//     * @param request
//     * @return
//     */
//    @ExceptionHandler(Exception.class)
//    Object handException(Exception ex, HttpServletRequest request) {
//
//        log.error("请求url:{}, 异常消息信息： {} ", request.getRequestURI(),ex.getMessage());
//        log.error("详细异常信息：", ex);
//
//        Map<String,Object> map=new HashMap<String,Object>();
//        map.put("code", 100);
//        map.put("msg", "发生异常");
//        map.put("url",request.getRequestURI());
//        return map;
//    }
//
//    @ExceptionHandler(OAuth2Exception.class)
//    Object handException(OAuth2Exception ex, HttpServletRequest request) {
//
//        log.error("请求url:{}, 异常消息信息： {} ", request.getRequestURI(),ex.getMessage());
//        log.error("详细异常信息：", ex);
//
//        Map<String,Object> map=new HashMap<String,Object>();
//        map.put("code", 100);
//        map.put("msg", "发生异常");
//        map.put("url",request.getRequestURI());
//        return map;
//    }
//
//}
