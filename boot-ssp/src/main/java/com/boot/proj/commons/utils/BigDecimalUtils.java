package com.boot.proj.commons.utils;

import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;

/**
 * @author suxiaolin
 * @date 2019/7/23 14:25
 */
public class BigDecimalUtils {

    /**
     * 两个数之和
     * @param value1
     * @param value2
     * @return
     */
    public static String add(String value1,String value2){
        String val;
        if (org.apache.commons.lang3.StringUtils.isBlank(value1)){
            val=value2;
        }else if(StringUtils.isBlank(value2)){
            val=value1;
        }else{
            BigDecimal b1 = new BigDecimal(value1);
            BigDecimal b2 = new BigDecimal(value2);
            val= b1.add(b2).toString();
        }

        return val;
    }

    /**
     * 两个数之差
     * @param value1
     * @param value2
     * @return
     */
    public static String sub(String value1,String value2){
        String val;
        if (StringUtils.isBlank(value1)){
            val=value2;
        }else if(StringUtils.isBlank(value2)){
            val=value1;
        }else{
            BigDecimal b1 = new BigDecimal(value1);
            BigDecimal b2 = new BigDecimal(value2);
            val=b1.subtract(b2).toString();
        }
        return val;
    }

    /**
     * 两个数之积
     * @param value1
     * @param value2
     * @return
     */
    public static String mul(String value1,String value2){
        String val;
        if (StringUtils.isBlank(value1)){
            val=value2;
        }else if(StringUtils.isBlank(value2)){
            val=value1;
        }else{
            BigDecimal b1 = new BigDecimal(value1);
            BigDecimal b2 = new BigDecimal(value2);
            val=b1.multiply(b2).toString();
        }
        return val;
    }

    /**
     * 两个数之商
     * @param value1
     * @param value2
     * @return
     */
    public static String div(String value1,String value2,int scale){
        String val;
        if (StringUtils.isBlank(value1)){
            val=value2;
        }else if(StringUtils.isBlank(value2)){
            val=value1;
        }else{
            BigDecimal b1 = new BigDecimal(value1);
            BigDecimal b2 = new BigDecimal(value2);
            val=b1.divide(b2,scale).toString();
        }
        return val;
    }
}
