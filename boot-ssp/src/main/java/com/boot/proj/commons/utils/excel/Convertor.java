package com.boot.proj.commons.utils.excel;


import com.sun.rowset.internal.Row;

/**
 * Excel文件导入导出属性值转换接口
 * 
 * @author hull
 * @version 2016-06-22
 *
 */
public interface Convertor {

	/**
	 * 导出转换
	 * @param sourceField 需要转换的JAVA BEAN源属性值
	 * @param sourceBean 需要转换的JAVA BEAN，需要通过其他属性实现转换算法时用到
	 * @return 转换后的值
	 */
	public <E> Object convert4Export(E sourceField, E sourceBean);

	/**
	 * 导入转换
	 * @param sourceField 需要转换的属性值，从excel表单元格中获取
	 * @param sourceRow 需要转换的excel行，需要通过其他属性实现转换算法时用到
	 * @return 转换后的值
	 */
	public <E> Object convert4Import(E sourceField, Row sourceRow);
}
