package com.boot.proj.commons.enums;

/**
 * @Auther: 苏小林
 * @Date: 2018/12/17 09:07
 * @Description:
 */
public class CredentialType {

    public final static String password = "password";

    public final static String sms = "sms";

    public final static String open_id = "open_id";


}
