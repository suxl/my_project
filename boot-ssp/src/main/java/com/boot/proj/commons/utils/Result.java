package com.boot.proj.commons.utils;

import java.io.Serializable;

public class Result<T> implements Serializable {
	
	/**
	 * 错误码
	 */
	private String code = "";
	
	/**
	 * 提示信息
	 */
	private String message = "";
	
	/**
	 * 返回的数据
	 */
	private T data = null;

	/**
	 * 总条数
	 */
	private Long count;

	public static final String CODE_SUCCESS = "1";//成功
	public static final String CODE_FAILED = "2";//失败
	public static final String CODE_PARTIAL_SUCCESS = "3";//部分成功
	public static final String CODE_LOCK = "4";//用户锁定
	public static final String CODE_DIFFERENT_PLACE_LOGIN = "5";//异地登录
	public static final String CODE_SERVER_DOWN = "6";//微服务调用失败
	
	public Result(String code, String message) {
		super();
		this.code = code;
		this.message = message;
	}
	
	public Result(String code, String message, T dat) {
		super();
		this.code = code;
		this.message = message;
		this.data = dat;
	}

	public Result(String code, String message, T dat, Long count) {
		super();
		this.code = code;
		this.message = message;
		this.data = dat;
		this.count = count;
	}


	public Result() {
		super();
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}

	@Override
	public String toString() {
		return "Result{" +
				"code='" + code + '\'' +
				", message='" + message + '\'' +
				", data=" + data +
				", count=" + count +
				'}';
	}
}
