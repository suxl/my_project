package com.boot.proj.commons.base;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.collect.Lists;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author suxl
 * @date 2018年9月30日 下午3:47:48
 * @TableField(exist = false),表示排除类中的属性.
 */
@Data
@MappedSuperclass
public abstract class BaseEntity<T extends Model> extends Model<T> {
    //	@TableField(exist = false)
    @JsonIgnore
    protected static int currentPage;
    //	@TableField(exist = false)
    @JsonIgnore
    protected static int pageSize;

    //排序列表
    @JsonIgnore
    @TableField(exist = false)
    @Transient
    private List<Map<String,String>> orderBy = Lists.newArrayList();

    /**
     * 自增id
     */
    @Id
    @Column(columnDefinition = "varchar(64) COMMENT 'id'")
    @TableField("id")
    protected String id;

    /**
     * 创建人
     */
    @Column(columnDefinition = "varchar(64) COMMENT '创建人'")
    @TableField("create_by")
    protected String createBy;

    /**
     * 创建人姓名
     */
    @Column(columnDefinition = "varchar(64) COMMENT '创建人姓名'")
    @TableField("create_name")
    protected String createName;
    /**
     * 创建时间
     */
    @Column(columnDefinition = "datetime COMMENT '创建时间'")
    @TableField("create_date")
    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    protected Date createDate;
    /**
     * 修改人
     */
    @Column(columnDefinition = "varchar(64) COMMENT '修改人'")
    @TableField("update_by")
    protected String updateBy;

    /**
     * 修改人姓名
     */
    @Column(columnDefinition = "varchar(64) COMMENT '修改人姓名'")
    @TableField("update_name")
    protected String updateName;
    /**
     * 修改时间
     */
    @Column(columnDefinition = "datetime COMMENT '修改时间'")
    @TableField("update_date")
    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    protected Date updateDate;
    /**
     * 删除标记
     */
    @TableField(value = "del_flag")
    @TableLogic //表字段逻辑处理注解（逻辑删除）
    @Column(columnDefinition = "char(1) COMMENT '删除标记'")
    protected String delFlag;
    /**
     * 备注
     */
    @Column(columnDefinition = "varchar(255) COMMENT '备注'")
    @TableField("remarks")
    protected String remarks;


}
