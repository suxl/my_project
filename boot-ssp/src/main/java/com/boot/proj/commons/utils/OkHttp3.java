package com.boot.proj.commons.utils;

import lombok.extern.slf4j.Slf4j;
import okhttp3.OkHttpClient;

import java.util.concurrent.TimeUnit;

/**
 * @description: 单例模式获取OkHttp3实例
 * @author: koujian
 * @create: 2019-10-25 13:45
 * 变量加上volatile关键字修饰，它可以保证当A线程对变量值做了变动之后，会立即刷回到主内存中，
 * 而其它线程读取到该变量的值也作废，
 * 强迫重新从主内存中读取该变量的值，这样在任何时刻，AB线程总是会看到变量的同一个值。
 *
 * instance变量被volatile关键字所修饰，如果去掉该关键字，就不能保证该代码执行的正确性。
 * 这是因为“instance = new Singleton();”这行代码并不是原子操作，其在JVM中被分为如下三个阶段执行：
 *
 * 1.为instance分配内存
 * 2.初始化instance
 * 3.将instance变量指向分配的内存空间
 * 由于JVM可能存在重排序，上述的二三步骤没有依赖的关系，可能会出现先执行第三步，
 * 后执行第二步的情况。也就是说可能会出现instance变量还没初始化完成，其他线程就已经判断了该变量值不为null，
 * 结果返回了一个没有初始化完成的半成品的情况。而加上volatile关键字修饰后，可以保证instance变量的操作不会被JVM所重排序，
 * 每个线程都是按照上述一二三的步骤顺序的执行，这样就不会出现问题。
 *
 *双重检查模式
 **/
@Slf4j
public class OkHttp3 {
    /**
     * 单例
     * **/
    private static volatile OkHttpClient singleton;
    //非常有必要，要不此类还是可以被new，但是无法避免反射
    private OkHttp3(){

    }
    public static OkHttpClient getInstance()throws Exception {
        if (singleton == null) {
            synchronized (OkHttp3.class) {

                if (singleton == null) {
                    singleton = new OkHttpClient();
                    singleton.newBuilder()
                            //连接超时时间
                            .connectTimeout(5000,TimeUnit.SECONDS)
                            //读超时时间
                            .readTimeout(5000,TimeUnit.SECONDS)
                            //写超时时间
                            .writeTimeout(5000,TimeUnit.SECONDS);
                }
            }
        }
        return singleton;
    }

}
