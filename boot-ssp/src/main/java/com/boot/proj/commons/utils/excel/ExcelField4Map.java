package com.boot.proj.commons.utils.excel;

/**
 * @Auther: 苏小林
 * @Date: 2019/1/15 17:01
 * @Description:
 */
public class ExcelField4Map {

    public enum Align {
        ALIGN_AUTO(0), ALIGN_LEFT(1), ALIGN_CENTER(2), ALIGN_RIGHT(3);
        int value;

        Align(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }
    }

    public enum ImportExportType {
        IMPORT(0), EXPORT(1), BOTH(2);
        int value;

        ImportExportType(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }
    }

    private String mapKey;
    private String title;
    private Align align = Align.ALIGN_CENTER;
    private ImportExportType importExportType = ImportExportType.BOTH;
    private int sort = 0;
    private String decimalFormat = "";

    public String getMapKey() {
        return mapKey;
    }

    public void setMapKey(String mapKey) {
        this.mapKey = mapKey;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Align getAlign() {
        return align;
    }

    public void setAlign(Align align) {
        this.align = align;
    }

    public ImportExportType getImportExportType() {
        return importExportType;
    }

    public void setImportExportType(ImportExportType importExportType) {
        this.importExportType = importExportType;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }

    public String getDecimalFormat() {
        return decimalFormat;
    }

    public void setDecimalFormat(String decimalFormat) {
        this.decimalFormat = decimalFormat;
    }

    public ExcelField4Map(String mapKey, String title, Align align, ImportExportType importExportType, int sort, String decimalFormat) {
        this.mapKey = mapKey;
        this.title = title;
        this.align = align;
        this.importExportType = importExportType;
        this.sort = sort;
        this.decimalFormat = decimalFormat;
    }

    public ExcelField4Map(String mapKey, String title, Align align, ImportExportType importExportType, int sort) {
        this.mapKey = mapKey;
        this.title = title;
        this.align = align;
        this.importExportType = importExportType;
        this.sort = sort;
    }

    public ExcelField4Map(String mapKey, String title, int sort) {
        this.mapKey = mapKey;
        this.title = title;
        this.sort = sort;
    }

    public ExcelField4Map(String mapKey, String title) {
        this.mapKey = mapKey;
        this.title = title;
    }
}
