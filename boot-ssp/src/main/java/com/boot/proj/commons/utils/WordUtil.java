package com.boot.proj.commons.utils;

import java.io.*;
import java.net.URL;
import java.util.Locale;
import java.util.Map;

import freemarker.cache.URLTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

@Component
public class WordUtil {

    private static Configuration freemarkerConfig;
    @Autowired
    HttpServletRequest req;
    static {
        freemarkerConfig = new Configuration(Configuration.VERSION_2_3_22);
        freemarkerConfig.setEncoding(Locale.getDefault(), "UTF-8");
    }

    /**
     * @Desc：生成word文件
     * @Author：张轮
     * @Date：2014-1-22下午05:33:42
     * @param dataMap word中需要展示的动态数据，用map集合来保存
     * @param templateName word模板名称，例如：test.ftl
     * @param filePath 文件生成的目标路径，例如：D:/wordFile/
     * @param fileName 生成的文件名称，例如：test.doc
     */
  @SuppressWarnings("unchecked")
public void createWord(Object dataMap, String templateName, String filePath, String fileName) throws IOException {
      Writer out = null;
      FileOutputStream fileOutputStream = null;
    try {
        freemarkerConfig.setTemplateLoader(new URLTemplateLoader() {

            @Override
            protected URL getURL(String arg0) {
                //此处访问的地址是项目target/classes文件夹
                //输出的类似：E:/software/idea/aa/target/classes/6.xm
                return WordUtil.class.getResource("/"+templateName);
            }
        });

        Template temp = freemarkerConfig.getTemplate(templateName);

        File targetFile = new File(filePath+fileName);
        fileOutputStream = new FileOutputStream(targetFile);
        out = new OutputStreamWriter(new FileOutputStream(targetFile),"UTF-8");

        //执行模板替换
        temp.process(dataMap, out);
        out.flush();
        System.err.println("文件生成完");

    } catch (Exception e) {
      e.printStackTrace();
    }finally {
        if (out !=null){
            out.close();
        }
        if (fileOutputStream !=null){
            fileOutputStream.close();
        }
        System.err.println("done");
    }
  }
}
