package com.boot.proj.commons.utils;

/**
 * 数学相关工具类
 * @author suxiaolin
 * @date 2019/11/6 11:57
 */
public class MathRelateUtils {


    /**
     * 求两个数的最大公约数 - 辗转相除法
     * @param m
     * @param n
     * @return
     */
    public static int maxCommonDivisor(int m,int n){
        int temp;
        if (n > m) {
            temp = n;
            n = m;
            m = temp;
        }
        if (m % n == 0) {
            return n;
        }
        return maxCommonDivisor(m - n, n);
    }

    public static void main(String[] args) {
        int i = maxCommonDivisor(720, 404);
        System.err.println(i);
    }


}
