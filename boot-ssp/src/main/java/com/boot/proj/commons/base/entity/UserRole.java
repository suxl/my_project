package com.boot.proj.commons.base.entity;

import lombok.Data;

/**
 * @author suxiaolin
 * @date 2019/5/23 16:51
 */
@Data
public class UserRole {
    private String userId;
    private String roleId;
}
