package com.boot.proj.commons.utils.excel.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @Auther: 苏小林
 * @Date: 2019/1/15 09:45
 * @Description: Excel注解定义
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface ExcelField {
    int ALIGN_AUTO = 0;
    int ALIGN_LEFT = 1;
    int ALIGN_CENTER = 2;
    int ALIGN_RIGHT = 3;

    int TYPE_IMPORT_EXPORT = 0;
    int TYPE_EXPORT = 1;
    int TYPE_IMPORT = 2;

    /**
     * 导出字段名（默认调用当前字段的“get”方法，如指定导出字段为对象，请填写“对象名.对象属性”，例：“area.name”、“office.name”）
     */
    String value() default "";

    /**
     * 导出字段标题（需要添加批注请用“**”分隔，标题**批注，仅对导出模板有效）
     */
    String title();

    /**
     * 字段类型（0：导出导入；1：仅导出；2：仅导入）
     */
    int type() default 0;

    /**
     * 导出字段对齐方式（0：自动；1：靠左；2：居中；3：靠右）
     */
    int align() default 0;

    /**
     * 导出字段字段排序（升序）
     */
    int sort() default 0;

    /**
     * 如果是字典类型，请设置字典的type值
     */
    String dictType() default "";

    /**
     * 反射类型
     */
    Class<?> fieldType() default Class.class;

    /**
     * 字段归属组（根据分组导出导入）
     */
    int[] groups() default {};

    /**
     * 枚举值，格式为{1=I18n:"true",0="false"}. "I18n:"为可选的前缀，表示后面为I18n的键值，否则为实际显示值
     */
    String enums() default "";

    /**
     * 属性值转换器，带包路径的类名字符串，该类必须实现com.cpit.ltem.omc.common.excel.Convertor接口
     *
     * @return
     */
    String convertor() default "";

    /**
     * 小数的格式，例如保留小数点后3位 "#.###"
     * @return
     */
    String decimalFormat() default "";
}
