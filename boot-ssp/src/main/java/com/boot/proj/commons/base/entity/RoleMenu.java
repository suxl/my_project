package com.boot.proj.commons.base.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

/**
 * @author suxiaolin
 * @date 2019/5/22 15:45
 */
@Data
public class RoleMenu {
    /**
     * 角色id
     */
    @TableField("role_id")
    private String roleId;

    /**
     * 菜单id
     */
    @TableField("menu_id")
    private String menuId;
}
