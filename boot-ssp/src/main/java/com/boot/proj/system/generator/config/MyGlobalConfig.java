package com.boot.proj.system.generator.config;

import com.baomidou.mybatisplus.generator.config.GlobalConfig;

/**
 * @author suxiaolin
 * @date 2019/6/28 9:25
 */
public class MyGlobalConfig extends GlobalConfig {
    private String viewName;

    public String getViewName() {
        return viewName;
    }

    public MyGlobalConfig setViewName(String viewName) {
        this.viewName = viewName;
        return this;
    }
}
