package com.boot.proj.system.dict.service;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.boot.proj.commons.base.BaseService;
import com.boot.proj.system.dict.dao.SysDictMapper;
import com.boot.proj.system.dict.entity.SysDict;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
/**
 * <p>
 * $!{table.comment} 服务实现类
 * </p>
 *
 * @author 苏小林
 * @since 2019-07-18
 */
@Service
@Transactional(rollbackFor = Exception.class)
@CacheConfig(cacheNames = "SysDict")
public class SysDictService extends BaseService<SysDictMapper, SysDict> {
    @Autowired
    private SysDictMapper dao;

    @Override
    @CacheEvict(cacheNames="SysDict", allEntries=true)
    public boolean save(SysDict entity) {
        return super.save(entity);
    }

    @Override
    @Cacheable(key = "#id",sync = true)
    public SysDict getById(Serializable id) {
        return super.getById(id);
    }

    @Override
    @CacheEvict(cacheNames="SysDict", allEntries=true)
    public boolean removeById(Serializable id) {
        return super.removeById(id);
    }

    @CacheEvict(cacheNames="SysDict", allEntries=true)
    @Override
    public boolean removeByIds(Collection<? extends Serializable> idList) {
        return super.removeByIds(idList);
    }

    @Cacheable(key = "#data",sync = true)
    public IPage<SysDict> selectPage(Map<String,Object> data) {
        int currentPage=1;
        if (!Objects.isNull(data.get("page"))) {
            currentPage=(Integer) data.get("page");
        }
        int pageSize=20;
        if (!Objects.isNull(data.get("size"))) {
            pageSize=(Integer) data.get("size");
        }

        SysDict entity = JSON.parseObject(JSON.toJSONString(data), SysDict.class);
        Page<SysDict> page = new Page(currentPage,pageSize);
        QueryWrapper<SysDict> wrapper = new QueryWrapper(entity);
        return super.selectPage(page, wrapper);
    }

    @Override
    @Cacheable(value = "SysDict",sync = true)
    public List<SysDict> selectAll() {
        return super.selectAll();
    }


}
