package com.boot.proj.system.menu.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.boot.proj.system.menu.entity.MenuTree;
import com.boot.proj.system.menu.entity.SysMenu;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Set;

/**
 * <p>
 * 菜单表 Mapper 接口
 * </p>
 *
 * @author 苏小林
 * @since 2018-12-10
 */
@Mapper
public interface SysMenuMapper extends BaseMapper<SysMenu> {

    List<SysMenu> getMenuByRoleIds(@Param("roleIds") Set<String> roleIds);

    /**
     * 获取最大的排序
     * @return
     */
    Integer getMaxSort();

    /**
     * 通过角色id查询权限列表
     * @param roleIds
     * @return
     */
    Set<SysMenu> getPermissionByRoleIds(@Param("roleIds") Set<String> roleIds);

    /**
     * 通过角色id查询角色菜单
     * @param roleId
     * @return
     */
    List<SysMenu> findRoleMenu(@Param("roleId") String roleId);

    List<MenuTree> findMenuTree();
}
