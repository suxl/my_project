package com.boot.proj.system.generator.config;

import com.baomidou.mybatisplus.generator.config.TemplateConfig;

/**
 * @author suxiaolin
 * @date 2019/6/28 8:57
 */
public class MyTemplateConfig extends TemplateConfig {
    private String entity = "/templates/entity.java";
    private String entityKt = "/templates/entity.kt";
    private String service = "/templates/service.java";
    private String serviceImpl = "/templates/serviceImpl.java";
    private String mapper = "/templates/mapper.java";
    private String xml = "/templates/mapper.xml";
    private String controller = "/templates/controller.java";
    private String view = "/templates/entity.java";

    public String getEntity(boolean kotlin) {
        return kotlin ? this.entityKt : this.entity;
    }

    public MyTemplateConfig() {
    }

    public String getEntityKt() {
        return this.entityKt;
    }

    public String getService() {
        return this.service;
    }

    public String getServiceImpl() {
        return this.serviceImpl;
    }

    public String getMapper() {
        return this.mapper;
    }

    public String getXml() {
        return this.xml;
    }

    public String getController() {
        return this.controller;
    }

    public TemplateConfig setEntity(final String entity) {
        this.entity = entity;
        return this;
    }

    public TemplateConfig setEntityKt(final String entityKt) {
        this.entityKt = entityKt;
        return this;
    }

    public TemplateConfig setService(final String service) {
        this.service = service;
        return this;
    }

    public TemplateConfig setServiceImpl(final String serviceImpl) {
        this.serviceImpl = serviceImpl;
        return this;
    }

    public TemplateConfig setMapper(final String mapper) {
        this.mapper = mapper;
        return this;
    }

    public TemplateConfig setXml(final String xml) {
        this.xml = xml;
        return this;
    }

    public TemplateConfig setController(final String controller) {
        this.controller = controller;
        return this;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof MyTemplateConfig)) {
            return false;
        } else {
            MyTemplateConfig other = (MyTemplateConfig)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label95: {
                    Object this$entity = this.entity;
                    Object other$entity = other.entity;
                    if (this$entity == null) {
                        if (other$entity == null) {
                            break label95;
                        }
                    } else if (this$entity.equals(other$entity)) {
                        break label95;
                    }

                    return false;
                }

                Object this$entityKt = this.getEntityKt();
                Object other$entityKt = other.getEntityKt();
                if (this$entityKt == null) {
                    if (other$entityKt != null) {
                        return false;
                    }
                } else if (!this$entityKt.equals(other$entityKt)) {
                    return false;
                }

                Object this$service = this.getService();
                Object other$service = other.getService();
                if (this$service == null) {
                    if (other$service != null) {
                        return false;
                    }
                } else if (!this$service.equals(other$service)) {
                    return false;
                }

                label74: {
                    Object this$serviceImpl = this.getServiceImpl();
                    Object other$serviceImpl = other.getServiceImpl();
                    if (this$serviceImpl == null) {
                        if (other$serviceImpl == null) {
                            break label74;
                        }
                    } else if (this$serviceImpl.equals(other$serviceImpl)) {
                        break label74;
                    }

                    return false;
                }

                label67: {
                    Object this$mapper = this.getMapper();
                    Object other$mapper = other.getMapper();
                    if (this$mapper == null) {
                        if (other$mapper == null) {
                            break label67;
                        }
                    } else if (this$mapper.equals(other$mapper)) {
                        break label67;
                    }

                    return false;
                }

                Object this$xml = this.getXml();
                Object other$xml = other.getXml();
                if (this$xml == null) {
                    if (other$xml != null) {
                        return false;
                    }
                } else if (!this$xml.equals(other$xml)) {
                    return false;
                }

                Object this$controller = this.getController();
                Object other$controller = other.getController();
                if (this$controller == null) {
                    if (other$controller != null) {
                        return false;
                    }
                } else if (!this$controller.equals(other$controller)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof TemplateConfig;
    }

    public int hashCode() {
        Boolean PRIME = true;
        int result = 1;
        Object $entity = this.entity;
        result = result * 59 + ($entity == null ? 43 : $entity.hashCode());
        Object $entityKt = this.getEntityKt();
        result = result * 59 + ($entityKt == null ? 43 : $entityKt.hashCode());
        Object $service = this.getService();
        result = result * 59 + ($service == null ? 43 : $service.hashCode());
        Object $serviceImpl = this.getServiceImpl();
        result = result * 59 + ($serviceImpl == null ? 43 : $serviceImpl.hashCode());
        Object $mapper = this.getMapper();
        result = result * 59 + ($mapper == null ? 43 : $mapper.hashCode());
        Object $xml = this.getXml();
        result = result * 59 + ($xml == null ? 43 : $xml.hashCode());
        Object $controller = this.getController();
        result = result * 59 + ($controller == null ? 43 : $controller.hashCode());
        return result;
    }

    public String toString() {
        return "TemplateConfig(entity=" + this.entity + ", entityKt=" + this.getEntityKt() + ", service=" + this.getService() + ", serviceImpl=" + this.getServiceImpl() + ", mapper=" + this.getMapper() + ", xml=" + this.getXml() + ", controller=" + this.getController() + ")";
    }


    public String getView() {
        return view;
    }

    public MyTemplateConfig setView(String view) {
        this.view = view;
        return this;
    }
//





}
