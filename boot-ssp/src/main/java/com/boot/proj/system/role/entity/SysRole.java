package com.boot.proj.system.role.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.boot.proj.commons.base.BaseEntity;
import com.google.common.collect.Lists;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * <p>
 * 角色表
 * </p>
 *
 * @author 苏小林
 * @since 2018-10-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("sys_role")
public class SysRole extends BaseEntity<SysRole> {

    /**
     * 角色code
     */
    @TableField("code")
    private String code;
    /**
     * 角色名
     */
    @TableField("name")
    private String name;

    /**
     * 菜单名称
     */
    @TableField(exist = false)
    private String menuNames;

    @TableField(exist = false)
    private List<String> menuIds = Lists.newArrayList();

    /**
     * 回显的所有菜单id。
     */
    @TableField("echo_menu_ids")
    private String echoMenuIds;


}
