package com.boot.proj.system.user.web;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.boot.proj.commons.utils.IdGen;
import com.boot.proj.commons.utils.OkHttp3;
import com.boot.proj.commons.utils.WordUtil;
import com.boot.proj.config.IpConfiguration;
import com.boot.proj.system.dict.entity.SysDict;
import com.boot.proj.system.dict.service.SysDictService;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import io.searchbox.client.JestClient;
import io.searchbox.client.JestResult;
import io.searchbox.core.Index;
import io.searchbox.core.Search;
import io.searchbox.indices.CreateIndex;
import io.searchbox.indices.DeleteIndex;
import io.searchbox.indices.mapping.GetMapping;
import okhttp3.*;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.*;
import java.util.*;


/**
 * @author suxiaolin
 * @date 2019/11/1 11:34
 */
@RestController
@RequestMapping("/users-anon")
public class BootProTestController {
    private static String indexName = "userindex";
    private static String typeName = "user";
    @Autowired
    private JestClient jestClient;

    @Autowired
    private SysDictService sysDictService;

    @Autowired
    private WordUtil wordUtil;

    @Autowired
    private IpConfiguration ipConfiguration;

//    @Autowired
//    private MockMvc mockMvc;

//    @Autowired
//    private MQSender sender;

    @RequestMapping(value = "/createIndex",method = RequestMethod.GET)
    public void createIndex(){
        JestResult jr = null;
        try {
            jr = jestClient.execute(new CreateIndex.Builder(indexName).build());
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(jr.isSucceeded());
    }
    @RequestMapping(value = "/insert",method = RequestMethod.GET)
    public void insert(){
        SysDict sysDict = new SysDict();
        sysDict.setId(IdGen.uuid());
        sysDict.setValue("0");
        sysDict.setLabel("我的祖国");
        sysDict.setType("area");
        sysDict.setDescription("中国");
        sysDict.setDelFlag("0");

        Index index = new Index.Builder(sysDict).index(indexName).type(typeName).build();
        try{
            JestResult jr = jestClient.execute(index);
            System.out.println(jr.isSucceeded());
        }catch(IOException e){
            e.printStackTrace();
        }
    }

    /**
     * 查询数据
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getIndexMapping",method = RequestMethod.GET)
    public void getIndexMapping() throws Exception {
        GetMapping getMapping = new GetMapping.Builder().addIndex(indexName).addType(typeName).build();
        JestResult jr =jestClient.execute(getMapping);
        System.out.println(jr.getJsonString());
    }

    /**
     * 全文搜索
     * @param indexName
     * @param typeName
     * @param query
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/search",method = RequestMethod.GET)
    public String search(String indexName, String typeName, String query) throws Exception {
        Search search = new Search.Builder(query).addIndex(indexName).addType(typeName).build();
        JestResult jr = jestClient.execute(search);
        System.out.println("--++"+jr.getJsonString());
        System.out.println("--"+jr.getSourceAsObject(SysDict.class));
        return jr.getSourceAsString();
    }

    /**
     * 删除索引
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/deleteIndex",method = RequestMethod.GET)
    public void deleteIndex() throws Exception{
        JestResult jr = jestClient.execute(new DeleteIndex.Builder(indexName).build());
        System.out.println(jr.isSucceeded());
    }

    //极光推送
    @RequestMapping(value = "/jpushAndroid",method = RequestMethod.GET)
    public void jpushAndroid(String phone, String message) {   // 推送的手机、推送的告警信息
        // 设置好极光的app_key和masterSecret
//        String appKey = "337e02edd10b2d28074e0c23";
//        String masterSecret = "f1f201a24478c724454d5350";
//        //创建JPushClient
//        JPushClient jpushClient = new JPushClient(masterSecret, appKey);
//        //推送的关键,构造一个payload
//        PushPayload payload = PushPayload.newBuilder()
//                .setPlatform(Platform.all()) //指定所有平台，安卓，苹果，WePhone
//                .setAudience(Audience.alias(phone)) //指定某个用户，注意，该用户必须存在于该appKey拥有者的服务用户下，不然会提示找不到该用户
//                .setNotification(Notification.alert(message)) // 发送的内容
//                //这里是指定开发环境,不用设置也没关系
//                .setOptions(Options.newBuilder().setApnsProduction(false).build())
//                .setMessage(Message.content(message))  //自定义信息
//                .build();
//        try {
//            PushResult pu = jpushClient.sendPush(payload);
//            System.out.println(pu.msg_id+" "+pu.sendno);  // 成功推送后输出id和sendno
//        } catch (APIConnectionException e) {
//            e.printStackTrace();
//            System.out.println("API error");
//        } catch (APIRequestException e) {
//            e.printStackTrace();
//            System.out.println("Request error");
//        }

        try {
//            String s = Base64.encodeBase64String("337e02edd10b2d28074e0c23:f1f201a24478c724454d5350".getBytes());
            String s = Base64.encodeBase64String("5eb5d262eefbf2c1e68839ea:c73f2c4f142cfd964146be1c".getBytes());
            System.err.println(s);

            String url="https://api.jpush.cn/v3/push";
//            String url="https://api.jpush.cn/v3/push/batch/regid/single";
            OkHttpClient okHttpClient = OkHttp3.getInstance();

//            Request request = new Request.Builder()
//                    .url(url)
//                    .header("Authorization", s)
//                    .build();

            MediaType mediaType = MediaType.parse("application/json; charset=utf-8");

            // 添加手机号
            JSONObject registration_id = new JSONObject();
            JSONArray jsonArray = new JSONArray();
            jsonArray.add("190e35f7e02df0f9fb3");
            registration_id.put("registration_id",jsonArray);
//            JSONObject audience = new JSONObject();
//            audience.put("audience",registration_id);
            JSONObject jsonO = new JSONObject();
            // 消息
            jsonO.put("alert","hello who are you!");
            JSONObject jsonObject = new JSONObject();
            // 推送平台
            jsonObject.put("platform","all");
            // 推送目标
            jsonObject.put("audience",registration_id);
            // 消息
            jsonObject.put("notification",jsonO);

            System.err.println(jsonObject);

            String string = JSON.toJSONString(jsonObject);
            RequestBody body = RequestBody.create(mediaType,string);

            Request request = new Request.Builder()
                    .url(url)
                    .addHeader("Authorization", "Basic "+s)
                    .post(body)
                    .build();
            Call call = okHttpClient.newCall(request);
            Response response = call.execute();
            System.out.println(response.body().string());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
//    @RequestMapping(value = "testMqSend",method = RequestMethod.GET)
//    public void testMqSend(){
//        sender.sendMSG("为人君，止于仁，为人臣，止于敬，为人子，止于孝，为人父，止于慈。");
//    }




    @RequestMapping(value = "/copyFile",method = RequestMethod.GET)
    public void copyFile(){
        File file = new File("C:\\temp\\ffm\\playlist.m3u8");
        System.out.println("文件绝对路径 :"+file.getAbsolutePath());
        List<String> listStr = new ArrayList<String>();
        BufferedReader br = null;
        String str = null;
        try {
            br = new BufferedReader(new FileReader(file));
            while ((str = br.readLine())!= null) {
                if (str.equals("file1.ts")) {
                    str = "file111.ts";
                }
                listStr.add(str);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(listStr);


        File file1 = new File("C:\\temp\\ffm\\playlist1.m3u8");// 要写入的文件路径
        if (!file1.exists()) {// 判断文件是否存在
            try {
                file1.createNewFile();// 如果文件不存在创建文件
                System.out.println("文件"+file1.getName()+"不存在已为您创建!");
            } catch (IOException e) {
                System.out.println("创建文件异常!");
                e.printStackTrace();
            }
        } else {
            System.out.println("文件"+file1.getName()+"已存在!");
        }

        for (String str1 : listStr) {// 遍历listStr集合
            FileOutputStream fos = null;
            PrintStream ps = null;
            try {
                fos = new FileOutputStream(file1,true);// 文件输出流	追加
                ps = new PrintStream(fos);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            String string  = str1 + "\r\n";// +换行
            ps.print(string); // 执行写操作
            ps.close();	// 关闭流

        }

    }

    @RequestMapping(value = "/testCreateDocument",method = RequestMethod.GET)
    public void testCreateDocument(){


        try {
            String filePath = "D:\\test\\"; //文件路径
            String fileName = "接口文档.doc"; //文件名称
            String templateName = "接口.ftl"; //文件唯一名称
            int port = ipConfiguration.getPort();

            String url = "http://127.0.0.1:"+port+"/v2/api-docs";

            OkHttpClient okHttpClient = OkHttp3.getInstance();

            Request request = new Request.Builder()
                    .get()
                    .url(url)
                    .build();

            Call call = okHttpClient.newCall(request);
            Response response1 = call.execute();
            String string = response1.body().string();
            System.err.println(string);

            JSONObject jsonObject = JSONObject.parseObject(string);
            HashMap<Object, List<Map>> result = Maps.newHashMap();
            Map<Object,Object> paths = (Map) jsonObject.get("paths");
            paths.keySet().forEach(k -> {
                Map<Object, Object> re = Maps.newHashMap();
                JSONObject o = (JSONObject) paths.get(k);
//                System.err.println(o);
                re.put("url",k);//请求路径

                Iterator<String> iterator = o.keySet().iterator();
                while (iterator.hasNext()) {
                    // 获得key
                    String key = iterator.next();
                    re.put("method",key);//请求方式
                    //获得key值对应的value
                    Object o1 = o.get(key);
                    Object description = Objects.isNull(getValue(o1, "description"))?"":getValue(o1, "description");
                    re.put("description",description);//请求方式
//                    System.err.println(key+"---"+o1);

                    JSONArray tags1 = (JSONArray) getValue(o1, "tags");

                    re.put("tags",tags1.get(0));
                    JSONArray parameters = (JSONArray)getValue(o1, "parameters");
                    List<Object> parameterList = Lists.newArrayList();
                    if (parameters != null && parameters.size()>0) {
                        for (Object parameter : parameters) {
                            Map<Object, Object> par = Maps.newHashMap();
                            JSONObject parameter1 = (JSONObject) parameter;
                            Object name = parameter1.get("name");
                            Object desc = parameter1.get("description");
                            Object required = parameter1.get("required");
                            Object type = Objects.isNull(parameter1.get("type"))?"String":parameter1.get("type");
                            par.put("name",name);
                            par.put("type",type);
                            par.put("desc",desc);
                            par.put("required",required.toString());
                            parameterList.add(par);
                        }
                        re.put("parameters",parameterList);
                    }else{
                        List<Object> pa = Lists.newArrayList();
                        re.put("parameters",pa);
                    }
                    if (result.containsKey(tags1.get(0))) {
                        result.get(tags1.get(0)).add(re);
                    }else{
                        List<Map> objects = Lists.newArrayList();
                        objects.add(re);
                        result.put(tags1.get(0),objects);
                    }
                }

            });


            Map<Object, Object> rr = Maps.newHashMap();
            rr.put("data",result);
            System.err.println(JSON.toJSON(rr));
            wordUtil.createWord(rr,templateName,filePath,fileName);

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public Object getValue(Object o,String p){
        JSONObject o1 = (JSONObject) JSON.toJSON(o);
        return o1.get(p);
    }


    @RequestMapping(value = "/testFremmark",method = RequestMethod.GET)
    public void testFremmark(){
//  [{"title":"用户接口","list":[{"":""},{"":""}]},{"title":"用户接口","list":[{"":""},{"":""}]}]
        try {
            String filePath = "D:\\test\\"; //文件路径
            String fileName = "test.doc"; //文件名称
            String templateName = "接口.ftl"; //文件唯一名称


            Map<String, List> map = Maps.newHashMap();
            for (int i = 0; i <3 ; i++) {
                // 结果数组
//                JSONArray resultArray = new JSONArray();
                JSONObject title = new JSONObject();
                JSONArray listArray = new JSONArray();

                JSONObject json1 = new JSONObject();
                json1.put("message","第一个"+i);
                json1.put("way","get");
                json1.put("url","www.baidu.com"+i);
                listArray.add(json1);

                JSONObject json2 = new JSONObject();
                json2.put("message","第二个"+i);
                json2.put("way","post");
                json2.put("url","www.tenxun.com"+i);
                listArray.add(json2);

                title.put("title","用户接口"+i);
                title.put("list",listArray);
//                resultArray.add(title);
                if (map.containsKey("data")) {
                    map.get("data").add(title);
                }else{
                    List<Object> l = Lists.newArrayList();
                    l.add(title);
                    map.put("data",l);
                }
            }

            System.err.println(JSON.toJSON(map));




            wordUtil.createWord(map,templateName,filePath,fileName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
