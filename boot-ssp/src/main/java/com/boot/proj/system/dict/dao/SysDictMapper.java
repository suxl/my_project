package com.boot.proj.system.dict.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.boot.proj.system.dict.entity.SysDict;
import org.apache.ibatis.annotations.Mapper;
/**
 * <p>
 * $!{table.comment} Mapper 接口
 * </p>
 *
 * @author 苏小林
 * @since 2019-07-17
 */
@Mapper
public interface SysDictMapper extends BaseMapper<SysDict> {

}
