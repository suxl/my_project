package com.boot.proj.system.menu.entity;

import com.google.common.collect.Lists;
import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * vue 动态路由
 * @author suxiaolin
 * @date 2019/5/31 11:53
 */
@Data
public class AsyncRoutes {
    private String id;
    private String parentId;
    private String name;
    private String path;
    private String icon;
    private String component;
    private Map<String,String> meta;
    private List<AsyncRoutes> children = Lists.newArrayList();
}
