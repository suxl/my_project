package com.boot.proj.system.menu.web;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.boot.proj.commons.base.entity.LoginUser;
import com.boot.proj.commons.utils.Result;
import com.boot.proj.commons.utils.StringUtils;
import com.boot.proj.commons.utils.UserUtils;
import com.boot.proj.system.menu.entity.MenuTree;
import com.boot.proj.system.menu.entity.SysMenu;
import com.boot.proj.system.menu.service.SysMenuService;
import com.boot.proj.system.role.entity.SysRole;
import com.google.common.collect.Lists;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * <p>
 * 菜单表 前端控制器
 * </p>
 *
 * @author 苏小林
 * @since 2018-12-10
 */
@RestController
@RequestMapping("/system/sysMenu")
public class SysMenuController {

	@Autowired
    public SysMenuService service;


	@ApiOperation(value = "保存/修改", notes = "保存和修改方法")
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public Result save(@RequestBody SysMenu data){
		try {

			if ("1".equals(data.getParentId())) {//顶级菜单
				data.setParentIds("1");
			}else{
				SysMenu sysMenu = service.getById(data.getParentId());
				data.setParentIds(sysMenu.getParentIds()+","+sysMenu.getId());
			}
			service.save(data);
			return new Result<>(Result.CODE_SUCCESS,"保存成功");
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
			return new Result<>(Result.CODE_FAILED,"保存失败");
		}

	}


	@ApiOperation(value = "删除", notes = "通过id删除。")
	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public Result delete(@RequestParam(value="id") String id) {
		try {
			service.removeById(id);
			return new Result<>(Result.CODE_SUCCESS,"删除成功");
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
			return new Result<>(Result.CODE_SUCCESS,"删除失败");
		}
	}

	@ApiOperation(value = "批量删除", notes = "批量删除。")
	@RequestMapping(value = "/batchDelete", method = RequestMethod.POST)
	public Result batchDelete(@RequestBody List<String> list) {
		try {
			service.removeByIds(list);
			return new Result<>(Result.CODE_SUCCESS,"删除成功");
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
			return new Result<>(Result.CODE_FAILED,"删除失败");
		}
	}

	@ApiOperation(value = "分页查询list", notes = "带参数查询,{\"page\": 1,\"size\": 5,......}。支持模糊查询")
	@RequestMapping(value="/findList",method = RequestMethod.POST)
	public Result findList(@RequestBody Map<String,Object> data) {
		try {
			int currentPage=1;
			if (!Objects.isNull(data.get("page"))) {
				currentPage=(Integer) data.get("page");
			}
			int pageSize=20;
			if (!Objects.isNull(data.get("size"))) {
				pageSize=(Integer) data.get("size");
			}

			SysMenu entity = JSON.parseObject(JSON.toJSONString(data), SysMenu.class);
			Page<SysMenu> page = new Page(currentPage,pageSize);
			QueryWrapper<SysMenu> wrapper = new QueryWrapper(entity);
			wrapper.orderByDesc("update_date");
			IPage<SysMenu> selectPage = service.selectPage(page, wrapper);
			return new Result<>(Result.CODE_SUCCESS,"",selectPage.getRecords(),selectPage.getTotal());
		} catch (Exception e) {
			e.printStackTrace();
			return new Result<>(Result.CODE_FAILED,e.getMessage());
		}
	}


	@ApiOperation(value = "通过id获取实体类", notes = "通过id获取实体类。")
	@RequestMapping(value="/getById",method = RequestMethod.GET)
	public Result getById(@RequestParam(value="id") String id) {
		return new Result<>(Result.CODE_SUCCESS,"",service.getById(id));
	}

	@ApiOperation(value = "通过id获取实体类详细信息", notes = "通过id获取实体类详细信息。")
	@RequestMapping(value="/detail",method = RequestMethod.GET)
	public Result detail(@RequestParam(value="id") String id) {
		return new Result<>(Result.CODE_SUCCESS,"",service.getById(id));
	}

	@ApiOperation(value = "查询菜单树结构", notes = "查询菜单树结构。")
	@RequestMapping(value="/findMenuTree",method = RequestMethod.GET)
	public Result findMenuTree(@RequestParam(value = "name",required = false) String name) {
        if (StringUtils.isNotBlank(name)){
            List<MenuTree> result = Lists.newArrayList();
            // 1.匹配模糊name的结果
            List<MenuTree> targetObj = Lists.newArrayList();
            List<MenuTree> sysMenus = service.findMenuTree();
            for (MenuTree menuTree : sysMenus) {
                if (menuTree.getLabel().contains(name)) {
                    targetObj.add(menuTree);
                }
            }
            result.addAll(targetObj);

            // 2. 找出targetObj的所有父节点
//			List<SysOffice> parents = Lists.newArrayList();
            for (MenuTree menuTree : targetObj) {
                String parentIds = menuTree.getParentIds();//所有父节点id
                String[] split = parentIds.split(",");
                for (String s : split) {
                    for (MenuTree menu : sysMenus) {
                        if (menu.getId().equals(s) && !"1".equals(menu.getId()) && !result.contains(menu)) {
                            result.add(menu);
                        }
                    }
                }
            }
//			result.addAll(parents);

            // 3. 找出targetObj的所有子节点
            List<MenuTree> sub = Lists.newArrayList();
            for (MenuTree menuTree : targetObj) {
                String id = menuTree.getId();
                for (MenuTree menu : sysMenus) {
                    if (menu.getParentIds().contains(id) && !result.contains(menu)) {
                        result.add(menu);
                    }
                }
            }

            List<MenuTree> father = result.stream().filter(sysMenu -> "1".equals(sysMenu.getParentId())).collect(Collectors.toList());
            service.setSubChild(father,result);
            return new Result<>(Result.CODE_SUCCESS,"",result);
        }else{
            List<MenuTree> list = service.findMenuTree();
            List<MenuTree> father = list.stream().filter(menuTree -> "1".equals(menuTree.getParentId())).collect(Collectors.toList());
            service.setSubChild(father,list);
            return new Result<>(Result.CODE_SUCCESS,"",list);
        }
	}

	@ApiOperation(value = "获取子节点", notes = "获取子节点。")
	@RequestMapping(value="/findSubList",method = RequestMethod.POST)
	public Result findSubList(@RequestBody Map<String,String> data) {

		String id = data.get("id");
		QueryWrapper<SysMenu> queryWrapper = new QueryWrapper<>();
		queryWrapper.setEntity(null);
		queryWrapper.eq("parent_id",id);
		queryWrapper.eq("del_flag",0);
		queryWrapper.orderByAsc("sort");
		List<SysMenu> list = service.list(queryWrapper);

		return new Result<>(Result.CODE_SUCCESS,"",list);
	}

	@ApiOperation(value = "查询角色授权菜单树列表", notes = "查询角色授权菜单树列表。")
	@RequestMapping(value="/findRoleAuthorTree",method = RequestMethod.GET)
	public Result findMenuTreeList() {
			List<SysMenu> sysMenus = service.selectAll();
			List<SysMenu> father = sysMenus.stream().filter(sysMenu -> "1".equals(sysMenu.getParentId())).collect(Collectors.toList());
			service.setSubChild(father,sysMenus);
			return new Result<>(Result.CODE_SUCCESS,"",sysMenus);
	}


	@ApiOperation(value = "保存/修改", notes = "保存和修改方法")
	@RequestMapping(value = "/findPermissions", method = RequestMethod.GET)
	public Result findPermissions(){
		try {

			//获取当前登录用户
			LoginUser loginAppUser = UserUtils.getLoginAppUser();
			Set<SysRole> sysRoles = loginAppUser.getSysRoles();
			Set<String> roleIds = sysRoles.parallelStream().map(SysRole::getId).collect(Collectors.toSet());
			Set<SysMenu> permission = service.getPermissionByRoleIds(roleIds);
			Set<String> permissions = null;
			if (!CollectionUtils.isEmpty(permission)) {
				permissions = permission.parallelStream().map(SysMenu::getPerms)
						.collect(Collectors.toSet());
			}

			return new Result<>(Result.CODE_SUCCESS,"保存成功",permissions);
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
			return new Result<>(Result.CODE_FAILED,"保存失败");
		}
	}

	@ApiOperation(value = "查询当前用户菜单", notes = "查询当前用户菜单。")
	@RequestMapping(value="/findCurrentMenus",method = RequestMethod.GET)
	public Result findCurrentMenus(HttpServletRequest request){
		try {
			//获取当前登录用户
			LoginUser loginAppUser = UserUtils.getLoginAppUser();
			Set<SysRole> sysRoles = loginAppUser.getSysRoles();
			Set<String> roleIds = sysRoles.parallelStream().map(SysRole::getId).collect(Collectors.toSet());
			List<SysMenu> sysMenus = service.getMenuByRoleIds(roleIds);

			List<SysMenu> father = sysMenus.stream().filter(sysMenu -> "1".equals(sysMenu.getParentId())).collect(Collectors.toList());
			father.sort((a, b) -> a.getSort() - b.getSort());

//			Collections.sort(father, new Comparator<SysMenu>() {
//
//				@Override
//				public int compare(SysMenu o1, SysMenu o2) {
//					String sort = String.valueOf(Reflections.getFieldValue(o1, "sort"));
//					Integer sort1 = o1.getSort();
//					Integer sort2 = o2.getSort();
//					int i = sort1.compareTo(sort2);
//					return i;
//				}
//			});
			service.setSubChild(father,sysMenus);

			System.err.println(loginAppUser);
			return new Result<>(Result.CODE_SUCCESS,"",sysMenus);
		} catch (Exception e) {
			e.printStackTrace();
			return new Result<>(Result.CODE_FAILED,e.getMessage());
		}
	}


}

