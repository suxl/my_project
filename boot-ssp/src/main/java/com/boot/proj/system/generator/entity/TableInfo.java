package com.boot.proj.system.generator.entity;

import lombok.Data;

@Data
public class TableInfo {
    private String tableName;
    private String tableComment;
}
