package com.boot.proj.system.role.web;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.boot.proj.commons.base.entity.RoleMenu;
import com.boot.proj.commons.utils.Result;
import com.boot.proj.system.menu.entity.SysMenu;
import com.boot.proj.system.menu.service.SysMenuService;
import com.boot.proj.system.role.entity.SysRole;
import com.boot.proj.system.role.service.SysRoleService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.Objects;


/**
 * <p>
 * 角色表 前端控制器
 * </p>
 *
 * @author 苏小林
 * @since 2018-10-09
 */
@RestController
@RequestMapping("/system/sysRole")
public class SysRoleController {

	@Autowired
    public SysRoleService service;
	@Autowired
	private SysMenuService sysMenuService;
    
	
	@ApiOperation(value = "保存/修改", notes = "保存和修改方法")
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public Result save(@RequestBody SysRole data){
		try {
			service.save(data);
			return new Result<>(Result.CODE_SUCCESS,"保存成功");
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
			return new Result<>(Result.CODE_FAILED,"保存失败");
		}
	}

	@ApiOperation(value = "保存角色菜单", notes = "保存角色菜单")
	@RequestMapping(value = "/saveRoleMenus", method = RequestMethod.POST)
	public Result saveRoleMenus(@RequestBody List<RoleMenu> data){
		try {
			service.saveRoleMenu(data);
			return new Result<>(Result.CODE_SUCCESS,"保存成功");
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
			return new Result<>(Result.CODE_FAILED,"保存失败");
		}
	}
	
	
	@ApiOperation(value = "删除", notes = "通过id删除。")
	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public Result delete(@RequestParam(value="id") String id) {
		try {
			service.delById(id);
			service.delRoleMenu(id);
			return new Result<>(Result.CODE_SUCCESS,"删除成功");
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
			return new Result<>(Result.CODE_SUCCESS,"删除失败");
		}
		
	}

	@ApiOperation(value = "批量删除", notes = "批量删除。")
	@RequestMapping(value = "/batchDelete", method = RequestMethod.POST)
	public Result batchDelete(@RequestBody List<String> list) {
		try {
			service.removeByIds(list);
			for (String s : list) {
				service.delRoleMenu(s);
			}
			return new Result<>(Result.CODE_SUCCESS,"删除成功");
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
			return new Result<>(Result.CODE_FAILED,"删除失败");
		}
	}

	@ApiOperation(value = "分页查询list", notes = "带参数查询,{\"page\": 1,\"size\": 5,......}。支持模糊查询")
	@RequestMapping(value="/findList",method = RequestMethod.POST)
	public Result findList(@RequestBody Map<String,Object> data) {
		int currentPage=1;
		if (!Objects.isNull(data.get("page"))) {
			currentPage=(Integer) data.get("page");
		}
		int pageSize=20;
		if (!Objects.isNull(data.get("size"))) {
			pageSize=(Integer) data.get("size");
		}
		System.err.println("8754");

		SysRole entity = JSON.parseObject(JSON.toJSONString(data), SysRole.class);
		Page<SysRole> page = new Page(currentPage,pageSize);
		QueryWrapper<SysRole> wrapper = new QueryWrapper(entity);
		IPage<SysRole> selectPage = service.selectPage(page, wrapper);
		return new Result<>(Result.CODE_SUCCESS,"",selectPage.getRecords(),selectPage.getTotal());
	}

	@ApiOperation(value = "查询所有数据list", notes = "查询所有数据list。")
	@RequestMapping(value="/findAll",method = RequestMethod.POST)
	public Result findAll(){

		return new Result<>(Result.CODE_SUCCESS,"",service.selectAll());
	}
	
	
//	@ApiOperation(value = "通过id获取实体类", notes = "通过id获取实体类。")
//	@RequestMapping(value="/getById",method = RequestMethod.GET)
//	public Result getById(@RequestParam(value="id") String id) {
//		SysRole sysRole = service.getById(id);
//		List<RoleMenu> list=service.findRoleMenu(id);
//		if (!list.isEmpty()) {
////			List<String> objects = Lists.newArrayList();
////			list.forEach(roleMenu -> {
////				objects.add(roleMenu.getMenuId());
////			});
////			sysRole.setMenuIds(objects);
//
//			String echoMenuIds = sysRole.getEchoMenuIds();
//			if (StringUtils.isNotBlank(echoMenuIds)){
//				List<String> strings = Arrays.asList(echoMenuIds.split(","));
//				sysRole.setMenuIds(strings);
//				String menuNames="";
//				Collection<SysMenu> sysMenus = sysMenuService.listByIds(strings);
//				for (SysMenu sysMenu : sysMenus) {
//					menuNames+=","+sysMenu.getName();
//				}
////			System.err.println( StringUtils.isBlank(menuNames));
//				sysRole.setMenuNames(StringUtils.isBlank(menuNames)?"":menuNames.substring(1));
//			}
//
//
//		}
//		return new Result<>(Result.CODE_SUCCESS,"",sysRole);
//	}

	@ApiOperation(value = "通过id获取实体类详细信息", notes = "通过id获取实体类详细信息。")
	@RequestMapping(value="/detail",method = RequestMethod.GET)
	public Result detail(@RequestParam(value="id") String id) {
		SysRole sysRole = service.getById(id);


		return new Result<>(Result.CODE_SUCCESS,"",sysRole);
	}

	@ApiOperation(value = "查询角色菜单", notes = "查询角色菜单。")
	@RequestMapping(value="/findRoleMenus",method = RequestMethod.GET)
	public Result findRoleMenus(@RequestParam(value="roleId") String roleId) {
		List<SysMenu> list = sysMenuService.findRoleMenu(roleId);
		return new Result<>(Result.CODE_SUCCESS,"",list);
	}
    

}

