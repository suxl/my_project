package com.boot.proj.system.generator.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.boot.proj.system.dict.entity.SysDict;
import com.boot.proj.system.generator.entity.Generator;
import com.boot.proj.system.generator.entity.TableInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface GeneratorMapper extends BaseMapper<Generator> {

    //查询所有的表名称
    List<TableInfo> getAllTables(@Param("dbName") String dbName,@Param("tableName") String tableName);

    //根据表名查询出属性
    List<Generator> getAttrByTable(@Param("dbName") String dbName, @Param("tableName") String tableName);
}
