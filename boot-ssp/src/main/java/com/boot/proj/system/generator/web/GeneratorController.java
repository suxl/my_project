package com.boot.proj.system.generator.web;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.boot.proj.commons.utils.Result;
import com.boot.proj.system.generator.config.*;
import com.boot.proj.system.generator.entity.Generator;
import com.boot.proj.system.generator.entity.TableInfo;
import com.boot.proj.system.generator.service.GeneratorService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/users-anon/generator")
public class GeneratorController {
    @Value("${spring.datasource.druid.url}")
    private String DbUrl;
    @Value("${spring.datasource.druid.username}")
    private String userName;
    @Value("${spring.datasource.druid.password}")
    private String password;
    @Value("${spring.datasource.druid.driver-class-name}")
    private String driverClassName;
    @Value("${code-generator.supper-entity-class}")
    private String supperEntityClass;
    @Value("${code-generator.supper-service-class}")
    private String supperServiceClass;
    @Value("${code-generator.supper-mapper-class}")
    private String supperMapperClass;
    @Value("${code-generator.author}")
    private String author;
    @Value("${code-generator.output-dir}")
    private String outputDir;
    /**
     * 是否强制带上注解
     */
    static boolean enableTableFieldAnnotation = false;
    /**
     * 生成的注解带上IdType类型
     */
    static IdType tableIdType = null;
    /**
     * 是否去掉生成实体的属性名前缀
     */
    static String[] fieldPrefix = null;
    /**
     * 生成的Service 接口类名是否以I开头
     * 默认是以I开头
     * user表 -> IUserService, UserServiceImpl
     */
    static boolean serviceClassNameStartWithI = false;

    @Autowired
    private GeneratorService generatorService;

    /**
     * 单表生成代码
     */
    @ApiOperation(value = "查询所有表名称", notes = "查询所有表名称")
    @RequestMapping(value = "/getAllTables",method = RequestMethod.POST)
    public Result getAllTables(@RequestBody Map<String,String> data){
        List<TableInfo> list = null;
        try {
            String dbName = data.get("dbName");
            String tableName = data.get("tableName");
            list = generatorService.getAllTables(dbName,tableName);
            Iterator<TableInfo> iterator = list.iterator();
            while (iterator.hasNext()){
                TableInfo tableInfo = iterator.next();
                if (tableInfo.getTableName().startsWith("qrtz") || tableInfo.getTableName().startsWith("QRTZ")){
                    iterator.remove();
                }
            }
            return new Result<>(Result.CODE_SUCCESS,"成功",list);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result<>(Result.CODE_FAILED,"失败",e.getMessage());
        }
    }

    /**
     * 通过表名获取属性
     */
    @ApiOperation(value = "查询所有表名称", notes = "查询所有表名称")
    @RequestMapping(value = "/getAttrByTable",method = RequestMethod.POST)
    public Result getAttrByTable(@RequestBody Map<String,String> data){
        try {
            String dbName = data.get("dbName");
            String tableName = data.get("tableName");

            List<Generator> generator = generatorService.getAttrByTable(dbName,tableName);
            return new Result<>(Result.CODE_SUCCESS,"成功",generator);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result<>(Result.CODE_FAILED,"失败",e.getMessage());
        }
    }


    /**
     * 单表生成代码
     */
    @ApiOperation(value = "生成单表代码", notes = "生成单表代码")
    @RequestMapping(value = "/single",method = RequestMethod.POST)
    public Result gengratorSingle(@RequestBody Map<String,String> data){
        try {
            String packageName = data.get("packageName");
            String moduleName = data.get("moduleName");
            String outputDir = data.get("outputDir");
            String tableName = data.get("tableName");
            MyGlobalConfig config = new MyGlobalConfig();
            DataSourceConfig dataSourceConfig = new DataSourceConfig();
            dataSourceConfig.setDbType(DbType.MYSQL)
                    .setUrl(DbUrl)
                    .setUsername(userName)
                    .setPassword(password)
                    .setDriverName(driverClassName);
            //策略配置
            StrategyConfig strategyConfig = new StrategyConfig();
            strategyConfig
                    .setVersionFieldName("version")
                    .setCapitalMode(true)
                    .setColumnNaming(NamingStrategy.underline_to_camel)
    //                .setDbColumnUnderline(true)
                    .setNaming(NamingStrategy.underline_to_camel)
                    .setSuperServiceImplClass(null)
                    // 【实体】是否为lombok模型（默认 false）<a href="https://projectlombok.org/">document</a>
                    .setEntityLombokModel(true)
                    //数据库表映射到实体的命名策略
    //                .setNaming(NamingStrategy.underline_to_camel)
                    //数据库表字段映射到实体的命名策略, 未指定按照 naming 执行
    //                .setColumnNaming(NamingStrategy.underline_to_camel)
                    // 自定义实体父类
                    .setSuperEntityClass(supperEntityClass)
                    //自定义基础的Entity类，公共字段
                    .setSuperEntityColumns("id", "create_by","create_name", "create_date", "update_by","update_name", "update_date", "del_flag", "remarks")

                    // 自定义 service 父类
                    .setSuperServiceClass(supperServiceClass)
                    //自定义Mapper 父类
                    .setSuperMapperClass(supperMapperClass)
                    .setRestControllerStyle(true)
    //                .fieldPrefix(fieldPrefix)//test_id -> id, test_type -> type
                    .setInclude(tableName);//修改替换成你需要的表名，多个表名传数组
            config
                    .setViewName("%s")
                    .setIdType(tableIdType)
                    .setAuthor(author)
                    .setBaseResultMap(true)
                    .setOutputDir(outputDir)
                    .setFileOverride(true)
    //                .setBaseColumnList(true)
                    // 自定义文件命名，注意 %s 会自动填充表实体属性！
                    .setXmlName("%sMapper")
                    .setMapperName("%sMapper");
            if (!serviceClassNameStartWithI) {
                config.setServiceName("%sService");
            }
            MyTemplateConfig myTemplateConfig = new MyTemplateConfig();

            myTemplateConfig.setView("/templates/view.vue");
            myTemplateConfig.setXml("/templates/mapper.xml");
            myTemplateConfig.setController("/templates/controller.java");
            myTemplateConfig.setService("/templates/service.java");
            myTemplateConfig.setServiceImpl(null); //不生成实现类
            myTemplateConfig.setEntity("/templates/entity.java");
            myTemplateConfig.setMapper("/templates/mapper.java");//注意：不要带上.vm
            InjectionConfig injectionConfig = new InjectionConfig() {
                @Override
                public void initMap() {//自定义参数
                    Map<String, Object> map = new HashMap<>();
                    map.put("abc", this.getConfig().getGlobalConfig().getAuthor() + "-mp");
                    this.setMap(map);
                }
            };

            MyPackageConfig myPackageConfig = new MyPackageConfig();
            myPackageConfig.setView("view");
            myPackageConfig.setModuleName(moduleName);
            myPackageConfig.setParent(packageName);
            myPackageConfig.setController("web");
            myPackageConfig.setEntity("entity");
            myPackageConfig.setMapper("dao");
            myPackageConfig.setXml("mapper");

            new AutoGenerator().setGlobalConfig(config)
                    .setTemplate(myTemplateConfig)//自定义模板路径
                    .setTemplateEngine(new FreemarkerTemplateEngine()) //设置模板引擎
                    .setCfg(injectionConfig)
                    .setDataSource(dataSourceConfig)
                    .setStrategy(strategyConfig)
                    .setPackageInfo(myPackageConfig).execute();
            return new Result<>(Result.CODE_SUCCESS,"成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result<>(Result.CODE_FAILED,"失败",e.getMessage());
        }
    }

}
