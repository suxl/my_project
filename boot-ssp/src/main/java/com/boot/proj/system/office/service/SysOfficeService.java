package com.boot.proj.system.office.service;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.boot.proj.commons.base.BaseService;
import com.boot.proj.system.office.dao.SysOfficeMapper;
import com.boot.proj.system.office.entity.SysOffice;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * $!{table.comment} 服务实现类
 * </p>
 *
 * @author 苏小林
 * @since 2019-06-22
 */
@Service
@Transactional(rollbackFor = Exception.class)
@CacheConfig(cacheNames = "SysOffice")
public class SysOfficeService extends BaseService<SysOfficeMapper, SysOffice> {
    @Autowired
    private SysOfficeMapper dao;

    @Override
    @CacheEvict(cacheNames="SysOffice", allEntries=true)
    public boolean save(SysOffice entity) {
        return super.save(entity);
    }

    @Override
    @Cacheable(key = "#id",sync = true)
    public SysOffice getById(Serializable id) {
        return super.getById(id);
    }

    @Override
    @CacheEvict(cacheNames="SysOffice", allEntries=true)
    public boolean removeById(Serializable id) {
        return super.removeById(id);
    }

    @Cacheable(key = "#data",sync = true)
    public IPage<SysOffice> selectPage(Map<String,Object> data) {
        int currentPage=1;
        if (!Objects.isNull(data.get("page"))) {
            currentPage=(Integer) data.get("page");
        }
        int pageSize=20;
        if (!Objects.isNull(data.get("size"))) {
            pageSize=(Integer) data.get("size");
        }

        SysOffice entity = JSON.parseObject(JSON.toJSONString(data), SysOffice.class);
        Page<SysOffice> page = new Page(currentPage,pageSize);
        QueryWrapper<SysOffice> wrapper = new QueryWrapper(entity);
        return super.selectPage(page, wrapper);
    }

    @Override
    @Cacheable(value = "SysOffice",sync = true)
    public List<SysOffice> selectAll() {
        QueryWrapper<SysOffice> wrapper = new QueryWrapper<SysOffice>();
        wrapper.setEntity(null);
        wrapper.ne("id","1");
        return dao.selectList(wrapper);
    }

    @Override
    @CacheEvict(cacheNames="SysOffice", allEntries=true)
    public boolean removeByIds(Collection<? extends Serializable> idList) {
        return super.removeByIds(idList);
    }

    public void setSubChild(List<SysOffice> father, List<SysOffice> sysOffices) {
        for (SysOffice sysOffice : father) {
            List<SysOffice> subChild = getSubChild(sysOffice,sysOffices);
            if (!CollectionUtils.isEmpty(subChild)){
                sysOffice.setChildren(subChild);
                sysOffices.removeAll(subChild);
                setSubChild(subChild,sysOffices);
            }
        }
    }

    private List<SysOffice> getSubChild(SysOffice sysOffice, List<SysOffice> sysOffices) {
        List<SysOffice> result = Lists.newArrayList();
        for (SysOffice office : sysOffices) {
            if (sysOffice.getId().equals(office.getParentId())){
                result.add(office);
            }
        }
        return result;
    }

    public Integer getMaxSort() {
        return dao.getMaxSort();
    }
}
