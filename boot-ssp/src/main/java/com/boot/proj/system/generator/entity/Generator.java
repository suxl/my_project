package com.boot.proj.system.generator.entity;

import com.boot.proj.commons.base.BaseEntity;
import lombok.Data;

@Data
public class Generator extends BaseEntity<Generator> {

    private String columnName;
    private String columnComment;
    private String dataType;
    private String characterMaximumLength;
}
