package com.boot.proj.system.generator.config;


/**
 * @author suxiaolin
 * @date 2019/6/28 12:18
 */
public abstract class FileOutConfig {
    private String templatePath;

    public FileOutConfig() {
    }

    public FileOutConfig(String templatePath) {
        this.templatePath = templatePath;
    }

    public abstract String outputFile(TableInfo tableInfo);

    public String getTemplatePath() {
        return this.templatePath;
    }

    public FileOutConfig setTemplatePath(final String templatePath) {
        this.templatePath = templatePath;
        return this;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof FileOutConfig)) {
            return false;
        } else {
            FileOutConfig other = (FileOutConfig)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                Object this$templatePath = this.getTemplatePath();
                Object other$templatePath = other.getTemplatePath();
                if (this$templatePath == null) {
                    if (other$templatePath != null) {
                        return false;
                    }
                } else if (!this$templatePath.equals(other$templatePath)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof FileOutConfig;
    }

    public int hashCode() {
        Boolean PRIME = true;
        int result = 1;
        Object $templatePath = this.getTemplatePath();
        result = result * 59 + ($templatePath == null ? 43 : $templatePath.hashCode());
        return result;
    }

    public String toString() {
        return "FileOutConfig(templatePath=" + this.getTemplatePath() + ")";
    }
}
