package com.boot.proj.system.user.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.boot.proj.commons.base.entity.UserRole;
import com.boot.proj.system.user.entity.SysUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author suxl
 * @since 2018-09-28
 */
@Mapper
public interface SysUserMapper extends BaseMapper<SysUser> {

	List<SysUser> findList();

	SysUser getByUserName(@Param("username") String username);

    void saveUserRole(@Param("userRoles") List<UserRole> userRoles);

	void delUserRole(@Param("userId") String userId);

	@Override
	int updateById(SysUser entity);

	//通过用户id查询用户角色
    List<UserRole> findUserRole(@Param("userId") String userId);

	void updatePwd(SysUser byUserName);
}
