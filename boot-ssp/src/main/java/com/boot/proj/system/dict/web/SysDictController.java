package com.boot.proj.system.dict.web;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.boot.proj.commons.utils.DateUtils;
import com.boot.proj.commons.utils.Result;
import com.boot.proj.commons.utils.excel.ExportExcel;
import com.boot.proj.system.dict.entity.SysDict;
import com.boot.proj.system.dict.service.SysDictService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 数据字典 前端控制器
 * </p>
 *
 * @author 苏小林
 * @since 2019-07-18
 */
@RestController
@RequestMapping("/dict/sysDict")
@Api("数据字典")
public class SysDictController {

	@Autowired
    public SysDictService service;

	@ApiOperation(value = "数据字典,保存/修改", notes = "数据字典,保存和修改方法")
	@ApiImplicitParam(name = "data", value = "字典所有属性", required = true, dataType = "SysDict")
	@RequestMapping(value = "/save", method = RequestMethod.POST)
    @PreAuthorize("hasAuthority('sys:dict:edit')")
	public Result save(@RequestBody SysDict data){
    	try {
    		service.save(data);
        	return new Result<>(Result.CODE_SUCCESS,"保存成功");
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
        	return new Result<>(Result.CODE_FAILED,"保存失败");
		}

	}


	@ApiOperation(value = "数据字典删除", notes = "通过id删除数据字典信息。")
	@RequestMapping(value = "/delete", method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('sys:dict:delete')")
	public Result delete(@ApiParam(value = "字典id") @RequestParam(value="id") String id,
						 @ApiParam(value = "字典名称") @RequestParam(value="name") String name) {
		try {
			service.removeById(id);
        	return new Result<>(Result.CODE_SUCCESS,"删除成功");
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
            return new Result<>(Result.CODE_FAILED,"删除失败");
		}

	}

	@ApiOperation(value = "批量删除", notes = "批量删除。")
	@ApiImplicitParam(name = "list", value = "{\"id1\": \"\",\"id2\": \"\"...}", required = true, dataType = "List")
	@RequestMapping(value = "/batchDelete", method = RequestMethod.POST)
	public Result batchDelete(@RequestBody List<String> list) {
		try {
			service.removeByIds(list);
			return new Result<>(Result.CODE_SUCCESS,"删除成功");
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
			return new Result<>(Result.CODE_FAILED,"删除失败");
		}
    }

	@ApiImplicitParam(name = "data", value = "字典信息的属性名值对组成的Map:{\"page\": 1,\"size\": 5}", required = true, dataType = "Map")
	@ApiOperation(value = "分页查询数据字典list", notes = "带参数查询,{\"page\": 1,\"size\": 5,......}。支持模糊查询")
	@RequestMapping(value="/findList",method = RequestMethod.POST)
    @PreAuthorize("hasAuthority('sys:dict:view')")
	public Result findList(@RequestBody Map<String,Object> data) {
        try {
        	IPage<SysDict> selectPage = service.selectPage(data);
        	return new Result<>(Result.CODE_SUCCESS,"",selectPage.getRecords(),selectPage.getTotal());
        } catch (Exception e) {
        	e.printStackTrace();
        	return new Result<>(Result.CODE_FAILED,"查询异常",e.getMessage());
        }
	}

	@ApiOperation(value = "查询数据字典所有数据list", notes = "查询数据字典所有数据list。")
	@RequestMapping(value="/findAll",method = RequestMethod.POST)
    @PreAuthorize("hasAuthority('sys:dict:view')")
	public Result findAll(){
        return new Result<>(Result.CODE_SUCCESS,"",service.selectAll());
	}


	@ApiOperation(value = "通过id获取数据字典实体类", notes = "通过id获取数据字典实体类。")
	@RequestMapping(value="/getById",method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('sys:dict:view')")
	public Result getById(@RequestParam(value="id") String id) {
        return new Result<>(Result.CODE_SUCCESS,"",service.getById(id));
	}

	@ApiOperation(value = "通过id获取实体类详细信息", notes = "通过id获取实体类详细信息。")
	@RequestMapping(value="/detail",method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('sys:dict:view')")
	public Result detail(@RequestParam(value="id") String id) {
        return new Result<>(Result.CODE_SUCCESS,"",service.getById(id));
	}


	@ApiOperation(value = "导出数据字典excel", notes = "导出数据字典excel。")
	@RequestMapping(value="/export",method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('sys:dict:export')")
	public String export(HttpServletRequest request, HttpServletResponse response){
        try {
        String fileName = "数据字典" + DateUtils.formatDateTime(new Date()) + ".xlsx";

        List<SysDict> list=service.selectAll();
        ExportExcel exportExcel =new ExportExcel("数据字典", SysDict.class);
        exportExcel.setDatePattern("yyyy-MM-dd HH:mm:ss");
        exportExcel.setDataList(list).write(response, fileName).dispose();

        return null;
        } catch (Exception e) {
        e.printStackTrace();
        }
        return null;
	}


}

