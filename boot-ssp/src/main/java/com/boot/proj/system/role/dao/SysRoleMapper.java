package com.boot.proj.system.role.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.boot.proj.commons.base.entity.RoleMenu;
import com.boot.proj.system.role.entity.SysRole;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Set;


/**
 * <p>
 * 角色表 Mapper 接口
 * </p>
 *
 * @author 苏小林
 * @since 2018-10-09
 */
@Mapper
public interface SysRoleMapper extends BaseMapper<SysRole> {

	/**
	 * 通过用户id查询角色
	 * @param userId
	 * @return
	 */
	Set<SysRole> getRoleByUserId(@Param("userId") String userId);

	/**
	 * 保存角色菜单
	 * @param menuList
	 */
    void saveRoleMenu(@Param("menuList") List<RoleMenu> menuList);


	/**
	 * 删除角色菜单
	 * @param roleId
	 */
	void deleteByRoleId(@Param("roleId") String roleId);

}
