package com.boot.proj.system.dict.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.boot.proj.commons.base.BaseEntity;
import com.boot.proj.commons.utils.excel.annotation.ExcelField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * <p>
 * $!{table.comment}
 * </p>
 *
 * @author 苏小林
 * @since 2019-07-17
 */
@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_dict")
@Table(name="sys_dict")
@org.hibernate.annotations.Table(appliesTo = "sys_dict",comment="数据字典")
public class SysDict extends BaseEntity<SysDict> {

    private static final long serialVersionUID = 1L;

    /**
     * 描述
     */
    @TableField("description")
    @ExcelField(title="描述", type = ExcelField.TYPE_IMPORT_EXPORT, align= ExcelField.ALIGN_CENTER, sort = 1)
    @Column(columnDefinition = "varchar(64) COMMENT '描述'")
    private String description;

    /**
     * 标签名
     */
    @TableField("label")
    @ExcelField(title="标签名", type = ExcelField.TYPE_IMPORT_EXPORT, align= ExcelField.ALIGN_CENTER, sort = 1)
    @Column(columnDefinition = "varchar(64) COMMENT '标签名'")
    private String label;

    /**
     * 排序
     */
    @TableField("sort")
    @ExcelField(title="排序", type = ExcelField.TYPE_IMPORT_EXPORT, align= ExcelField.ALIGN_CENTER, sort = 1)
    @Column(columnDefinition = "int(11) COMMENT '排序'")
    private Integer sort;

    /**
     * 类型
     */
    @TableField("type")
    @ExcelField(title="类型", type = ExcelField.TYPE_IMPORT_EXPORT, align= ExcelField.ALIGN_CENTER, sort = 1)
    @Column(columnDefinition = "varchar(64) COMMENT '类型'")
    private String type;

    /**
     * 数据值
     */
    @TableField("value")
    @ExcelField(title="数据值", type = ExcelField.TYPE_IMPORT_EXPORT, align= ExcelField.ALIGN_CENTER, sort = 1)
    @Column(columnDefinition = "varchar(10) COMMENT '数据值'")
    private String value;


    public SysDict(String description, String label, String type, String value) {
        this.description = description;
        this.label = label;
        this.type = type;
        this.value = value;
    }


    public SysDict() {
    }
}
