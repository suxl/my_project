package com.boot.proj.system.login;

import com.boot.proj.commons.base.entity.LoginUser;
import com.boot.proj.commons.enums.CredentialType;
import com.boot.proj.commons.utils.RSACoder;
import com.boot.proj.commons.utils.Result;
import com.boot.proj.config.IpConfiguration;
import com.boot.proj.security.oauth.ClientInfo;
import com.boot.proj.system.user.service.SysUserService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.token.ConsumerTokenServices;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * 登录管理
 * @author suxiaolin
 * @date 2019/7/11 9:55
 */
@RestController
@Slf4j
public class LoginController {
    @Value("${rsa.privateKey}")
    private String privateKey;

    @Autowired
    private IpConfiguration ipConfiguration;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private ConsumerTokenServices tokenServices;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    private SysUserService sysUserService;

    /**
     * 当前登陆用户信息<br>
     * <p>
     * security获取当前登录用户的方法是SecurityContextHolder.getContext().getAuthentication()<br>
     * 返回值是接口org.springframework.security.core.Authentication，又继承了Principal<br>
     * 这里的实现类是org.springframework.security.oauth2.provider.OAuth2Authentication<br>
     * <p>
     * 因此这只是一种写法，下面注释掉的三个方法也都一样，这四个方法任选其一即可，也只能选一个，毕竟uri相同，否则启动报错<br>
     * 2018.05.23改为默认用这个方法，好理解一点
     *
     * @return
     */
    @GetMapping("/user-me")
    @ApiOperation(value = "查询当前登录用户", notes = "查询当前登录用户")
    public Authentication principal() {
        System.err.println("查询当前用户");
        System.err.println("当前的端口号:"+ipConfiguration.getPort());
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        log.debug("user-me:{}", authentication.getName());
        return authentication;
    }

    @RequestMapping(value = "/users-anon/login",method = RequestMethod.POST)
    @ApiOperation(value = "登录接口", notes = "登录接口")
    public Result login(@RequestBody Map<String, String> parameters, HttpServletResponse response) {
        try {
            String password = RSACoder.decrypt(parameters.get("password"), privateKey);
//            String password = parameters.get("password");

//            String url="http://localhost:"+ipConfiguration.getPort()+"/oauth/token"
            String url="http://localhost:"+ipConfiguration.getPort()+"/oauth/token"
                    +"?username="+ parameters.get("username")
                    + "&password=" +  password
                    + "&grant_type="+ ClientInfo.GRANT_TYPE
                    + "&scope="+ ClientInfo.CLIENT_SCOPE
                    + "&client_id=" + ClientInfo.CLIENT_ID
                    + "&client_secret=" + ClientInfo.CLIENT_SECRET
                    + "&auth_type=" + CredentialType.password;

            LoginUser loginAppUser = sysUserService.getByUserName(parameters.get("username"));
            if (loginAppUser!=null){
                if (!passwordEncoder.matches(password,loginAppUser.getPassword())) {
                    response.sendError(506,"用户名或密码错误");//定义506为用户名或密码错误
                    response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
                    throw new Exception("用户名或密码错误");
                }
            }else{
                response.sendError(507,"用户不存在");//定义507为用户不存在
                throw new Exception("用户不存在");
            }
            System.err.println(url);

            ResponseEntity<Object> objectResponseEntity = restTemplate.postForEntity(url, null, Object.class);
            Map<String,Object> map = (Map<String,Object>)objectResponseEntity.getBody();
            return new Result<>(Result.CODE_SUCCESS,"登录成功",map);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result<>(Result.CODE_FAILED,"登录失败",e.getMessage());
        }

    }


    @RequestMapping(value = "/users-anon/mobile/login",method = RequestMethod.POST)
    @ApiOperation(value = "登录接口", notes = "登录接口")
    public Result mobileLogin(@RequestBody Map<String, String> parameters, HttpServletResponse response) {


        try {
            String password = RSACoder.decrypt(parameters.get("password"), privateKey);
//            String password = parameters.get("password");

            String url="http://localhost:"+ipConfiguration.getPort()+"/oauth/token"
                    +"?username="+ parameters.get("username")
                    + "&password=" +  password
                    + "&grant_type="+ ClientInfo.GRANT_TYPE
                    + "&scope="+ ClientInfo.CLIENT_SCOPE
                    + "&client_id=" + ClientInfo.CLIENT_ID
                    + "&client_secret=" + ClientInfo.CLIENT_SECRET
                    + "&auth_type=" + CredentialType.sms;

//            LoginUser loginAppUser = sysUserService.getByUserName(parameters.get("username"));
//            if (loginAppUser!=null){
//                if (!passwordEncoder.matches(password,loginAppUser.getPassword())) {
//                    response.sendError(506,"用户名或密码错误");//定义506为用户名或密码错误
//                    response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
//                    throw new Exception("用户名或密码错误");
//                }
//            }else{
//                response.sendError(507,"用户不存在");//定义507为用户不存在
//                throw new Exception("用户不存在");
//            }
            System.err.println(url);

            ResponseEntity<Object> objectResponseEntity = restTemplate.postForEntity(url, null, Object.class);
            Map<String,Object> map = (Map<String,Object>)objectResponseEntity.getBody();
            return new Result<>(Result.CODE_SUCCESS,"登录成功",map);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result<>(Result.CODE_FAILED,"登录失败",e.getMessage());
        }

    }

    @RequestMapping(value = "/users-anon/qq/login",method = RequestMethod.GET)
    @ApiOperation(value = "登录接口", notes = "登录接口")
    public Result mobileLogin(HttpServletResponse response) {


        try {
//            String password = RSACoder.decrypt(parameters.get("password"), privateKey);

            String url="http://localhost:"+ipConfiguration.getPort()+"/login/qq";

//            LoginUser loginAppUser = sysUserService.getByUserName(parameters.get("username"));
//            if (loginAppUser!=null){
//                if (!passwordEncoder.matches(password,loginAppUser.getPassword())) {
//                    response.sendError(506,"用户名或密码错误");//定义506为用户名或密码错误
//                    response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
//                    throw new Exception("用户名或密码错误");
//                }
//            }else{
//                response.sendError(507,"用户不存在");//定义507为用户不存在
//                throw new Exception("用户不存在");
//            }
            System.err.println(url);

            ResponseEntity<Object> objectResponseEntity = restTemplate.postForEntity(url, null, Object.class);
            Map<String,Object> map = (Map<String,Object>)objectResponseEntity.getBody();
            return new Result<>(Result.CODE_SUCCESS,"登录成功",map);
        } catch (Exception e) {

            e.printStackTrace();
            return new Result<>(Result.CODE_FAILED,"登录失败",e.getMessage());
        }

    }

    /**
     * 退出
     *
     * @param access_token
     */
    @GetMapping("/users-anon/logout")
    public void logout(String access_token, @RequestHeader(required = false, value = "Authorization") String token) {
        if (StringUtils.isBlank(access_token)) {
            if (StringUtils.isNoneBlank(token)) {
                access_token = token.substring(OAuth2AccessToken.BEARER_TYPE.length() + 1);
            }
        }
        boolean flag = tokenServices.revokeToken(access_token);
        if (flag) {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        }
    }

}
