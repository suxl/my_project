package com.boot.proj.system.office.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.boot.proj.commons.base.BaseEntity;
import com.boot.proj.commons.utils.excel.annotation.ExcelField;
import com.google.common.collect.Lists;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.List;

/**
 * <p>
 * $!{table.comment}
 * </p>
 *
 * @author 苏小林
 * @since 2019-06-22
 */
@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_office")
@Table(name="sys_office")
@org.hibernate.annotations.Table(appliesTo = "sys_office",comment="机构管理")
public class SysOffice extends BaseEntity<SysOffice> {

    private static final long serialVersionUID = 1L;

    /**
     * 机构名称
     */
    @TableField("name")
    @ExcelField(title="机构名称", type = ExcelField.TYPE_IMPORT_EXPORT, align= ExcelField.ALIGN_CENTER, sort = 1)
    @Column(columnDefinition = "varchar(50) COMMENT '机构名称'")
    private String name;

    /**
     * 上级机构ID，一级机构为1
     */
    @TableField("parent_id")
    @ExcelField(title="上级机构ID，一级机构为0", type = ExcelField.TYPE_IMPORT_EXPORT, align= ExcelField.ALIGN_CENTER, sort = 1)
    @Column(columnDefinition = "varchar(64) COMMENT '上级机构ID，一级机构为1'")
    private String parentId;

    /**
     * 上级机构名称
     */
    @TableField("parent_name")
    @ExcelField(title="上级机构名称", type = ExcelField.TYPE_IMPORT_EXPORT, align= ExcelField.ALIGN_CENTER, sort = 1)
    @Column(columnDefinition = "varchar(64) COMMENT '上级机构name'")
    private String parentName;

    /**
     * 所有父节点
     */
    @TableField("parent_ids")
    @ExcelField(title="所有父节点", type = ExcelField.TYPE_IMPORT_EXPORT, align= ExcelField.ALIGN_CENTER, sort = 1)
    @Column(columnDefinition = "varchar(2000) COMMENT '所有父节点'")
    private String parentIds;

    /**
     * 排序
     */
    @TableField("sort")
    @ExcelField(title="排序", type = ExcelField.TYPE_IMPORT_EXPORT, align= ExcelField.ALIGN_CENTER, sort = 1)
    @Column(columnDefinition = "int(11) COMMENT '排序'")
    private Integer sort;

    /**
     * 子机构
     */
    @TableField(exist = false)
    @Transient
    private List<SysOffice> children= Lists.newArrayList();


}
