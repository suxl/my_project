package com.boot.proj.system.role.service;

import com.boot.proj.commons.base.BaseService;
import com.boot.proj.commons.base.entity.RoleMenu;
import com.boot.proj.system.role.dao.SysRoleMapper;
import com.boot.proj.system.role.entity.SysRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 角色表 服务实现类
 * </p>
 *
 * @author 苏小林
 * @since 2018-10-09
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class SysRoleService extends BaseService<SysRoleMapper, SysRole> {

    @Autowired
    private SysRoleMapper sysRoleMapper;

    @Override
    public boolean save(SysRole entity) {

//        Boolean flag=true;
//        if (StringUtils.isNotBlank(entity.getId())) {//表示修改
//            SysRole sysRole = sysRoleMapper.selectById(entity.getId());
//            String echoMenuIds = sysRole.getEchoMenuIds();
//            if (StringUtils.isNotBlank(echoMenuIds) && echoMenuIds.equals(entity.getEchoMenuIds())){
//                flag=false;
//            }
//        }
//        List<String> menuIds = entity.getMenuIds();
        super.save(entity);

//        if (flag){
//            //删除角色菜单
//            this.delRoleMenu(entity.getId());
//
//            //保存角色菜单
//            List<RoleMenu> menuList = Lists.newArrayList();
//            for (String s : menuIds) {
//                RoleMenu roleMenu = new RoleMenu();
//                roleMenu.setRoleId(entity.getId());
//                roleMenu.setMenuId(s);
//                menuList.add(roleMenu);
//            }
//            String aa= null;
//            if (aa.equals("5")) {
//                System.err.println("99");
//            }
//            this.saveRoleMenu(menuList);
//        }

        return true;
    }

    /**
     * 删除角色菜单
     * @param roleId
     */
    public void delRoleMenu(String roleId) {
        sysRoleMapper.deleteByRoleId(roleId);
    }

    /**
     * 保存角色菜单
     * @param menuList
     */
    public void saveRoleMenu(List<RoleMenu> menuList) {
        for (RoleMenu roleMenu : menuList) {
            String roleId = roleMenu.getRoleId();
            sysRoleMapper.deleteByRoleId(roleId);
            break;
        }
        sysRoleMapper.saveRoleMenu(menuList);
    }


    /**
     * 通过id 删除角色
     * @param id
     */
    public void delById(String id) {
        super.removeById(id);
        this.delRoleMenu(id);
    }

}
