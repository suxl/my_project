package com.boot.proj.system.user.service;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.boot.proj.commons.base.BaseService;
import com.boot.proj.commons.base.entity.LoginUser;
import com.boot.proj.commons.base.entity.UserRole;
import com.boot.proj.system.menu.entity.SysMenu;
import com.boot.proj.system.menu.service.SysMenuService;
import com.boot.proj.system.role.dao.SysRoleMapper;
import com.boot.proj.system.role.entity.SysRole;
import com.boot.proj.system.user.dao.SysUserMapper;
import com.boot.proj.system.user.entity.SysUser;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author suxl
 * @since 2018-09-28
 */
@Service
public class SysUserService extends BaseService<SysUserMapper, SysUser> {
	
	private static Logger log=LoggerFactory.getLogger(SysUserService.class);

	@Autowired
	private SysUserMapper userDao;
	@Autowired
	private SysRoleMapper roleDao;
    @Autowired
	private SysMenuService sysMenuService;


    /**
     * 通过用户名查询用户
     * @param username
     * @return
     */
	public LoginUser getByUserName(String username) {
		//根据用户名查询用户
		SysUser user=userDao.getByUserName(username);
        if (user ==null){
            //后台抛出的异常是：org.springframework.security.authentication.BadCredentialsException: Bad credentials  坏的凭证 如果要抛出UsernameNotFoundException 用户找不到异常则需要自定义重新它的异常
            log.info("登录用户：" + username + " 不存在.");
            return null;
        }else{

            LoginUser loginUser = this.findUserRoleMenu(user);
            return loginUser;
        }
	}

    /**
     * 通过手机号码查询用户
     * @param phone
     * @return
     */
    public LoginUser getByPhone(String phone) {
        //根据用户名查询用户
        QueryWrapper<SysUser> queryWrapper = new QueryWrapper<>();
        queryWrapper.setEntity(null);
        queryWrapper.eq("phone",phone);
        SysUser user = this.getOne(queryWrapper);
        if (user ==null){
            //后台抛出的异常是：org.springframework.security.authentication.BadCredentialsException: Bad credentials  坏的凭证 如果要抛出UsernameNotFoundException 用户找不到异常则需要自定义重新它的异常
            log.info("登录用户：" + phone + " 不存在.");
            return null;
        }else{

            LoginUser loginUser = this.findUserRoleMenu(user);
            return loginUser;
        }
    }

    /**
     * 查询用户角色权限菜单
     * @param user
     * @return
     */
    private LoginUser findUserRoleMenu(SysUser user) {
        LoginUser loginUser = new LoginUser();
        //将查询出的用户属性复制到loginUser
        BeanUtils.copyProperties(user, loginUser);

        Set<SysRole> sysRoles = roleDao.getRoleByUserId(user.getId());
        loginUser.setSysRoles(sysRoles);// 设置角色

        //查询权限集合
        Set<String> roleIds = sysRoles.parallelStream().map(SysRole::getId).collect(Collectors.toSet());
        if (!CollectionUtils.isEmpty(sysRoles)) {

            Set<SysMenu> sysPermissions=sysMenuService.getPermissionByRoleIds(roleIds);
            if (!CollectionUtils.isEmpty(sysPermissions)) {
                Set<String> permissions = sysPermissions.parallelStream().map(SysMenu::getPerms)
                        .collect(Collectors.toSet());
                loginUser.setPermissions(permissions);// 设置权限集合
            }

        }

        //查询菜单集合
        if (!CollectionUtils.isEmpty(sysRoles)){
            List<SysMenu> menuList=sysMenuService.getMenuByRoleIds(roleIds);
            if (!CollectionUtils.isEmpty(menuList)){
                List<SysMenu> father = menuList.stream().filter(sysMenu -> "1".equals(sysMenu.getParentId())).collect(Collectors.toList());
                sysMenuService.setSubChild(father,menuList);
                loginUser.getMenuList().addAll(menuList);
            }
        }
        return loginUser;
    }

    public IPage<SysUser> selectPage(Map<String, Object> data) {
        int currentPage=1;
        if (!Objects.isNull(data.get("page"))) {
            currentPage = Integer.valueOf(data.get("page").toString()).intValue();
        }
        int pageSize=20;
        if (!Objects.isNull(data.get("size"))) {
            pageSize = Integer.valueOf(data.get("size").toString()).intValue();
        }

        SysUser entity = JSON.parseObject(JSON.toJSONString(data), SysUser.class);
        Page<SysUser> page = new Page(currentPage,pageSize);
        QueryWrapper<SysUser> wrapper = new QueryWrapper(entity);
        IPage<SysUser> selectPage = super.selectPage(page, wrapper);
        for (SysUser record : selectPage.getRecords()) {
            List<UserRole> userRoles = this.findUserRole(record.getId());
            record.setUserRoles(userRoles);
            record.setPassword("");
        }
        return selectPage;
    }

    /**
     * 通过用户id 查询用户角色
     * @param userId
     * @return
     */
    private List<UserRole> findUserRole(String userId) {
	    return userDao.findUserRole(userId);
    }

    @Override
    public boolean save(SysUser entity) {
//        Boolean flag=true;
//        if (StringUtils.isNotBlank(entity.getId())) {//表示修改
//            SysUser sysUser = userDao.selectById(entity.getId());
//            String roleIds = sysUser.getRoleIds();
//            if (StringUtils.isNotBlank(roleIds) && roleIds.equals(entity.getRoleIds())){
//                flag=false;
//            }
//        }
        super.save(entity);
        //删除用户角色
        this.delUserRole(entity.getId());

        List<UserRole> userRoles = entity.getUserRoles();
        for (UserRole userRole : userRoles) {
            userRole.setUserId(entity.getId());
        }
        this.saveUserRole(userRoles);
//        if (flag){
//            String roleIds = entity.getRoleIds();
//            String[] split = roleIds.split(",");
//            //删除角色菜单
//            this.delUserRole(entity.getId());
//            //保存用户角色
//            List<UserRole> userRoles = Lists.newArrayList();
//            for (String s : split) {
//                UserRole userRole = new UserRole();
//                userRole.setUserId(entity.getId());
//                userRole.setRoleId(s);
//                userRoles.add(userRole);
//            }
//            this.saveUserRole(userRoles);
//        }
        return true;
    }

    /**
     * 保存用户角色
     * @param userRoles
     */
    public void saveUserRole(List<UserRole> userRoles) {
        userDao.saveUserRole(userRoles);
    }

    /**
     * 删除用户角色
     * @param id
     */
    public void delUserRole(String id) {
        userDao.delUserRole(id);
    }

    /**
     * 通过id删除用户
     * @param id
     */
    public void deleteById(String id) {
        super.removeById(id);
        this.delUserRole(id);
    }


    public void updatePwd(SysUser byUserName) {
        userDao.updatePwd(byUserName);
    }
}
