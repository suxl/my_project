package com.boot.proj.system.generator.config;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.po.TableField;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.IntStream;

/**
 * @author suxiaolin
 * @date 2019/6/28 12:16
 */
public class TableInfo {
    private final Set<String> importPackages = new HashSet();
    private boolean convert;
    private String name;
    private String comment;
    private String entityName;
    private String mapperName;
    private String xmlName;
    private String serviceName;
    private String viewName;
    private String serviceImplName;
    private String controllerName;
    private List<TableField> fields;
    private List<TableField> commonFields;
    private String fieldNames;

    public TableInfo setConvert(boolean convert) {
        this.convert = convert;
        return this;
    }

    protected TableInfo setConvert(StrategyConfig strategyConfig) {
        if (strategyConfig.containsTablePrefix(this.name)) {
            this.convert = true;
        } else if (strategyConfig.isCapitalModeNaming(this.name)) {
            this.convert = false;
        } else if (NamingStrategy.underline_to_camel == strategyConfig.getColumnNaming()) {
            if (StringUtils.containsUpperCase(this.name)) {
                this.convert = true;
            }
        } else if (!this.entityName.equalsIgnoreCase(this.name)) {
            this.convert = true;
        }

        return this;
    }

    public String getEntityPath() {
        return this.entityName.substring(0, 1).toLowerCase() + this.entityName.substring(1);
    }

    public TableInfo setEntityName(StrategyConfig strategyConfig, String entityName) {
        this.entityName = entityName;
        this.setConvert(strategyConfig);
        return this;
    }

    public TableInfo setFields(List<TableField> fields) {
        if (CollectionUtils.isNotEmpty(fields)) {
            this.fields = fields;
            Iterator var2 = fields.iterator();

            while(var2.hasNext()) {
                TableField field = (TableField)var2.next();
                if (null != field.getColumnType() && null != field.getColumnType().getPkg()) {
                    this.importPackages.add(field.getColumnType().getPkg());
                }

                if (!field.isKeyFlag()) {
                    if (field.isConvert()) {
                        this.importPackages.add(com.baomidou.mybatisplus.annotation.TableField.class.getCanonicalName());
                    }
                } else {
                    if (field.isConvert() || field.isKeyIdentityFlag()) {
                        this.importPackages.add(TableId.class.getCanonicalName());
                    }

                    if (field.isKeyIdentityFlag()) {
                        this.importPackages.add(IdType.class.getCanonicalName());
                    }
                }

                if (null != field.getFill()) {
                    this.importPackages.add(com.baomidou.mybatisplus.annotation.TableField.class.getCanonicalName());
                    this.importPackages.add(FieldFill.class.getCanonicalName());
                }
            }
        }

        return this;
    }

    public TableInfo setImportPackages(String pkg) {
        this.importPackages.add(pkg);
        return this;
    }

    public boolean isLogicDelete(String logicDeletePropertyName) {
        return this.fields.parallelStream().anyMatch((tf) -> {
            return tf.getName().equals(logicDeletePropertyName);
        });
    }

    public String getFieldNames() {
        if (StringUtils.isEmpty(this.fieldNames)) {
            StringBuilder names = new StringBuilder();
            IntStream.range(0, this.fields.size()).forEach((i) -> {
                TableField fd = (TableField)this.fields.get(i);
                if (i == this.fields.size() - 1) {
                    names.append(fd.getName());
                } else {
                    names.append(fd.getName()).append(", ");
                }

            });
            this.fieldNames = names.toString();
        }

        return this.fieldNames;
    }

    public TableInfo() {
    }

    public Set<String> getImportPackages() {
        return this.importPackages;
    }

    public boolean isConvert() {
        return this.convert;
    }

    public String getName() {
        return this.name;
    }

    public String getComment() {
        return this.comment;
    }

    public String getEntityName() {
        return this.entityName;
    }

    public String getMapperName() {
        return this.mapperName;
    }

    public String getXmlName() {
        return this.xmlName;
    }

    public String getServiceName() {
        return this.serviceName;
    }

    public String getServiceImplName() {
        return this.serviceImplName;
    }

    public String getControllerName() {
        return this.controllerName;
    }

    public List<TableField> getFields() {
        return this.fields;
    }

    public List<TableField> getCommonFields() {
        return this.commonFields;
    }

    public TableInfo setName(final String name) {
        this.name = name;
        return this;
    }

    public TableInfo setComment(final String comment) {
        this.comment = comment;
        return this;
    }

    public TableInfo setEntityName(final String entityName) {
        this.entityName = entityName;
        return this;
    }

    public TableInfo setMapperName(final String mapperName) {
        this.mapperName = mapperName;
        return this;
    }

    public TableInfo setXmlName(final String xmlName) {
        this.xmlName = xmlName;
        return this;
    }

    public TableInfo setServiceName(final String serviceName) {
        this.serviceName = serviceName;
        return this;
    }

    public TableInfo setServiceImplName(final String serviceImplName) {
        this.serviceImplName = serviceImplName;
        return this;
    }

    public TableInfo setControllerName(final String controllerName) {
        this.controllerName = controllerName;
        return this;
    }

    public TableInfo setCommonFields(final List<TableField> commonFields) {
        this.commonFields = commonFields;
        return this;
    }

    public TableInfo setFieldNames(final String fieldNames) {
        this.fieldNames = fieldNames;
        return this;
    }

    public String getViewName() {
        return viewName;
    }

    public TableInfo setViewName(String viewName) {
        this.viewName = viewName;
        return this;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof TableInfo)) {
            return false;
        } else {
            TableInfo other = (TableInfo)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label159: {
                    Object this$importPackages = this.getImportPackages();
                    Object other$importPackages = other.getImportPackages();
                    if (this$importPackages == null) {
                        if (other$importPackages == null) {
                            break label159;
                        }
                    } else if (this$importPackages.equals(other$importPackages)) {
                        break label159;
                    }

                    return false;
                }

                if (this.isConvert() != other.isConvert()) {
                    return false;
                } else {
                    label151: {
                        Object this$name = this.getName();
                        Object other$name = other.getName();
                        if (this$name == null) {
                            if (other$name == null) {
                                break label151;
                            }
                        } else if (this$name.equals(other$name)) {
                            break label151;
                        }

                        return false;
                    }

                    Object this$comment = this.getComment();
                    Object other$comment = other.getComment();
                    if (this$comment == null) {
                        if (other$comment != null) {
                            return false;
                        }
                    } else if (!this$comment.equals(other$comment)) {
                        return false;
                    }

                    label137: {
                        Object this$entityName = this.getEntityName();
                        Object other$entityName = other.getEntityName();
                        if (this$entityName == null) {
                            if (other$entityName == null) {
                                break label137;
                            }
                        } else if (this$entityName.equals(other$entityName)) {
                            break label137;
                        }

                        return false;
                    }

                    Object this$mapperName = this.getMapperName();
                    Object other$mapperName = other.getMapperName();
                    if (this$mapperName == null) {
                        if (other$mapperName != null) {
                            return false;
                        }
                    } else if (!this$mapperName.equals(other$mapperName)) {
                        return false;
                    }

                    label123: {
                        Object this$xmlName = this.getXmlName();
                        Object other$xmlName = other.getXmlName();
                        if (this$xmlName == null) {
                            if (other$xmlName == null) {
                                break label123;
                            }
                        } else if (this$xmlName.equals(other$xmlName)) {
                            break label123;
                        }

                        return false;
                    }

                    Object this$serviceName = this.getServiceName();
                    Object other$serviceName = other.getServiceName();
                    if (this$serviceName == null) {
                        if (other$serviceName != null) {
                            return false;
                        }
                    } else if (!this$serviceName.equals(other$serviceName)) {
                        return false;
                    }

                    Object this$serviceImplName = this.getServiceImplName();
                    Object other$serviceImplName = other.getServiceImplName();
                    if (this$serviceImplName == null) {
                        if (other$serviceImplName != null) {
                            return false;
                        }
                    } else if (!this$serviceImplName.equals(other$serviceImplName)) {
                        return false;
                    }

                    label102: {
                        Object this$controllerName = this.getControllerName();
                        Object other$controllerName = other.getControllerName();
                        if (this$controllerName == null) {
                            if (other$controllerName == null) {
                                break label102;
                            }
                        } else if (this$controllerName.equals(other$controllerName)) {
                            break label102;
                        }

                        return false;
                    }

                    label95: {
                        Object this$fields = this.getFields();
                        Object other$fields = other.getFields();
                        if (this$fields == null) {
                            if (other$fields == null) {
                                break label95;
                            }
                        } else if (this$fields.equals(other$fields)) {
                            break label95;
                        }

                        return false;
                    }

                    label88: {
                        Object this$commonFields = this.getCommonFields();
                        Object other$commonFields = other.getCommonFields();
                        if (this$commonFields == null) {
                            if (other$commonFields == null) {
                                break label88;
                            }
                        } else if (this$commonFields.equals(other$commonFields)) {
                            break label88;
                        }

                        return false;
                    }

                    Object this$fieldNames = this.getFieldNames();
                    Object other$fieldNames = other.getFieldNames();
                    if (this$fieldNames == null) {
                        if (other$fieldNames != null) {
                            return false;
                        }
                    } else if (!this$fieldNames.equals(other$fieldNames)) {
                        return false;
                    }

                    return true;
                }
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof TableInfo;
    }

    public int hashCode() {
        Boolean PRIME = true;
        int result = 1;
        Object $importPackages = this.getImportPackages();
        result = result * 59 + ($importPackages == null ? 43 : $importPackages.hashCode());
        result = result * 59 + (this.isConvert() ? 79 : 97);
        Object $name = this.getName();
        result = result * 59 + ($name == null ? 43 : $name.hashCode());
        Object $comment = this.getComment();
        result = result * 59 + ($comment == null ? 43 : $comment.hashCode());
        Object $entityName = this.getEntityName();
        result = result * 59 + ($entityName == null ? 43 : $entityName.hashCode());
        Object $mapperName = this.getMapperName();
        result = result * 59 + ($mapperName == null ? 43 : $mapperName.hashCode());
        Object $xmlName = this.getXmlName();
        result = result * 59 + ($xmlName == null ? 43 : $xmlName.hashCode());
        Object $serviceName = this.getServiceName();
        result = result * 59 + ($serviceName == null ? 43 : $serviceName.hashCode());
        Object $serviceImplName = this.getServiceImplName();
        result = result * 59 + ($serviceImplName == null ? 43 : $serviceImplName.hashCode());
        Object $controllerName = this.getControllerName();
        result = result * 59 + ($controllerName == null ? 43 : $controllerName.hashCode());
        Object $fields = this.getFields();
        result = result * 59 + ($fields == null ? 43 : $fields.hashCode());
        Object $commonFields = this.getCommonFields();
        result = result * 59 + ($commonFields == null ? 43 : $commonFields.hashCode());
        Object $fieldNames = this.getFieldNames();
        result = result * 59 + ($fieldNames == null ? 43 : $fieldNames.hashCode());
        return result;
    }

    public String toString() {
        return "TableInfo(importPackages=" + this.getImportPackages() + ", convert=" + this.isConvert() + ", name=" + this.getName() + ", comment=" + this.getComment() + ", entityName=" + this.getEntityName() + ", mapperName=" + this.getMapperName() + ", xmlName=" + this.getXmlName() + ", serviceName=" + this.getServiceName() + ", serviceImplName=" + this.getServiceImplName() + ", controllerName=" + this.getControllerName() + ", fields=" + this.getFields() + ", commonFields=" + this.getCommonFields() + ", fieldNames=" + this.getFieldNames() + ")";
    }
}
