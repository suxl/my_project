package com.boot.proj.system.office.dao;

import org.apache.ibatis.annotations.Mapper;
import com.boot.proj.system.office.entity.SysOffice;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;
/**
 * <p>
 * $!{table.comment} Mapper 接口
 * </p>
 *
 * @author 苏小林
 * @since 2019-06-22
 */
@Mapper
public interface SysOfficeMapper extends BaseMapper<SysOffice> {

    Integer getMaxSort();
}
