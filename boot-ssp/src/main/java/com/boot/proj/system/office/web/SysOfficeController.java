package com.boot.proj.system.office.web;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.boot.proj.commons.utils.DateUtils;
import com.boot.proj.commons.utils.Result;
import com.boot.proj.commons.utils.StringUtils;
import com.boot.proj.commons.utils.excel.ExportExcel;
import com.boot.proj.system.office.entity.SysOffice;
import com.boot.proj.system.office.service.SysOfficeService;
import com.google.common.collect.Lists;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 * 机构管理 前端控制器
 * </p>
 *
 * @author 苏小林
 * @since 2019-06-22
 */
@RestController
@RequestMapping("/office/sysOffice")
public class SysOfficeController {

	@Autowired
    public SysOfficeService service;


	@ApiOperation(value = "机构管理,保存/修改", notes = "机构管理,保存和修改方法")
	@RequestMapping(value = "/save", method = RequestMethod.POST)
    @PreAuthorize("hasAuthority('sys:office:edit')")
	public Result save(@RequestBody SysOffice data){
    	try {

			if ("1".equals(data.getParentId())) {//顶级菜单
				data.setParentIds("1");
			}else{
				SysOffice sysOffice = service.getById(data.getParentId());
				data.setParentIds(sysOffice.getParentIds()+","+sysOffice.getId());
			}
    		service.save(data);
        	return new Result<>(Result.CODE_SUCCESS,"保存成功");
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
        	return new Result<>(Result.CODE_FAILED,"保存失败");
		}

	}

	@ApiOperation(value = "批量删除", notes = "批量删除。")
	@RequestMapping(value = "/batchDelete", method = RequestMethod.POST)
	public Result batchDelete(@RequestBody List<String> list) {
		try {
			service.removeByIds(list);
			return new Result<>(Result.CODE_SUCCESS,"删除成功");
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
			return new Result<>(Result.CODE_FAILED,"删除失败");
		}
	}


	@ApiOperation(value = "机构管理删除", notes = "通过id删除机构管理信息。")
	@RequestMapping(value = "/delete", method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('sys:office:delete')")
	public Result delete(@RequestParam(value="id") String id) {
		try {
			service.removeById(id);
        	return new Result<>(Result.CODE_SUCCESS,"删除成功");
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
            return new Result<>(Result.CODE_FAILED,"删除失败");
		}

	}

	@ApiOperation(value = "分页查询机构管理list", notes = "带参数查询,{\"page\": 1,\"size\": 5,......}。支持模糊查询")
	@RequestMapping(value="/findList",method = RequestMethod.POST)
    @PreAuthorize("hasAuthority('sys:office:view')")
	public Result findList(@RequestBody Map<String,Object> data) {
        try {
        	IPage<SysOffice> selectPage = service.selectPage(data);
        	return new Result<>(Result.CODE_SUCCESS,"",selectPage.getRecords(),selectPage.getTotal());
        } catch (Exception e) {
        	e.printStackTrace();
        	return new Result<>(Result.CODE_FAILED,"查询异常",e.getMessage());
        }
	}

	@ApiOperation(value = "查询机构树结构", notes = "查询机构树结构。")
	@RequestMapping(value="/findOfficeTree",method = RequestMethod.GET)
	public Result findOfficeTree(@RequestParam(value = "name",required = false) String name) {
		if (StringUtils.isNotBlank(name)){

			List<SysOffice> result = Lists.newArrayList();
			// 1.匹配模糊name的结果
			List<SysOffice> targetObj = Lists.newArrayList();
			List<SysOffice> sysOffices = service.selectAll();
			for (SysOffice sysOffice : sysOffices) {
				if (sysOffice.getName().contains(name)) {
					targetObj.add(sysOffice);
				}
			}
			result.addAll(targetObj);

			// 2. 找出targetObj的所有父节点
//			List<SysOffice> parents = Lists.newArrayList();
			for (SysOffice sysOffice : targetObj) {
				String parentIds = sysOffice.getParentIds();//所有父节点id
				String[] split = parentIds.split(",");
				for (String s : split) {
					for (SysOffice office : sysOffices) {
						if (office.getId().equals(s) && !"1".equals(office.getId()) && !result.contains(office)) {
							result.add(office);
						}
					}
				}
			}
//			result.addAll(parents);

			// 3. 找出targetObj的所有子节点
			List<SysOffice> sub = Lists.newArrayList();
			for (SysOffice sysOffice : targetObj) {
				String id = sysOffice.getId();
				for (SysOffice office : sysOffices) {
					if (office.getParentIds().contains(id) && !result.contains(office)) {
						result.add(office);
					}
				}
			}

			List<SysOffice> father = result.stream().filter(sysOffice -> "1".equals(sysOffice.getParentId())).collect(Collectors.toList());
			service.setSubChild(father,result);
			return new Result<>(Result.CODE_SUCCESS,"",result);
		}else{
			List<SysOffice> sysOffices = service.selectAll();
			List<SysOffice> father = sysOffices.stream().filter(sysOffice -> "1".equals(sysOffice.getParentId())).collect(Collectors.toList());
			service.setSubChild(father,sysOffices);
			return new Result<>(Result.CODE_SUCCESS,"",sysOffices);
		}
	}

	@ApiOperation(value = "查询机构管理所有数据list", notes = "查询机构管理所有数据list。")
	@RequestMapping(value="/findAll",method = RequestMethod.POST)
    @PreAuthorize("hasAuthority('sys:office:view')")
	public Result findAll(){
        return new Result<>(Result.CODE_SUCCESS,"",service.selectAll());
	}


	@ApiOperation(value = "通过id获取机构管理实体类", notes = "通过id获取机构管理实体类。")
	@RequestMapping(value="/getById",method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('sys:office:view')")
	public Result getById(@RequestParam(value="id") String id) {
        return new Result<>(Result.CODE_SUCCESS,"",service.getById(id));
	}

	@ApiOperation(value = "通过id获取实体类详细信息", notes = "通过id获取实体类详细信息。")
	@RequestMapping(value="/detail",method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('sys:office:view')")
	public Result detail(@RequestParam(value="id") String id) {
        return new Result<>(Result.CODE_SUCCESS,"",service.getById(id));
	}


	@ApiOperation(value = "导出机构管理excel", notes = "导出机构管理excel。")
	@RequestMapping(value="/export",method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('sys:office:export')")
	public String export(HttpServletRequest request, HttpServletResponse response){
        try {
        String fileName = "机构管理" + DateUtils.formatDateTime(new Date()) + ".xlsx";

        List<SysOffice> list=service.selectAll();
        ExportExcel exportExcel =new ExportExcel("机构管理", SysOffice.class);
        exportExcel.setDatePattern("yyyy-MM-dd HH:mm:ss");
        exportExcel.setDataList(list).write(response, fileName).dispose();

        return null;
        } catch (Exception e) {
        e.printStackTrace();
        }
        return null;
	}


}

