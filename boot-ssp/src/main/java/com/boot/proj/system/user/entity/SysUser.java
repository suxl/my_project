package com.boot.proj.system.user.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.boot.proj.commons.base.BaseEntity;
import com.boot.proj.commons.base.entity.UserRole;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * <p>
 * 用户表
 * </p>
 *
 * @author suxl
 * @since 2018-09-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("sys_user")
public class SysUser extends BaseEntity<SysUser> {


    /**
     * 用户名
     */
    @TableField("username")
    private String username;
    /**
     * 密码
     */
    @TableField("password")
    private String password;
    /**
     * 昵称
     */
    @TableField("nickname")
    private String nickname;
    /**
     * 头像url
     */
    @TableField("headimgurl")
    private String headimgurl;
    /**
     * email
     */
    @TableField("email")
    private String email;
    /**
     * 手机号
     */
    @TableField("phone")
    private String phone;
    /**
     * 性别
     */
    @TableField("sex")
    private Integer sex;
    /**
     * 状态（1有效,0无效）
     */
//    @TableField("enabled")
//    @Column(columnDefinition = "int(64) COMMENT '状态（1有效,0无效）'")
//    private Boolean enabled;
    /**
     * 类型（暂未用）
     */
    @TableField("type")
    private String type;

    /**
     * 用户角色id
     */
    @TableField("role_id")
    private String roleId;

    /**
     * 用户角色name
     */
    @TableField("role_name")
    private String roleName;

    /**
     * 用户角色name
     */
    @TableField("office_id")
    private String officeId;

    /**
     * 用户角色name
     */
    @TableField("office_name")
    private String officeName;

    /**
     * 用户角色
     */
    @TableField(exist = false)
    private List<UserRole> userRoles;



}
