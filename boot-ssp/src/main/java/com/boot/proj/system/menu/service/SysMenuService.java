package com.boot.proj.system.menu.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.boot.proj.commons.base.BaseService;
import com.boot.proj.commons.utils.Reflections;
import com.boot.proj.system.menu.dao.SysMenuMapper;
import com.boot.proj.system.menu.entity.MenuTree;
import com.boot.proj.system.menu.entity.SysMenu;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;

/**
 * <p>
 * 菜单表 服务实现类
 * </p>
 *
 * @author 苏小林
 * @since 2018-12-10
 */
@Service
public class SysMenuService extends BaseService<SysMenuMapper, SysMenu> {

    @Autowired
    private SysMenuMapper sysMenuMapper;

    public List<SysMenu> getMenuByRoleIds(Set<String> roleIds) {
        List<SysMenu> menus = sysMenuMapper.getMenuByRoleIds(roleIds);
        //菜单去重
        HashSet h = new HashSet(menus);
        menus.clear();
        menus.addAll(h);

        menus.sort((a, b) -> a.getSort() - b.getSort());
        return menus;
    }

    /**
     * 获取最大的soft
     *
     * @return
     */
    public Integer getMaxSort() {
        return sysMenuMapper.getMaxSort();
    }

    /**
     * 通过角色查询权限列表
     *
     * @param roleIds
     * @return
     */
    public Set<SysMenu> getPermissionByRoleIds(Set<String> roleIds) {
        return sysMenuMapper.getPermissionByRoleIds(roleIds);
    }

    public List<SysMenu> findRoleMenu(String roleId) {
        List<SysMenu> list = sysMenuMapper.findRoleMenu(roleId);
        return list;
    }

    @Override
    public List<SysMenu> selectAll() {
        QueryWrapper<SysMenu> wrapper = new QueryWrapper<SysMenu>();
        wrapper.setEntity(null);
        wrapper.ne("id", "1");
        return sysMenuMapper.selectList(wrapper);
    }

    //    public List<AsyncRoutes> setAsyncRoutes(List<SysMenu> menuList) {
//        List<AsyncRoutes> asyncRoutesList = Lists.newArrayList();
////        List<SysMenu> father = menuList.stream().filter(sysMenu -> "1".equals(sysMenu.getParentId())).collect(Collectors.toList());
//        List<AsyncRoutes> asyncRoutes = Lists.newArrayList();
//        setAsyncRoutes(asyncRoutesList,menuList);
//
//        return asyncRoutesList;
//    }

//    public void setAsyncRoutes(List<AsyncRoutes> asyncRoutesList, List<SysMenu> father) {
//
//        for (SysMenu sysMenu : father) {
//            AsyncRoutes asyncRoutes = new AsyncRoutes();
//            asyncRoutes.setName(sysMenu.getName());
//            asyncRoutes.setPath(sysMenu.getPath());
//            asyncRoutes.setComponent(sysMenu.getComponent());
//            Map<String, String> map = Maps.newHashMap();
//            map.put("icon",sysMenu.getIcon());
//            map.put("title",sysMenu.getName());
//            asyncRoutes.setMeta(map);
//            if (sysMenu.getChildren().size()>0 ){
//                if (!asyncRoutesList.contains(sysMenu)){
//                    List<SysMenu> childList = sysMenu.getChildren();
//                    List<AsyncRoutes> child = getChild(childList);
//                    asyncRoutes.setChildren(child);
//                    asyncRoutesList.add(asyncRoutes);
//                    setAsyncRoutes(asyncRoutes.getChildren(),childList);
//                }else{
//                    List<SysMenu> childList = sysMenu.getChildren();
//                    List<AsyncRoutes> child = getChild(childList);
//                    asyncRoutesList.stream().forEach(asyncRoutes1 -> {
//                        if (asyncRoutes1.getPath().equals(sysMenu.getPath())){
//                            asyncRoutes1.getChildren().addAll(child);
//                            setAsyncRoutes(asyncRoutes1.getChildren(),childList);
//                        }
//                    });
//                }
//            }else{
//                if (!asyncRoutesList.contains(asyncRoutes)){
//                    asyncRoutesList.add(asyncRoutes);
//                }
//            }
//        }
//    }

//    private List<AsyncRoutes> getChild(List<SysMenu> childList) {
//        List<AsyncRoutes> asyncRoutesList = Lists.newArrayList();
//        for (SysMenu sysMenu : childList) {
//            AsyncRoutes asyncRoutes = new AsyncRoutes();
//            asyncRoutes.setPath(sysMenu.getPath());
//            asyncRoutes.setName(sysMenu.getName());
//            asyncRoutes.setComponent(sysMenu.getComponent());
//            Map<String, String> map = Maps.newHashMap();
//            map.put("icon",sysMenu.getIcon());
//            map.put("title",sysMenu.getName());
//            asyncRoutes.setMeta(map);
//            asyncRoutesList.add(asyncRoutes);
//        }
//        return asyncRoutesList;
//
//    }


//    private List<AsyncRoutes> getSubChildRoutes(AsyncRoutes sysMenu, List<AsyncRoutes> asyncRoutes) {
//        List<AsyncRoutes> result = Lists.newArrayList();
//        for (AsyncRoutes menu : asyncRoutes) {
//            if (sysMenu.getId().equals(menu.getParentId())){
//                result.add(menu);
//            }
//        }
//        return result;
//    }

    //    public void setSubChild(List<SysMenu> father, List<SysMenu> sysMenus) {
//        for (SysMenu sysMenu : father) {
//            List<SysMenu> subChild = getSubChild(sysMenu,sysMenus);
//            if (!CollectionUtils.isEmpty(subChild)){
//                sysMenu.setChildren(subChild);
//                sysMenus.removeAll(subChild);
//                setSubChild(subChild,sysMenus);
//            }
//        }
//    }
    public void setSubChild(Object fa, Object sysMe) {
        if (fa instanceof List && sysMe instanceof List) {
            List<Object> father = (List<Object>) fa;
            List<Object> sysMenus = (List<Object>) sysMe;
            for (Object object : father) {
                List<Object> subChild = getSubChild(object, sysMenus);
                if (!CollectionUtils.isEmpty(subChild)) {
                    Reflections.setFieldValue(object,"children",subChild);
                    sysMenus.removeAll(subChild);
                    setSubChild(subChild, sysMenus);
                }
            }

        }
//        for (SysMenu sysMenu : father) {
//            List<SysMenu> subChild = getSubChild(sysMenu,sysMenus);
//            if (!CollectionUtils.isEmpty(subChild)){
//                sysMenu.setChildren(subChild);
//                sysMenus.removeAll(subChild);
//                setSubChild(subChild,sysMenus);
//            }
//        }
    }

    public List<Object> getSubChild(Object sysMenu, Object sysM) {
        List<Object> result = Lists.newArrayList();
        if (sysM instanceof List) {
            List<Object> sysMenus = (List<Object>) sysM;
            for (Object menu : sysMenus) {
                String id = String.valueOf(Reflections.getFieldValue(sysMenu, "id"));
                String parentId = String.valueOf(Reflections.getFieldValue(menu, "parentId"));
                if (id.equals(parentId)) {
                    result.add(menu);
                }
            }

            Collections.sort(result, new Comparator<Object>() {

                @Override
                public int compare(Object o1, Object o2) {
                    String sort = String.valueOf(Reflections.getFieldValue(o1, "sort"));
                    Integer sort1 = Integer.parseInt(String.valueOf(Reflections.getFieldValue(o1, "sort")));
                    Integer sort2 = Integer.parseInt(String.valueOf(Reflections.getFieldValue(o2, "sort")));
                    int i = sort1.compareTo(sort2);
                    return i;
                }
            });
        }
        return result;
    }

//    public List<SysMenu> getSubChild(SysMenu sysMenu, List<SysMenu> sysMenus) {
//        List<SysMenu> result = Lists.newArrayList();
//        for (SysMenu menu : sysMenus) {
//            if (sysMenu.getId().equals(menu.getParentId())) {
//                result.add(menu);
//            }
//        }
//        return result;
//    }


    public List<MenuTree> findMenuTree() {
        return sysMenuMapper.findMenuTree();
    }
}
