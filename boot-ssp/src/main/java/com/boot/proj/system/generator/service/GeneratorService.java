package com.boot.proj.system.generator.service;

import com.boot.proj.commons.base.BaseService;
import com.boot.proj.system.generator.dao.GeneratorMapper;
import com.boot.proj.system.generator.entity.Generator;
import com.boot.proj.system.generator.entity.TableInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(rollbackFor = Exception.class)
public class GeneratorService extends BaseService<GeneratorMapper, Generator> {
    @Autowired
    private GeneratorMapper generatorMapper;

    public List<TableInfo> getAllTables(String dbName,String tableName) {
        return generatorMapper.getAllTables(dbName,tableName);
    }

    public List<Generator> getAttrByTable(String dbName, String tableName) {
        return generatorMapper.getAttrByTable(dbName,tableName);
    }
}
