package com.boot.proj.system.menu.entity;

import com.google.common.collect.Lists;
import lombok.Data;

import java.util.List;

/**
 * @author suxiaolin
 * @date 2019/5/22 8:53
 */
@Data
public class MenuTree {
    private String id;
    private String label;
    private int sort;
    private String icon;
    private String parentId;
    private String parentIds;
    private List<MenuTree> children= Lists.newArrayList();
}
