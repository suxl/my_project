package com.boot.proj.system.generator.config;

import com.baomidou.mybatisplus.extension.toolkit.PackageHelper;
import com.baomidou.mybatisplus.generator.config.rules.FileType;

import java.io.File;

public interface IFileCreate {
    boolean isCreate(MyConfigBuilder configBuilder, FileType fileType, String filePath);

    default void checkDir(String filePath) {
        File file = new File(filePath);
        boolean exist = file.exists();
        if (!exist) {
            PackageHelper.mkDir(file.getParentFile());
        }

    }
}
