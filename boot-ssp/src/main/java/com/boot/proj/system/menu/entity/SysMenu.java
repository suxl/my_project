package com.boot.proj.system.menu.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.boot.proj.commons.base.BaseEntity;
import com.google.common.collect.Lists;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import java.util.List;

/**
 * <p>
 * 菜单表
 * </p>
 *
 * @author 苏小林
 * @since 2018-12-10
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_menu")
public class SysMenu extends BaseEntity<SysMenu> {

    /**
     * 菜单名
     */
    @TableField("name")
    @Column(columnDefinition = "varchar(64) COMMENT '菜单名'")
    private String name;

    /**
     * 父菜单id
     */
    @TableField("parent_id")
    @Column(columnDefinition = "varchar(64) COMMENT '父菜单id'")
    private String parentId;

    /**
     * 所有父菜单
     */
    @TableField("parent_ids")
    @Column(columnDefinition = "varchar(2000) COMMENT '所有父菜单'")
    private String parentIds;

    /**
     * 父菜单name
     */
    @TableField("parent_name")
    @Column(columnDefinition = "varchar(64) COMMENT '父菜单name'")
    private String parentName;

    /**
     * url
     */
    @TableField("url")
    @Column(columnDefinition = "varchar(64) COMMENT 'url'")
    private String url;

    /**
     * perms 授权(多个用逗号分隔，如：sys:user:add,sys:user:edit)
     */
    @TableField("perms")
    @Column(columnDefinition = "varchar(64) COMMENT '授权'")
    private String perms;

    /**
     *  类型   0：目录   1：菜单   2：按钮
     */
    @TableField("type")
    @Column(columnDefinition = "varchar(64) COMMENT '类型'")
    private Integer type;

    /**
     * 图标
     */
    @TableField("icon")
    @Column(columnDefinition = "varchar(64) COMMENT '图标'")
    private String icon;
    /**
     * 排序
     */
    @TableField("sort")
    @Column(columnDefinition = "int(6) COMMENT '排序'")
    private Integer sort;
    /**
     * 是否显示 （0 显示 1 不显示）
     */
    @TableField("if_show")
    @Column(columnDefinition = "varchar(64) COMMENT '是否显示 0 是 1 否'")
    private Integer ifShow;

    /**
     * 子菜单
     */
    @TableField(exist = false)
    private List<SysMenu> children= Lists.newArrayList();



}
