package com.boot.proj.system.user.web;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.boot.proj.commons.base.entity.LoginUser;
import com.boot.proj.commons.utils.RSACoder;
import com.boot.proj.commons.utils.Result;
import com.boot.proj.commons.utils.StringUtils;
import com.boot.proj.commons.utils.UserUtils;
import com.boot.proj.system.user.entity.SysUser;
import com.boot.proj.system.user.service.SysUserService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * <p>
 * 用户表 前端控制器
 * </p>
 *  缓存 参照：https://blog.csdn.net/qq_37946744/article/details/79621058#
 *            https://blog.csdn.net/qq_40591332/article/details/82352039
 * @author 苏小林
 * @since 2018-09-28
 */
@RestController
//@RequestMapping("/system/sysUser")
//@CacheConfig(cacheNames = "sysUser")
public class SysUserController {

	@Autowired
	private JedisConnectionFactory jedisConnectionFactory;

	@Autowired
	private RedisTemplate redisTemplate;

	@Value("${rsa.privateKey}")
	private String privateKey;

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	@Autowired
    public SysUserService sysUserService;

//	@Autowired
//	private KafkaSender kafkaSender;
    
    @GetMapping(value = "/users-anon/internal", params = "username")
    public LoginUser findByUsername(String username) {
        return sysUserService.getByUserName(username);
    }

//	//http://127.0.0.1:9999/system/sysUser/testKafka
//	@RequestMapping(value = "/system/sysUser/testKafka", method = RequestMethod.POST)
//	public void testKafka(@RequestBody String json){
////		kafkaSender.send("message","hello 你好呀。我是kafka测试");
////		kafkaSender.send("message",json);
//
//		//批量查询
//		List<String> objects = Lists.newArrayList();
//		objects.add("1");
//		objects.add("2");
//		List<SysUser> sysUsers = sysUserService.selectBatchIds(objects);
//		for (SysUser sysUser : sysUsers) {
//			sysUser.setPhone("15311766807");
//		}
//
//
//		//批量修改
//		sysUserService.insertOrUpdateAllColumnBatch(sysUsers);
//		System.err.println("输出结果:"+sysUsers.size());
//
//
//	}

	/**
	 * 清除所有缓存
	 */
	@ApiOperation(value = "清除所有缓存", notes = "清除所有缓存")
	@ApiImplicitParam(name = "data", value = "{\"page\": 1,\"size\": 5,\"name\":\"\"...}", required = true)
	@RequestMapping(value = "/system/sysUser/cleanCache", method = RequestMethod.POST)
	public Result cleanCache(){
		RedisConnection connection = jedisConnectionFactory.getConnection();
		try {
			Set keys = redisTemplate.keys("user:" + "*");
			redisTemplate.delete(keys);
//			connection.flushDb();
			return new Result<>(Result.CODE_SUCCESS,"缓存清除成功");
		} catch (Exception e) {
			e.printStackTrace();
			return new Result<>(Result.CODE_SUCCESS,"缓存清除失败");
		}finally {
//			connection.close();
		}

	}
	
    @ApiOperation(value = "保存/修改", notes = "保存和修改方法")
	@ApiImplicitParam(name = "data", value = "SysUser属性:{\"id\": \"\"...}", required = true)
	@RequestMapping(value = "/system/sysUser/save", method = RequestMethod.POST)
	public Result save(@RequestBody SysUser data){
    	try {
    		//密码加密
			if (StringUtils.isNotBlank(data.getPassword())) {
				data.setPassword(bCryptPasswordEncoder.encode(RSACoder.decrypt(data.getPassword(), privateKey)));
			}
    		sysUserService.save(data);
			return new Result<>(Result.CODE_SUCCESS,"保存成功");
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
			return new Result<>(Result.CODE_FAILED,"保存失败");
		}
	}

	@ApiOperation(value = "修改用户信息", notes = "修改用户信息")
	@ApiImplicitParam(name = "data", value = "SysUser属性:{\"id\": \"\"...}", required = true)
	@RequestMapping(value = "/system/sysUser/updateUserInfo", method = RequestMethod.POST)
	public Result updateUserInfo(@RequestBody SysUser data){
		try {
			//密码加密
			if (StringUtils.isNotBlank(data.getPassword())) {
				data.setPassword(bCryptPasswordEncoder.encode(data.getPassword()));
			}
			sysUserService.updateById(data);
			return new Result<>(Result.CODE_SUCCESS,"保存成功");
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
			return new Result<>(Result.CODE_FAILED,"保存失败");
		}
	}

	@ApiOperation(value = "修改用户密码", notes = "修改用户密码")
	@ApiImplicitParam(name = "data", value = "SysUser属性:{\"oldPass\": \"\",\"pass\": \"\",\"userName\": \"\"}", required = true)
	@RequestMapping(value = "/system/sysUser/updatePassword", method = RequestMethod.POST)
	public Result updateUserInfo(@RequestBody Map<String,String> data){
		try {
			LoginUser loginAppUser = UserUtils.getLoginAppUser();
			String oldPass = data.get("oldPass");//原密码
			String pass = data.get("pass");//新密码
			String userName = data.get("userName");
			SysUser byUserName = sysUserService.getByUserName(userName);
			String decrypt = RSACoder.decrypt(oldPass, privateKey);
			String newPwd = RSACoder.decrypt(pass, privateKey);

			byUserName.setUpdateDate(new Date());
			byUserName.setUpdateBy(loginAppUser.getId());
			byUserName.setUsername(loginAppUser.getUsername());

			if (!bCryptPasswordEncoder.matches(decrypt,byUserName.getPassword())) {
				return new Result<>(Result.CODE_FAILED,"原密码输入不正确");
			}

			//新密码加密
			byUserName.setPassword(bCryptPasswordEncoder.encode(newPwd));

			sysUserService.updatePwd(byUserName);
			return new Result<>(Result.CODE_SUCCESS,"保存成功");
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
			return new Result<>(Result.CODE_FAILED,"保存失败");
		}
	}

	/**
	 * 校验用户名是否重复
	 * @param username
	 * @return
	 */
	@ApiOperation(value = "校验用户名是否重复", notes = "校验用户名是否重复")
	@GetMapping(value = "/system/sysUser/validUserName", params = "username")
	public Result validUserName(String username) {
		try {
			LoginUser byUserName = sysUserService.getByUserName(username);
			if (byUserName !=null){
				return new Result<>(Result.CODE_SUCCESS,"",true);
			}else{
				return new Result<>(Result.CODE_SUCCESS,"",false);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new Result<>(Result.CODE_SUCCESS,"",false);
		}
	}


	@ApiOperation(value = "获取当前用户信息", notes = "获取当前用户信息")
	@RequestMapping(value = "/system/sysUser/getUserInfo", method = RequestMethod.GET)
	public Result getUserInfo(){
		try {
			LoginUser loginAppUser = UserUtils.getLoginAppUser();
			SysUser sysUser = sysUserService.getById(loginAppUser.getId());
			if (sysUser !=null) {
				sysUser.setPassword(null);
			}
			return new Result(Result.CODE_SUCCESS,"查询成功",sysUser);
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(Result.CODE_FAILED,"查询失败",e.getMessage());
		}

	}
	
	@ApiOperation(value = "删除", notes = "通过id删除。")
	@RequestMapping(value = "/system/sysUser/delete", method = RequestMethod.GET)
	public Result delete(@RequestParam(value="id") String id) {

		try {
			sysUserService.deleteById(id);
			return new Result<>(Result.CODE_SUCCESS,"删除成功");
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
			return new Result<>(Result.CODE_FAILED,"删除失败");
		}
		
	}

	@ApiOperation(value = "批量删除", notes = "批量删除。")
	@RequestMapping(value = "/system/sysUser/batchDelete", method = RequestMethod.POST)
	public Result batchDelete(@RequestBody List<String> list) {
		try {
			sysUserService.removeByIds(list);
			for (String s : list) {
				sysUserService.delUserRole(s);
			}

			return new Result<>(Result.CODE_SUCCESS,"删除成功");
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
			return new Result<>(Result.CODE_FAILED,"删除失败");
		}
	}




	/**
	 * 当前登录用户 LoginAppUser
	 */
	@GetMapping("/users/current")
	public LoginUser getLoginAppUser() {
		return UserUtils.getLoginAppUser();
	}

	/**
	 * 当前登录用户 LoginAppUser
	 */

	@GetMapping("/system/sysUser/testRemoveRedis")
//	@CacheEvict(cacheNames = "sysUser",allEntries=true)  //清空list下面的所有缓存
//	@CacheEvict(cacheNames = "list",key="2")  //清空list下面key为2的缓存
	public LoginUser testRemoveRedis() {

		return UserUtils.getLoginAppUser();
	}


	@ApiOperation(value = "查询list", notes = "带参数查询,支持模糊查询。")
	@RequestMapping(value="/users-anon/system/sysUser/findList",method = RequestMethod.POST)
//	@PreAuthorize("hasAuthority('back:user:query')")
//	@Cacheable(key = "#data") //缓存名为list  缓存key为data参数
	public Result findList(@RequestBody Map<String,Object> data) {
		System.err.println("进入查询");

		IPage<SysUser> selectPage = sysUserService.selectPage(data);

		return new Result<>(Result.CODE_SUCCESS,"",selectPage.getRecords(),selectPage.getTotal());
	}

	@ApiOperation(value = "通过id获取实体类", notes = "通过id获取实体类。")
	@RequestMapping(value="/system/sysUser/getById",method = RequestMethod.GET)
	public Result getById(@RequestParam(value="id") String id) {
		SysUser sysUser = sysUserService.getById(id);
		if (sysUser!=null) {
			sysUser.setPassword("");
		}
		return new Result<>(Result.CODE_SUCCESS,"",sysUser);
	}

	@ApiOperation(value = "通过id获取实体类详细信息", notes = "通过id获取实体类详细信息。")
	@RequestMapping(value="/system/sysUser/detail",method = RequestMethod.GET)
	public Result detail(@RequestParam(value="id") String id) {
		SysUser sysUser = sysUserService.getById(id);
		if (sysUser!=null) {
			sysUser.setPassword("");
		}
		return new Result<>(Result.CODE_SUCCESS,"",sysUser);
	}
	
	
    

}

