package com.boot.proj.websocket.config;

import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.server.HandshakeInterceptor;
import org.springframework.web.socket.server.support.DefaultHandshakeHandler;

import java.security.Principal;
import java.util.Map;

/**
 * @author suxiaolin
 * @date 2019/5/21 10:57
 */
@Configuration
@EnableWebSocketMessageBroker
public class WebSocketAutoConfig implements WebSocketMessageBrokerConfigurer {

    @Value("${security.oauth2.resource.user-info-uri}")
    private String checkUrl;

    @Autowired
    private RestTemplate restTemplate;
    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        registry.addEndpoint("/websocket")         //开启/bullet端点
                .addInterceptors(new HandshakeInterceptor() {
                    /**
                     * websocket握手
                     */
                    @Override
                    public boolean beforeHandshake(ServerHttpRequest request, ServerHttpResponse response, WebSocketHandler wsHandler, Map<String, Object> attributes) throws Exception {
                        ServletServerHttpRequest req = (ServletServerHttpRequest) request;
                        //获取token认证
                        String token = req.getServletRequest().getParameter("access_token");
                        //解析token获取用户信息
                        Principal user = parseToken(token);
                        if (user == null) {   //如果token认证失败user为null，返回false拒绝握手
                            return false;
                        }
                        //保存认证用户
                        attributes.put("user", user);
                        return true;
                    }

                    @Override
                    public void afterHandshake(ServerHttpRequest request, ServerHttpResponse response, WebSocketHandler wsHandler, Exception exception) {

                    }
                }).setHandshakeHandler(new DefaultHandshakeHandler() {
            @Override
            protected Principal determineUser(ServerHttpRequest request, WebSocketHandler wsHandler, Map<String, Object> attributes) {
                //设置认证用户
                return (Principal) attributes.get("user");
            }
        })
                .setAllowedOrigins("*")         //允许跨域访问
                .withSockJS()                  //使用sockJS
                .setClientLibraryUrl( "https://cdn.jsdelivr.net/npm/sockjs-client@1.3.0/dist/sockjs.min.js" ); //Added

    }

    /**
     * 根据token认证授权
     *
     * @param token
     */
    private Principal parseToken(String token) {
        //TODO 解析token并获取认证用户信息
//        http://localhost:8080/user-me

        Principal principal = null;
        try {
            Map<String,Object> forObject = restTemplate.getForObject(checkUrl+"?access_token=" + token, Map.class);
            Object princ = forObject.get("principal");
            JSONObject o = (JSONObject)JSONObject.toJSON(princ);
            principal = JSONObject.toJavaObject(o, Principal.class);
        } catch (RestClientException e) {
            e.printStackTrace();
        }
        return principal;
    }

    @Override
    public void configureMessageBroker(MessageBrokerRegistry registry) {
        registry.enableSimpleBroker("/allMessage");  //订阅Broker名称
        //客户端发送消息的目的地为/app/sendTest，则对应控制层@MessageMapping(“/sendTest”)
        //客户端订阅主题的目的地为/app/subscribeTest，则对应控制层@SubscribeMapping(“/subscribeTest”)
        registry.setApplicationDestinationPrefixes("/app");
    }
}
