package com.boot.proj.file.controller;


import com.boot.proj.commons.utils.IdGen;
import com.boot.proj.commons.utils.Result;
import com.boot.proj.file.ffmpeg.video.VideoOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 *
 *
 * @author suxiaolin
 */
@RestController
@RequestMapping("/users-anon/fdfs")
public class FastDFSController {
    private static Logger logger = LoggerFactory.getLogger(FastDFSController.class);

//    @Autowired
//    private FastDFSClient fastDFSClient;
    @Autowired
    private VideoOperation videoOperation;


//    @Value("${fastdfs.http_secret_key}")
//    private String httpSecretKey;
//    @Value("${fastdfs.tracker_servers}")
//    private String trackerServers;

    /**
     * @return restResponse
     */
    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public Result uploadMultipartFile(@RequestParam MultipartFile file) throws IOException {
        File excelFile = null;
        Result result = null;
        try {
            if (!file.isEmpty()) {
                // 获取文件名
                String fileName = file.getOriginalFilename();
                // 获取文件后缀
                String prefix=fileName.substring(fileName.lastIndexOf("."));
                // 用uuid作为文件名，防止生成的临时文件重复
                excelFile = File.createTempFile(IdGen.uuid(), prefix);
                // MultipartFile to File
                file.transferTo(excelFile);

                // 视频截图
                //Result videoCoverImg = videoOperation.getVideoCoverImg(excelFile, "D:\\workspace\\ffm\\new.jpg");

//                Result result = videoOperation.videoConvert(excelFile, "E:\\BaiduNetdiskDownload\\");
                //获取视频信息
                //result = videoOperation.getVideoInfo(excelFile);

                FileInputStream fileInputStream = new FileInputStream(excelFile);
                String env = System.getProperty("os.name");
                String os = env.toLowerCase();
                String temp;//临时文件目录
                String uuid = IdGen.uuid();
                if (os.contains("win")) {// windows环境
//                    temp = "C:\\temp\\"+uuid+"\\";
                    temp = "C:\\temp\\";
                }else{
                    temp = "/temp";
                }

//                FileUtils.deleteDir(new File(temp));

                //视频分片
                String s = videoOperation.videoToTs(excelFile,temp);
////                System.err.println(s);

//
//                String s = fastDFSClient.uploadFile(IOUtils.toByteArray(fileInputStream), FilenameUtils.getExtension(excelFile.getName()));
//                result = new Result(Result.CODE_SUCCESS,"成功",s);
            }
        } catch (Exception e) {
            result = new Result(Result.CODE_FAILED,"上传失败",e.getMessage());
        }finally {
            if (file !=null) {
                excelFile.delete();
            }

        }
        return result;


//        try {
//            return new Result(Result.CODE_SUCCESS,"上传成功",fastDFSClient.uploadFile(IOUtils.toByteArray(file.getInputStream()), FilenameUtils.getExtension(file.getOriginalFilename())));
//        } catch (Exception e) {
//            return new Result(Result.CODE_FAILED,"上传失败",e.getMessage());
//        }
    }


//    /**
//     * 获取访问服务器的token，拼接到地址后面
//     *
//     * @param filepath 文件路径 group1/M00/00/00/wKgzgFnkTPyAIAUGAAEoRmXZPp876.jpeg
//     * @return 返回token，如： token=078d370098b03e9020b82c829c205e1f&ts=1508141521
//     */
//    @RequestMapping(value = "/getToken", method = RequestMethod.GET)
//    public String getToken(String filepath){
//        // unix seconds
//        int ts = (int) Instant.now().getEpochSecond();
//        // token
//        String token = "null";
//        try {
//            token = ProtoCommon.getToken(getFilename(filepath), ts, httpSecretKey);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        String[] split = trackerServers.split(":");
//        String url="http://"+split[0]+"/"+filepath+"?token="+token+"&ts="+ts;
//
//        return url;
//    }
//
//    /**
//     * 获取FastDFS文件的名称，如：M00/00/00/wKgzgFnkTPyAIAUGAAEoRmXZPp876.jpeg
//     *
//     * @param fileId 包含组名和文件名，如：group1/M00/00/00/wKgzgFnkTPyAIAUGAAEoRmXZPp876.jpeg
//     * @return FastDFS 返回的文件名：M00/00/00/wKgzgFnkTPyAIAUGAAEoRmXZPp876.jpeg
//     */
//    public static String getFilename(String fileId){
//        String[] results = new String[2];
//        StorageClient1.split_file_id(fileId, results);
//
//        return results[1];
//    }
//
//    /**
//     * @param fileUrl 删除的文件全路径
//     * @return restResponse
//     */
//    @RequestMapping(value = "/deleteFile", method = RequestMethod.POST)
//    public Result deleteFile(@RequestParam(value = "fileUrl") String fileUrl) {
//        logger.info("CLASS_METHOD：" + Thread.currentThread().getStackTrace()[1].getMethodName() + "----> REQUEST_MESSAGE: " + fileUrl);
//        FastDFSResponse restResponse = new FastDFSResponse();
//        try {
//            fastDFSClient.deleteFile(fileUrl);
//            return new Result(Result.CODE_SUCCESS,"删除成功");
//        } catch (Exception e) {
//            e.printStackTrace();
//            return new Result(Result.CODE_FAILED,"删除失败",e.getMessage());
//        }
//    }
//
//    /**
//     * @param fileUrl 下载的文件全路径
//     * @return restResponse
//     */
//    @RequestMapping(value = "/download", method = RequestMethod.GET)
//    public void download(@RequestParam(value = "fileUrl") String fileUrl, @RequestParam(value = "fileName") String fileName,HttpServletResponse response) {
//        FastDFSResponse restResponse = new FastDFSResponse();
//        try {
//
//            byte[] bytes = fastDFSClient.downloadFile(fileUrl);
//            //设置相应类型application/octet-stream        （注：applicatoin/octet-stream 为通用，一些其它的类型苹果浏览器下载内容可能为空）
//            response.reset();
//            response.setContentType("applicatoin/octet-stream");
//            //设置头信息                 Content-Disposition为属性名  附件形式打开下载文件   指定名称  为 设定的fileName
//            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(fileName, "UTF-8"));
//            // 写入到流
//            ServletOutputStream out = response.getOutputStream();
//            out.write(bytes);
//            out.close();
//        } catch (Exception e) {
//            logger.error("CLASS_METHOD：" + Thread.currentThread().getStackTrace()[1].getMethodName() + "--->" + "ERROR_MESSAGE:", e);
//        }
//        logger.info("CLASS_METHOD：" + Thread.currentThread().getStackTrace()[1].getMethodName() + "----> RESPONSE_MESSAGE: " + JSON.toJSONString(restResponse));
//    }
//
//    @GetMapping(value = "/testThread")
//    public void testThread(){
//
//        List<Integer> list = Lists.newArrayList();
//        for (int i = 1; i <=19 ; i++) {
//            list.add(i);
//        }
//        ExecutorService executorService = Executors.newFixedThreadPool(10);
//        List<Future<String>> resultList = new ArrayList<Future<String>>();
//        int threadNum = 5;
//        int length = list.size();
//
//        int tl = length % threadNum == 0 ? length / threadNum : (length
//                / threadNum + 1);
//        System.err.println(tl);
//        for (int i = 0; i <threadNum ; i++) {
//            int end = (i + 1) * tl;
//            Future<String> submit = executorService.submit(new TaskWithResult(i,list, i * tl, end > length ? length : end));
//            resultList.add(submit);
//        }
//
//        //遍历任务的结果
//        for (Future<String> fs : resultList) {
//            try {
//                while (!fs.isDone()) ;//Future返回如果没有完成，则一直循环等待，直到Future返回完成
//                System.out.println(fs.get());     //打印各个线程（任务）执行的结果
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            } catch (ExecutionException e) {
//                e.printStackTrace();
//            } finally {
//                //启动一次顺序关闭，执行以前提交的任务，但不接受新任务
//                executorService.shutdown();
//            }
//
//
//        }
//    }
//
//    class TaskWithResult implements Callable<String> {
//        private int id;
//        private List<Integer> list;
//        private int start;
//        private int end;
//
//        public TaskWithResult(int id,List<Integer> list,int start,int end){
//            this.id = id;
//            this.list=list;
//            this.start=start;
//            this.end=end;
//        }
//
//        @Override
//        public String call() throws Exception {
//            List<Integer> integers = list.subList(start, end);
//            for (Integer integer : integers) {
//                System.out.println(Thread.currentThread().getName()+"截取的数据   "+integer);
//            }
//
//            //该返回结果将被Future的get方法得到
//            return "call()方法被自动调用，任务返回的结果是：" + id + "    " + Thread.currentThread().getName();
//        }
//    }
}
