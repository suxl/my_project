package com.boot.proj.file.ffmpeg.common;

import lombok.Data;

/**
 * @author suxiaolin
 * @date 2019/11/6 9:44
 */
@Data
public class MusicMetaInfo {

    /**
     * 音频格式
     */
    private String format;

    /**
     * 时长
     */
    private Long duration;

    /**
     * 音频码率
     */
    private Integer bitRate;

    /**
     * 音频采样率
     */
    private Long sampleRate;
}
