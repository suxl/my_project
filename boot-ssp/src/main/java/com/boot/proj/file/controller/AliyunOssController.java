package com.boot.proj.file.controller;

import com.boot.proj.commons.utils.Result;
import com.boot.proj.file.utils.AliyunOSSUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;

/**
 * @author suxiaolin
 * @date 2019/7/1 16:39
 * 阿里云 Oss 上传类
 */
@RestController
@RequestMapping("/oss")
@Slf4j
public class AliyunOssController {
    @Autowired
    private AliyunOSSUtil aliyunOSSUtil;

    /**
     * @return restResponse
     */
    @RequestMapping(value = "/ossUpload", method = RequestMethod.POST)
    public Result ossUpload(@RequestParam MultipartFile file) {

        log.info("文件上传");
        String filename = file.getOriginalFilename();
        System.out.println(filename);
        try {
            String uploadUrl = "";
            if (file != null) {
                if (!"".equals(filename.trim())) {
                    File newFile = new File(filename);
                    FileOutputStream os = new FileOutputStream(newFile);
                    os.write(file.getBytes());
                    os.close();
                    file.transferTo(newFile);
                    // 上传到OSS
                    uploadUrl = aliyunOSSUtil.upLoad(newFile);

                }

            }
            return new Result(Result.CODE_SUCCESS,"上传成功",uploadUrl);

        } catch (Exception ex) {
            ex.printStackTrace();
            return new Result(Result.CODE_FAILED,"上传失败",ex.getMessage());
        }
    }

    /**
     * 删除文件
     * @param fileName
     * @return
     */
    @RequestMapping(value = "/deleteFile", method = RequestMethod.GET)
    public Result deleteFile(@RequestParam("fileName") String fileName) {
        Boolean aBoolean = aliyunOSSUtil.deleteFile(fileName);
        if (aBoolean){
            return new Result(Result.CODE_SUCCESS,"删除成功");
        }else{
            return new Result(Result.CODE_FAILED,"删除失败");
        }
    }
}
