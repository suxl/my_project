package com.boot.proj.file.config.oss;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author suxiaolin
 * @date 2019/7/1 12:48
 */
@Component
@ConfigurationProperties(prefix = "aliyun.oss")
@Data
public class AliyunOSSConfig {
    private String bucketName;
    private String endpoint;
    private String accessKeyId;
    private String accessKeySecret;
}
