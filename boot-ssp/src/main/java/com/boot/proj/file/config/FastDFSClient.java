package com.boot.proj.file.config;

import org.csource.fastdfs.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.bind.Bindable;
import org.springframework.boot.context.properties.bind.Binder;
import org.springframework.context.EnvironmentAware;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Map;
import java.util.Properties;


@Component
public class FastDFSClient implements EnvironmentAware {

    private static final Logger logger = LoggerFactory.getLogger(FastDFSClient.class);

    @Override
    public void setEnvironment(Environment environment) {
        Binder binder = Binder.get(environment); //绑定配置器
        Map<String, Object> dsMap = binder.bind("fastdfs", Bindable.of(Map.class)).get();
        Properties props = new Properties();
        for (Map.Entry<String, Object> entry : dsMap.entrySet()) {
            props.put("fastdfs." + entry.getKey(), entry.getValue());
        }
        props.putAll(dsMap);
        initFastDFS(props);
    }


    private void initFastDFS(Properties props) {
        try {
            // FastDFS 初始化
            ClientGlobal.initByProperties(props);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }

    /**
     * 上传文件
     *
     * @param file MultipartFile文件对象
     * @return 文件访问地址
     * @throws Exception
     */

//    public String uploadFile(MultipartFile file) throws Exception {
//        String result = storageClient.upload_file1(file.getBytes(), FilenameUtils.getExtension(file.getOriginalFilename()), null);
//        return result;
//    }

    /**
     * 上传文件
     *
     * @param file 文件对象
     * @return 文件访问地址
     * @throws Exception
     */

//    public String uploadFile(File file) throws Exception {
//        FileInputStream inputStream = new FileInputStream(file);
//        ByteArrayOutputStream bos = new ByteArrayOutputStream(1000);
//        byte[] b = new byte[1000];
//        int n;
//        while ((n = inputStream.read(b)) != -1) {
//            bos.write(b, 0, n);
//        }
//        byte[] buffer = bos.toByteArray();
//        inputStream.close();
//        bos.close();
//        String result = storageClient.upload_file1(buffer, FilenameUtils.getExtension(file.getName()), null);
//        return result;
//    }


    /**
     * @param fileContent byte[]
     * @param extName     文件类型 例如：jpg/png/txt
     * @return 上传文件的路径
     * @throws Exception
     */

    public String uploadFile(byte[] fileContent, String extName) throws Exception {
        TrackerClient trackerClient = new TrackerClient();
        TrackerServer trackerServer = trackerClient.getConnection();
        StorageServer storageServer = trackerClient.getStoreStorage(trackerServer);
        StorageClient1 storageClient = new StorageClient1(trackerServer, storageServer);
        String result = storageClient.upload_file1(fileContent, extName, null);
        storageServer.close();
        trackerServer.close();
        return result;
    }

    /**
     * @param inputStream 文件流
     * @param extName     文件类型 例如：jpg/png/txt
     * @return 上传文件的路径
     * @throws Exception
     */

//    public String uploadFile(InputStream inputStream, String extName) throws Exception {
//        ByteArrayOutputStream bos = new ByteArrayOutputStream(1000);
//        byte[] b = new byte[1000];
//        int n;
//        while ((n = inputStream.read(b)) != -1) {
//            bos.write(b, 0, n);
//        }
//        byte[] buffer = bos.toByteArray();
//        inputStream.close();
//        bos.close();
//        String result = storageClient.upload_file1(buffer, extName, null);
//        return result;
//    }

    /**
     * base64字符串生成一个文件上传
     *
     * @param content       文件内容
     * @param fileExtension 文件后缀
     * @return Exception e
     */

//    public String uploadFile(String content, String fileExtension) throws Exception {
//        byte[] buff = Base64Util.decodeBase64(content);
//        String result = storageClient.upload_file1(buff, fileExtension, null);
//        return result;
//    }


    /**
     * 删除上传文件
     *
     * @param fileUrl 上传得到的那一串字符串
     * @return 产出成功返回0，失败返回非0
     * @throws Exception e
     */

    public int deleteFile(String fileUrl) throws Exception {
        TrackerClient trackerClient = new TrackerClient();
        TrackerServer trackerServer = trackerClient.getConnection();
        StorageServer storageServer = trackerClient.getStoreStorage(trackerServer);
        StorageClient1 storageClient = new StorageClient1(trackerServer, storageServer);
        if (StringUtils.isEmpty(fileUrl)) {
            return -1;
        } else {
            return storageClient.delete_file1(fileUrl);

        }
    }

    /**
     * 下载文件
     * 将文件下载到本地文件中
     * @param file_id        文件上传时返回的字符串
     * @param local_filename 本地文件路径：全路径
     * @return 0成功，其他为失败
     * @throws Exception e
     */
//    public int downloadFile(String file_id, String local_filename) throws Exception {
//        return storageClient.download_file1(file_id, local_filename);
//    }

    /**
     * @param file_id 文件上传时返回的字符串
     * @return 文件的byte数组
     * @throws Exception e
     */
    public byte[] downloadFile(String file_id) throws Exception {
        TrackerClient trackerClient = new TrackerClient();
        TrackerServer trackerServer = trackerClient.getConnection();
        StorageServer storageServer = trackerClient.getStoreStorage(trackerServer);
        StorageClient1 storageClient = new StorageClient1(trackerServer, storageServer);
        return storageClient.download_file1(file_id);
    }

    /**
     * 更新文件
     *
     * @param fileUrl       上传得到的那一串字符串
     * @param fileContent   文件内容
     * @param fileExtension 文件后缀
     * @return 返回更新文件的新路径
     * @throws Exception e
     */
    public String updateFile(String fileUrl, byte[] fileContent, String fileExtension) throws Exception {
        String uploadFile = uploadFile(fileContent, fileExtension);
        deleteFile(fileUrl);
        return uploadFile;
    }

    /**
     * 更新文件
     * @param fileUrl 上传得到的那一串字符串
     * @param file 更新的文件
     * @return 返回更新文件的新路径
     * @throws Exception e
     */

//    public String updateFile(String fileUrl,File file) throws Exception {
//        deleteFile(fileUrl);
//        return uploadFile(file);
//    }

    /**
     * 更新文件
     * @param fileUrl 上传得到的那一串字符串
     * @param bytes 更新的文件
     * @param extName 文件类型
     * @return 返回更新文件的新路径
     * @throws Exception e
     */
//    public String updateFile(String fileUrl,byte[] bytes,String extName) throws Exception {
//        deleteFile(fileUrl);
//        return uploadFile(bytes,extName);
//    }
}
