package com.boot.proj.file.ffmpeg.video;


import com.boot.proj.commons.utils.MathRelateUtils;
import com.boot.proj.commons.utils.Result;
import com.boot.proj.commons.utils.StringUtils;
import com.boot.proj.file.config.FastDFSClient;
import com.boot.proj.file.ffmpeg.common.*;
import com.cloud.file.utils.FileUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @auther alan.chen
 * @time 2019/9/11 11:20 AM
 */
@Component
@Slf4j
public class VideoOperation {

    @Autowired
    private FastDFSClient fastDFSClient;

    /**
     * 视频时长正则匹配式
     * 用于解析视频及音频的时长等信息时使用；
     *
     * (.*?)表示：匹配任何除\r\n之外的任何0或多个字符，非贪婪模式
     *
     */
    private static String durationRegex = "Duration: (\\d*?):(\\d*?):(\\d*?)\\.(\\d*?), start: (.*?), bitrate: (\\d*) kb\\/s.*";
    private static Pattern durationPattern;

    /**
     * 视频流信息正则匹配式
     * 用于解析视频详细信息时使用；
     */
    private static String videoStreamRegex = "Stream #\\d:\\d[\\(]??\\S*[\\)]??: Video: (\\S*\\S$?)[^\\,]*, (.*?), (\\d*)x(\\d*)[^\\,]*, (\\d*) kb\\/s, (\\d*[\\.]??\\d*) fps";
    private static Pattern videoStreamPattern;
    /**
     * 音频流信息正则匹配式
     * 用于解析音频详细信息时使用；
     */
    private static String musicStreamRegex = "Stream #\\d:\\d[\\(]??\\S*[\\)]??: Audio: (\\S*\\S$?)(.*), (.*?) Hz, (.*?), (.*?), (\\d*) kb\\/s";;
    private static Pattern musicStreamPattern;

    /**
     * 静态初始化时先加载好用于音视频解析的正则匹配式
     */
    static {
        durationPattern = Pattern.compile(durationRegex);
        videoStreamPattern = Pattern.compile(videoStreamRegex);
        musicStreamPattern = Pattern.compile(musicStreamRegex);
    }

    /**
     *  ffmpeg文件路径
     */
    @Value("${ffmpeg.path}")
    private String ffmpegEXE;

//    public VideoOperation(String ffmpegEXE) {
//        this.ffmpegEXE = ffmpegEXE;
//    }
//
//    public static VideoOperation builder(String ffmpegEXE) {
//        return new VideoOperation(ffmpegEXE);
//    }


    /**
     *  视频转换格式
     *
     * @param file 原始视频绝对路径（带视频名称）
     * @param outputVideo  输出视频绝对路径（带视频名称）
     * @return result 返回执行code和message
     * @throws IOException
     */
    public Result videoConvert(File file, String outputVideo)  {
        //  ffmpeg -i input.mp4 -y out.mp4
        // ffmpeg -i in.mov -vcodec copy -acodec copy out.mp4  // mov --> mp4
        // ffmpeg -i in.flv -vcodec copy -acodec copy out.mp4
        try {
//            if(StringUtils.isBlank(inputVideo) || StringUtils.isBlank(outputVideo)) {
////                throw new FFMpegExceptionn("videoInputFullPath or videoOutFullPath must not be null");
////            }
            BaseFileUtil.checkAndMkdir(outputVideo);

            // 构建命令
            List<String> command = Lists.newArrayList();
            command.add(ffmpegEXE);
            command.add("-i");
//            command.add("-y");
            command.add(file.getAbsolutePath());
            command.add("-sameq"); // 表示保持同样的视频质量
//            command.add("-qscale");     //指定转换的质量
//            command.add("6");
//            command.add("-ab");	//设置音频码率
//            command.add("64");
//            command.add("-ac");	//设置声道数
//            command.add("2");
//            command.add("-ar");	//设置声音的采样频率
//            command.add("22050");
//            command.add("-r");	//设置帧频
//            command.add("24");
            command.add("-y"); // 添加参数＂-y＂，该参数指定将覆盖已存在的文件
            command.add(outputVideo+"\\1234.mp4");
            // 执行操作
            ProcessBuilder builder = new ProcessBuilder(command);
            Process process = builder.start();
            InputStream errorStream = process.getErrorStream();
            InputStreamReader isr = new InputStreamReader(errorStream);
            BufferedReader br = new BufferedReader(isr);
            String line = "";
            while ((line = br.readLine()) != null) {
            }
//            if (br != null) {
//                br.close();
//            }
//            if (isr != null) {
//                isr.close();
//            }
//            if (errorStream != null) {
//                errorStream.close();
//            }

//            List<String> commands = new ArrayList<String>();
//            commands.add(ffmpegEXE);
//
//            commands.add("-y");
//            commands.add("-i");
//            commands.add(file.getAbsolutePath());
//
//            commands.add("-vcodec");
//            commands.add("copy");
//            commands.add("-acodec");
//
//            //commands.add("-y");
//            commands.add("copy");
//            commands.add(outputVideo+"\\1234.mp4");
//
//            ProcessBuilder builder = new ProcessBuilder(commands).redirectErrorStream(true);
//            Process proc = builder.start();

            return StreamHanlerCommon.closeStreamQuietly(process);
        } catch (IOException e) {
            throw new FFMpegExceptionn(e.getMessage());
        }
    }

    /**
     * 保留视频原声合成音频
     *
     * @param bgm
     * @param inputVideo
     * @param outputVideo
     * @param seconds
     * @return
     */
    public Result mergeVideoAndBgm(String bgm, String inputVideo, String outputVideo, double seconds) {
//     保留原声合并音视频 ffmpeg -i bgm.mp3 -i input.mp4 -t 6 -filter_complex amix=inputs=2 output.mp4
        try {
            if(StringUtils.isBlank(bgm) || StringUtils.isBlank(inputVideo) || StringUtils.isBlank(outputVideo) || seconds <= 0) {
                throw new FFMpegExceptionn("请输入正确参数，参数不能为空");
            }
            BaseFileUtil.checkAndMkdir(outputVideo);

            List<String> commands = new ArrayList<>();
            commands.add(ffmpegEXE);

            commands.add("-i");
            commands.add(bgm);

            commands.add("-i");
            commands.add(inputVideo);

            commands.add("-t");
            commands.add(String.valueOf(seconds));

            commands.add("-filter_complex");
            commands.add("amix=inputs=2");

            commands.add("-y");
            commands.add(outputVideo);

            ProcessBuilder builder= new ProcessBuilder(commands);
            Process process = builder.start();

            return StreamHanlerCommon.closeStreamQuietly(process);
        } catch (IOException e) {
            throw new FFMpegExceptionn(e.getMessage());
        }
    }

    /**
     * 对视频进行截取，获取视频封面图
     *
     * @param file 原始视频
     * @param coverOut 图片输出路径
     * @return
     */
    public Result getVideoCoverImg(File file, String coverOut) {
//        ffmpeg -ss 00:00:01 -y -i input.mp4 -vframes 1 out.jpg


        try {
            if(file == null || StringUtils.isBlank(coverOut)) {
                throw new FFMpegExceptionn("请输入视频路径或封面图片输入参数");
            }
            String absolutePath = file.getAbsolutePath();
            System.err.println(absolutePath);
            BaseFileUtil.checkAndMkdir(coverOut);

            List<String> commands = new ArrayList<>();
//            commands.add(ffmpegEXE);

            commands.add("-ss");
            commands.add("00:00:01");

            commands.add("-y");
            commands.add("-i");
            commands.add(file.getAbsolutePath());

            commands.add("-vframes");
            commands.add("1");

            commands.add(coverOut);


            String s = FFMpegUtils.executeCommand(commands, ffmpegEXE);
            System.err.println("执行完成:"+s);

            return null;
        } catch (Exception e) {
            e.printStackTrace();
            throw new FFMpegExceptionn(e.getMessage());
        }
    }

    /**
     * 对视频的指定秒开始截图，可截多张图
     *
     * @param startSeconds 多少秒开始截图
     * @param inputVideo 需要截图的视频绝对路径
     * @param everySecondImg 每秒截多少张图
     * @param seconds 一共持续截图多少秒
     * @param coverOutPath 截图生成的路径（图片名称会以001 002... 命名）
     * @return
     */
    public Result getVideoCoverImgs(Integer startSeconds, String inputVideo,Integer everySecondImg, Integer seconds,String coverOutPath) {
//        ffmpeg -y -ss 0 -i 2222.mp4 -f image2 -r 1 -t 3 -q:a 1 ./%2d.jpg
        try {
            if(StringUtils.isBlank(inputVideo) || StringUtils.isBlank(coverOutPath) || everySecondImg <= 0 || startSeconds <= 0 || seconds <= 0) {
                throw new FFMpegExceptionn("请输入正确参数，参数不能为空");
            }
            BaseFileUtil.checkAndMkdir(coverOutPath);

            List<String> commands = new ArrayList<>();
            commands.add(ffmpegEXE);

            commands.add("-y");
            commands.add("-ss");
            commands.add(String.valueOf(startSeconds));

            commands.add("-i");
            commands.add(inputVideo);

            commands.add("-f");
            commands.add("image2");

            commands.add("-r");
            commands.add(String.valueOf(everySecondImg));

            commands.add("-t");
            commands.add(String.valueOf(seconds));

            commands.add("-q:a");
            commands.add("1");

            commands.add(coverOutPath + "/%3d.jpg");

            ProcessBuilder builder = new ProcessBuilder(commands);
            Process process = builder.start();

            return StreamHanlerCommon.closeStreamQuietly(process);
        } catch (IOException e) {
            throw new FFMpegExceptionn(e.getMessage());
        }
    }

    /**
     * 去除视频的音频
     *
     * @param inputVideo
     * @param outputVideo
     * @return
     */
    public Result wipeAudio(String inputVideo, String outputVideo) {
//       ffmpeg -y -i source.mp4 -an -vcodec copy output.mp4
        if(StringUtils.isBlank(inputVideo) || StringUtils.isBlank(outputVideo)) {
            throw new FFMpegExceptionn("请输入正确参数，参数不能为空");
        }
        BaseFileUtil.checkAndMkdir(outputVideo);

        try {
            List<String> commands = new ArrayList<>();
            commands.add(ffmpegEXE);

            commands.add("-y");
            commands.add("-i");
            commands.add(inputVideo);

            commands.add("-an");
            commands.add("-vcodec");

            commands.add("copy");
            commands.add(outputVideo);

            ProcessBuilder builder = new ProcessBuilder(commands);
            Process process = builder.start();

            return StreamHanlerCommon.closeStreamQuietly(process);
        } catch (IOException e) {
            throw new FFMpegExceptionn(e.getMessage());
        }
    }

    /**
     * 视频缩放
     *
     * @param inputVideo 需要操作的原始视频绝对路径
     * @param outWidth 处理之后的视频的宽度
     * @param outHeight 处理之后的视频高度
     * @param outputVideo 处理之后生成的新的视频绝对路径
     * @return
     */
    public Result videoScale(String inputVideo, String outWidth, String outHeight, String outputVideo) {
//       ffmpeg -y -i in.mp4 -vf scale=360:640 -acodec aac -vcodec h264 out.mp4

        if(StringUtils.isBlank(inputVideo) || StringUtils.isBlank(outputVideo)) {
            throw new FFMpegExceptionn("请输入正确参数，参数不能为空");
        }
        BaseFileUtil.checkAndMkdir(outputVideo);

        try {
            List<String> commands = new ArrayList<>();
            commands.add(ffmpegEXE);

            commands.add("-y");
            commands.add("-i");
            commands.add(inputVideo);

            commands.add("-vf");
            commands.add("scale="+ outWidth + ":" + outHeight);

            commands.add("-acodec");
            commands.add("aac");

            commands.add("-vcodec");
            commands.add("h264");

            commands.add(outputVideo);

            ProcessBuilder builder = new ProcessBuilder(commands);
            Process process = builder.start();

            return StreamHanlerCommon.closeStreamQuietly(process);
        } catch (IOException e) {
            throw new FFMpegExceptionn(e.getMessage());
        }
    }

    /**
     * 视频的页面长宽进行裁剪
     *
     * @param inputVideo 需要操作的原始视频绝对路径
     * @param outWidth 处理之后的视频的宽度
     * @param outHeight 处理之后的视频高度
     * @param outputVideo 处理之后生成的新的视频绝对路径
     * @return
     */
    public Result videoCrop(String inputVideo, String outWidth, String outHeight, String x, String y, String outputVideo) {
//       ffmpeg -y -i in.mp4 -strict -2 -vf crop=1080:1080:0:420 out.mp4
        if(StringUtils.isBlank(inputVideo) || StringUtils.isBlank(outputVideo)) {
            throw new FFMpegExceptionn("请输入正确参数，参数不能为空");
        }
        BaseFileUtil.checkAndMkdir(outputVideo);

        try {
            List<String> commands = new ArrayList<>();
            commands.add(ffmpegEXE);

            commands.add("-y");
            commands.add("-i");
            commands.add(inputVideo);

            commands.add("-strict");
            commands.add("-2");

            commands.add("-vf");
            commands.add("crop=" + outWidth + ":" + outHeight + ":" + x + ":" + y);

            commands.add(outputVideo);

            ProcessBuilder builder = new ProcessBuilder(commands);
            Process process = builder.start();

            return StreamHanlerCommon.closeStreamQuietly(process);
        } catch (IOException e) {
            throw new FFMpegExceptionn(e.getMessage());
        }
    }

    /**
     * 视频角度旋转
     *
     * @param inputVideo 需要操作的原始视频绝对路径
     * @param angleNum 旋转的角度，1：180度 2：90度
     * @param outWidth 输出视频的宽度，如果不指定，默认是输入视频的宽度
     * @param outHeight 输出视频的高度，如果不指定，默认是输入视频的高度
     * @param outputVideo 处理之后生成的新的视频绝对路径
     * @return
     */
    public Result videoRotate(String inputVideo, Integer angleNum, String outWidth, String outHeight, String outputVideo) {
//       ffmpeg -i in.mp4 -vf rotate=PI/2:ow=1080:oh=1920 out.mp4
        if(StringUtils.isBlank(inputVideo) || StringUtils.isBlank(outputVideo)) {
            throw new FFMpegExceptionn("请输入正确参数，参数不能为空");
        }
        BaseFileUtil.checkAndMkdir(outputVideo);

        if(angleNum != 1 && angleNum != 2) {
            throw new FFMpegExceptionn("非法参数，旋转角度需为-> 1：180deg or 2：90deg");
        }

        try {
            List<String> commands = new ArrayList<>();
            commands.add(ffmpegEXE);

            commands.add("-y");
            commands.add("-i");
            commands.add(inputVideo);

            commands.add("-vf");

            if(StringUtils.isNotBlank(outWidth) && StringUtils.isNotBlank(outHeight)) {
                commands.add("rotate=PI/"+ angleNum + ":ow=" + outWidth + ":oh" + outHeight);
            } else {
                commands.add("rotate=PI/"+ angleNum);
            }

            commands.add(outputVideo);

            ProcessBuilder builder = new ProcessBuilder(commands);
            Process process = builder.start();

            return StreamHanlerCommon.closeStreamQuietly(process);
        } catch (IOException e) {
            throw new FFMpegExceptionn(e.getMessage());
        }
    }

    /**
     * 调节视频帧数
     *
     * @param inputVideo 需要操作的原始视频绝对路径
     * @param fps 需要调节到多少帧
     * @param outputVideo 处理之后生成的新的视频绝对路径
     * @return
     */
    public Result videoFps(String inputVideo, Integer fps, String outputVideo) {
//      ffmpeg -y -i in.mp4 -r 15 out.mp4
        if(StringUtils.isBlank(inputVideo) || StringUtils.isBlank(outputVideo)) {
            throw new FFMpegExceptionn("请输入正确参数，参数不能为空");
        }
        BaseFileUtil.checkAndMkdir(outputVideo);

        try {
            List<String> commands = new ArrayList<>();
            commands.add(ffmpegEXE);

            commands.add("-y");
            commands.add("-i");
            commands.add(inputVideo);

            if(StringUtils.isNotBlank(outputVideo)) {
                commands.add("-r");
                commands.add(String.valueOf(fps));
            }

            commands.add(outputVideo);

            ProcessBuilder builder = new ProcessBuilder(commands);
            Process process = builder.start();

            return StreamHanlerCommon.closeStreamQuietly(process);
        } catch (IOException e) {
            throw new FFMpegExceptionn(e.getMessage());
        }
    }

    /**
     * gif转换为video
     *
     * @param gif gif绝对路径
     * @param outputVideo 输出视频绝对路径
     * @return
     */
    public Result gifConvertToVideo(String gif, String outputVideo) {
//      ffmpeg -i in.gif -vf scale=420:-2,format=yuv420p out.mp4  // gif --> mp4
        if(StringUtils.isBlank(gif) || StringUtils.isBlank(outputVideo)) {
            throw new FFMpegExceptionn("请输入正确参数，参数不能为空");
        }
        BaseFileUtil.checkAndMkdir(outputVideo);

        try {
            List<String> commands = new ArrayList<>();
            commands.add(ffmpegEXE);

            commands.add("-y");
            commands.add("-i");
            commands.add(gif);

            commands.add("-vf");

            commands.add("scale=420:-2,format=yuv420p");

            commands.add(outputVideo);

            ProcessBuilder builder = new ProcessBuilder(commands);
            Process process = builder.start();

            return StreamHanlerCommon.closeStreamQuietly(process);
        } catch (IOException e) {
            throw new FFMpegExceptionn(e.getMessage());
        }
    }

    /**
     * 视频转gif
     *
     * @param inputVideo 需操作视频的绝对路径
     * @param outputGif 生成gif的输出绝对路径
     * @param highQuality 是否生成高质量gif
     * @return
     */
    public Result videoConvertToGif(String inputVideo,  String outputGif, boolean highQuality) {
//      ffmpeg -i src.mp4 -b 2048k des.gif
//      ffmpeg -i src.mp4 des.gif
        if(StringUtils.isBlank(inputVideo) || StringUtils.isBlank(outputGif)) {
            throw new FFMpegExceptionn("请输入正确参数，参数不能为空");
        }
        BaseFileUtil.checkAndMkdir(outputGif);
        try {
            BaseFileUtil.checkAndMkdir(outputGif);

            List<String> commands = new ArrayList<>();
            commands.add(ffmpegEXE);

            commands.add("-y");
            commands.add("-i");
            commands.add(inputVideo);

            if(highQuality) {
                commands.add("-b");
                commands.add("2048k");
            }

            commands.add(outputGif);

            ProcessBuilder builder = new ProcessBuilder(commands);
            Process process = builder.start();

            return StreamHanlerCommon.closeStreamQuietly(process);
        } catch (IOException e) {
            throw new FFMpegExceptionn(e.getMessage());
        }
    }


    /**
     * 对视频的播放时间进行裁剪
     *
     * @param inputVideo 原始需要操作视频的绝对路径
     * @param startTime 开始裁剪的时间 支持格式： 2  或  00:00:02 从2秒开始
     * @param seconds  剪裁持续的时间 支持格式： 3 或 00:00:03 持续3秒
     * @param outputVideo 输出视频的绝对路径
     * @return
     */
    public Result videoCut(String inputVideo,String startTime, String seconds, String outputVideo) {
//      ffmpeg -ss 10 -t 15 -accurate_seek -i test.mp4 -codec copy -avoid_negative_ts 1 cut.mp4
//      ffmpeg -i src.mp4  -ss 00:00:00 -t 00:00:20 des.mp4
        if(StringUtils.isBlank(inputVideo) || StringUtils.isBlank(startTime) || StringUtils.isBlank(seconds) || StringUtils.isBlank(outputVideo)) {
            throw new FFMpegExceptionn("请输入正确参数，参数不能为空");
        }
        BaseFileUtil.checkAndMkdir(outputVideo);

        try {
            List<String> commands = new ArrayList<>();
            commands.add(ffmpegEXE);

            commands.add("-y");
            commands.add("-ss");
            commands.add(startTime);

            commands.add("-t");
            commands.add(seconds);

            commands.add("-accurate_seek");

            commands.add("-i");
            commands.add(inputVideo);

            commands.add("-codec");
            commands.add("-copy");
            commands.add("-avoid_negative_ts");
            commands.add("1");

            commands.add(outputVideo);

            ProcessBuilder builder = new ProcessBuilder(commands);
            Process process = builder.start();

            return StreamHanlerCommon.closeStreamQuietly(process);
        } catch (IOException e) {
            throw new FFMpegExceptionn(e.getMessage());
        }
    }

    /**
     * 获取视频信息
     * ffmpeg 命令： ffmpeg -i video.mp4
     * @param file
     * @return
     */
    public Result getVideoInfo(File file) throws IOException {
        if (null == file || !file.exists()) {
            log.error("--- 解析视频信息失败，因为要解析的源视频文件不存在 ---");
            return null;
        }

        String parseResult = this.getMetaInfoFromFFmpeg(file);

        Matcher durationMacher = durationPattern.matcher(parseResult);
        Matcher videoStreamMacher = videoStreamPattern.matcher(parseResult);
        Matcher videoMusicStreamMacher = musicStreamPattern.matcher(parseResult);

        Long duration = 0L; // 视频时长
        Integer videoBitrate = 0; // 视频码率
        String videoFormat = this.getFormat(file); // 视频格式
        Long videoSize = file.length(); // 视频大小

        String videoEncoder = ""; // 视频编码器
        Integer videoHeight = 0; // 视频高度
        Integer videoWidth = 0; // 视频宽度
        Float videoFramerate = 0F; // 视频帧率

        String musicFormat = ""; // 音频格式
        Long samplerate = 0L; // 音频采样率
        Integer musicBitrate = 0; // 音频码率

        try {
            // 匹配视频播放时长等信息
            if (durationMacher.find()) {
                long hours = (long)Integer.parseInt(durationMacher.group(1));
                long minutes = (long)Integer.parseInt(durationMacher.group(2));
                long seconds = (long)Integer.parseInt(durationMacher.group(3));
                long dec = (long)Integer.parseInt(durationMacher.group(4));
                duration = dec * 100L + seconds * 1000L + minutes * 60L * 1000L + hours * 60L * 60L * 1000L;
                //String startTime = durationMacher.group(5) + "ms";
                videoBitrate = Integer.parseInt(durationMacher.group(6));
            }
            // 匹配视频分辨率等信息
            if (videoStreamMacher.find()) {
                videoEncoder = videoStreamMacher.group(1);
                String s2 = videoStreamMacher.group(2);
                videoWidth = Integer.parseInt(videoStreamMacher.group(3));
                videoHeight = Integer.parseInt(videoStreamMacher.group(4));
                String s5 = videoStreamMacher.group(5);
                videoFramerate = Float.parseFloat(videoStreamMacher.group(6));
            }
            // 匹配视频中的音频信息
            if (videoMusicStreamMacher.find()) {
                musicFormat = videoMusicStreamMacher.group(1); // 提取音频格式
                //String s2 = videoMusicStreamMacher.group(2);
                samplerate = Long.parseLong(videoMusicStreamMacher.group(3)); // 提取采样率
                //String s4 = videoMusicStreamMacher.group(4);
                //String s5 = videoMusicStreamMacher.group(5);
                musicBitrate = Integer.parseInt(videoMusicStreamMacher.group(6)); // 提取比特率
            }
        } catch (Exception e) {
            log.error("--- 解析视频参数信息出错！ --- 错误信息： " + e.getMessage());
            return null;
        }

        // 封装视频中的音频信息
        MusicMetaInfo musicMetaInfo = new MusicMetaInfo();
        musicMetaInfo.setFormat(musicFormat);
        musicMetaInfo.setDuration(duration);
        musicMetaInfo.setBitRate(musicBitrate);
        musicMetaInfo.setSampleRate(samplerate);
        // 封装视频信息
        VideoMetaInfo videoMetaInfo = new VideoMetaInfo();
        videoMetaInfo.setFormat(videoFormat);
        videoMetaInfo.setSize(videoSize);
        videoMetaInfo.setBitRate(videoBitrate);
        videoMetaInfo.setDuration(duration);
        videoMetaInfo.setEncoder(videoEncoder);
        videoMetaInfo.setFrameRate(videoFramerate);
        videoMetaInfo.setHeight(videoHeight);
        videoMetaInfo.setWidth(videoWidth);
        if (videoWidth !=0 && videoHeight !=0) {
            int i = MathRelateUtils.maxCommonDivisor(videoWidth, videoHeight);
            int i1 = videoWidth / i;
            int i2 = videoHeight / i;
            videoMetaInfo.setVideoRatio(i1+":"+i2);
        }

        videoMetaInfo.setMusicMetaInfo(musicMetaInfo);

        return new Result(Result.CODE_SUCCESS,"",videoMetaInfo);



//        if (file == null || !file.exists()) {
//            throw new RuntimeException("源媒体文件不存在，源媒体文件路径： ");
//        }
//        List<String> commond = new ArrayList<String>();
//        commond.add(ffmpegEXE);
//        commond.add("-i");
//        commond.add(file.getAbsolutePath());
//        ProcessBuilder builder = new ProcessBuilder(commond);
//        Process process = builder.start();
//        Result result = StreamHanlerCommon.closeStreamQuietly(process);
    }

    public String videoToTs(File file,String temp) throws Exception {

        // ffmpeg -y -i C:\Users\suxl5\Desktop\conclusion.mp4 -hls_time 10 -hls_list_size 0 -hls_playlist_type vod -hls_segment_filename D:\workspace\ffm\file%d.ts D:\workspace\ffm\playlist.m3u8
        FileUtils.mkDir(temp);
        List<String> commands = new ArrayList<>();
        commands.add("-i");
        commands.add(file.getAbsolutePath());
        commands.add("-c:v");
        commands.add("copy");
        commands.add("-c:a");
        commands.add("copy");
        commands.add("-f");
        commands.add("hls");
        commands.add("-hls_time"); //指定生成 ts 视频切片的时间长度s
        commands.add("5");
        commands.add("-hls_key_info_file");
        commands.add("C:\\temp\\enc.keyinfo");
        commands.add("-hls_list_size"); //索引播放列表的最大列数 默认5，0 为不限制
        commands.add("10");
        commands.add("-hls_playlist_type"); // -hls_playlist_type vod表示当前的视频流并不是一个直播流，而是点播流
        commands.add("vod");
        commands.add("-hls_segment_filename"); //输出 ts m3u8 文件路径
        commands.add(temp+"file%d.ts");
        commands.add(temp+"playlist.m3u8");
        String s = FFMpegUtils.executeCommand(commands, ffmpegEXE);

        List<File> listFiles = FileUtils.getListFiles(temp);

        ExecutorService executorService = Executors.newFixedThreadPool(1);
        System.err.println(listFiles);
        List<Future<String>> resultList = new ArrayList<Future<String>>();
        long l = System.currentTimeMillis();
        for (File listFile : listFiles) {
            String fileName = listFile.getName();
            String suffix = fileName.substring(fileName.lastIndexOf(".") + 1);
            if (suffix.equals("ts")) {
                Callable<String> callable = new Callable<String>() {
                    @Override
                    public String call() throws Exception {
                        String s = fastDFSClient.uploadFile(IOUtils.toByteArray(new FileInputStream(listFile)), FilenameUtils.getExtension(listFile.getName()));
                        return listFile.getName()+":"+s;
                    }
                };
                Future<String> submit = executorService.submit(callable);
                resultList.add(submit);
            }
        }


        int i=1;
        //遍历任务的结果
        HashMap<String, String> map = Maps.newHashMap();
        for (Future<String> fs : resultList) {
            try {
                while (!fs.isDone()) ;//Future返回如果没有完成，则一直循环等待，直到Future返回完成
                System.out.println("上传完成返回结果:"+i+"--"+fs.get());     //打印各个线程（任务）执行的结果

                String s1 = fs.get();
                String[] spl = s1.split(":");
//                String[] split = spl[1].split("/");
                map.put(spl[0],spl[1]);
                i++;
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            } finally {
                //启动一次顺序关闭，执行以前提交的任务，但不接受新任务

            }
        }

        System.err.println(map);

        executorService.shutdown();

        File file1 = updateM3u8(map, temp, "playlist.m3u8");
        String n = fastDFSClient.uploadFile(IOUtils.toByteArray(new FileInputStream(file1)), FilenameUtils.getExtension(file1.getName()));
        System.err.println("m3u8上传路径:"+n);
        long l1 = System.currentTimeMillis();
        System.err.println("耗时："+(l1-l));


        return s;
    }

    private File updateM3u8(HashMap<String, String> map, String temp, String s) throws IOException {
        File file = new File(temp+s);
        System.out.println("文件绝对路径 :"+file.getAbsolutePath());
        List<String> listStr = new ArrayList<String>();
        BufferedReader br = null;
        String str = null;
        try {
            br = new BufferedReader(new FileReader(file));
            while ((str = br.readLine())!= null) {
                if (map.containsKey(str)) {
                    str = map.get(str);
                }
                listStr.add(str);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            br.close();
        }
        System.out.println(listStr);


        File file1 = new File(temp+"playlistNew.m3u8");// 要写入的文件路径
        if (!file1.exists()) {// 判断文件是否存在
            try {
                file1.createNewFile();// 如果文件不存在创建文件
                System.out.println("文件"+file1.getName()+"不存在已为您创建!");
            } catch (IOException e) {
                System.out.println("创建文件异常!");
                e.printStackTrace();
            }
        } else {
            System.out.println("文件"+file1.getName()+"已存在!");
        }

        for (String str1 : listStr) {// 遍历listStr集合
            FileOutputStream fos = null;
            PrintStream ps = null;
            try {
                fos = new FileOutputStream(file1,true);// 文件输出流	追加
                ps = new PrintStream(fos);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            String string  = str1 + "\r\n";// +换行
            ps.print(string); // 执行写操作
            ps.close();	// 关闭流

        }

        return file1;
    }


    /**
     * 使用FFmpeg的"-i"命令来解析视频信息
     * @param inputFile 源媒体文件
     * @return 解析后的结果字符串，解析失败时为空
     */
    public String getMetaInfoFromFFmpeg(File inputFile) {
        if (inputFile == null || !inputFile.exists()) {
            throw new RuntimeException("源媒体文件不存在，源媒体文件路径： ");
        }
        List<String> commond = new ArrayList<String>();
        commond.add("-i");
        commond.add(inputFile.getAbsolutePath());
        String executeResult = FFMpegUtils.executeCommand(commond,ffmpegEXE);
        return executeResult;
    }

    /**
     * 获取指定文件的后缀名
     * @param file
     * @return
     */
    private String getFormat(File file) {
        String fileName = file.getName();
        String format = fileName.substring(fileName.indexOf(".") + 1);
        return format;
    }



}
