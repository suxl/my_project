<template>
    <div class="page-container">
        <!--工具栏-->
        <div class="toolbar" style="float:left;padding-top:10px;padding-left:15px;">
            <el-form :inline="true" :model="listQuery" :size="size">
                <el-form-item>
                    <el-input v-model="listQuery.jobNameCh" placeholder="任务中文名"></el-input>
                </el-form-item>
                <el-form-item>
                    <kt-button icon="fa fa-search" :label="$t('action.search')"
                               perms="${table.name?replace('_', ':')}:view" type="primary" @click="findPage(null)"/>
                </el-form-item>
                <el-form-item>
                    <kt-button icon="fa fa-plus" :label="$t('action.add')" perms="${table.name?replace('_', ':')}:add"
                               type="primary" @click="handleAdd"/>
                </el-form-item>
            </el-form>
        </div>

        <div class="toolbar" style="float:right;padding-top:10px;padding-right:15px;">
            <el-form :inline="true" :size="size">
                <el-form-item>
                    <el-button-group>
                        <el-tooltip content="刷新" placement="top">
                            <el-button icon="fa fa-refresh" @click="findPage(null)"></el-button>
                        </el-tooltip>
                        <el-tooltip content="导出" placement="top">
                            <el-button icon="fa fa-file-excel-o"></el-button>
                        </el-tooltip>
                    </el-button-group>
                </el-form-item>
            </el-form>
        </div>

        <div>
            <!--表格栏-->
            <el-table :data="tableData" :highlight-current-row="highlightCurrentRow" @selection-change="selectionChange"
                      @current-change="handleCurrentChange" v-loading="loading"
                      :element-loading-text="$t('action.loading')" :border="border" :stripe="stripe"
                      :show-overflow-tooltip="showOverflowTooltip" :max-height="maxHeight" :height="height" :size="size"
                      :align="align" style="width:100%;"
                      :row-style="rowStyle" :cell-style="cellStyle">

                <el-table-column
                    type="selection"
                    width="40"
                    v-if="showBatchDelete & showOperation">
                </el-table-column>
				<#list table.fields as field>
                <el-table-column
                    prop="${field.propertyName}"
                    label="${field.comment}"
                    align="center">
                </el-table-column>
                </#list>

                <el-table-column
                    min-width="110px"
                    label="操作">
                    <template slot-scope="scope">
                        <kt-button icon="fa fa-edit"
                               :label="$t('action.edit')"
                               perms="${table.name?replace('_', ':')}:edit"
                               :size="size"
                               @click="handleEdit(scope)"/>
                        <kt-button icon="fa fa-trash"
                               :label="$t('action.delete')"
                               perms="${table.name?replace('_', ':')}:delete"
                               :size="size"
                               type="danger"
                               @click="preHandleDelete(scope)"/>
                    </template>
                </el-table-column>
            </el-table>

            <!--新增编辑界面-->
            <transition name='fade'
                        enter-active-class='animated slideInRight'
                        leave-active-class='animated slideOutRight'>
                <div v-if='maskShow' class="add-form">

                    <div slot="header" style="padding: 20px 0px 40px 0px;border-bottom: 1px solid #efefef">
                        <div style="float: left;padding-left: 20px;">
                            <span style="font-family: Consolas;font-weight: bold;font-size: large">{{title}}</span>
                        </div>
                        <div slot="footer" style="float: right;padding-right: 15px;">
                            <el-button :size="size" @click.native="handleCancle">{{$t('action.cancel')}}</el-button>
                            <el-button :size="size" type="primary" @click.native="submitForm">{{$t('action.submit')}}</el-button>
                        </div>
                    </div>

                    <div class="add-content" style="padding: 20px 20px 10px 20px;height: 80%;overflow-y:auto">
                        <el-form :model="dataForm" label-width="80px" :rules="dataFormRules" ref="dataForm" :size="size"
                                 label-position="right">
					<#list table.fields as fields>
                        <el-form-item label="${fields.comment}" prop="${fields.propertyName}">
                            <el-input v-model="dataForm.${fields.propertyName}" auto-complete="off"></el-input>
                        </el-form-item>
                    </#list>
                        </el-form>
                    </div>
                </div>
            </transition>

            <!--新增编辑界面-->
            <!--分页栏-->
            <div class="toolbar" style="padding:10px;">
                <kt-button :label="$t('action.batchDelete')" perms="${table.name?replace('_', ':')}:delete" :size="size"
                           type="danger" @click="preHandleDelete(null)"
                           :disabled="this.selections.length===0" style="float:left;"
                           v-if="showBatchDelete & showOperation"/>
                <pagination v-show="total>0" :total="total" :page.sync="listQuery.page" :limit.sync="listQuery.size"
                            style="float:right;" @pagination="findPage"/>
            </div>
        </div>
    </div>

</template>

<script>
    import KtButton from "@/views/Core/KtButton"
    import Pagination from '@/components/Pagination' // secondary package based on el-pagination
    export default {
        components: {
            KtButton, Pagination
        },
        data() {
            return {
                //  表格样式 开始
                loading: false,  // 加载标识
                size: 'mini',
                align: 'left',
                maxHeight: 420,
                height: 350,
                border: false,
                stripe: true,
                highlightCurrentRow: true,
                showOverflowTooltip: true,
                showOperation: true,
                showBatchDelete: true,
                //  表格样式 结束
                tableData: null,
                maskShow: false,
                title: '',
                selections: [],  // 列表选中列
                dataFormRules: {
                    <#list table.fields as fields>
                        ${fields.propertyName}: [
                        {required: true, message: '请输入${fields.comment}', trigger: 'blur'}
                    ]<#if fields_has_next>,</#if>
                    </#list>
                },
                total: 1,
                listQuery:{
                    page: 1,
                    size: 20,
                    orderBy:
                    [{
                        name: 'updateDate',
                        sort: 'desc'
                    }],
                    label: ''
                },
                changeData: {
                    id: null,
                    state: null
                },
                dataForm: {
                    <#list table.commonFields as field>
                        ${field.propertyName}: null,
                    </#list><#list table.fields as fields>
                        ${fields.propertyName}: null<#if fields_has_next>,</#if>
                    </#list>
                }
        }
        },
        methods: {
            formatter(row, column) {
                return row.address;
            },
            submitForm: function () {
                this.$refs.dataForm.validate((valid) => {
                    if (valid) {
                        this.$confirm('确认提交吗？', '提示', {}).then(() => {
                            // this.editLoading = true
                            let params = Object.assign({}, this.dataForm)
                            this.$api.quartz.save(params).then((res) => {
                                // this.editLoading = false
                                if (res.code == 1) {
                                    this.$message({message: '操作成功', type: 'success'})
                                    this.$refs['dataForm'].resetFields()
                                    this.handleCancle()
                                } else {
                                    this.$message({message: '操作失败, ' + res.msg, type: 'error'})
                                }
                                this.findPage(null)
                            })
                        })
                    }
                })
            },
            //弹框关闭
            handleColse: function(formName) {
                //清除校验
                this.$refs[formName].clearValidate();
            },
            // 获取分页数据
            findPage: function () {
                this.loading = true
                this.$api.quartz.findList(this.listQuery).then((res) => {
                    console.log(res.data)
                    this.tableData = res.data
                    this.total = res.count
                    this.loading = false
                })
            },
            // 显示新增界面
            handleAdd: function () {
                this.title = '新增'
                this.maskShow = true;
                document.getElementById('shade').style.display = 'block';
                this.dataForm = {}
            },
            // 显示编辑界面
            handleEdit: function (params) {
                this.title = '修改'
                this.maskShow = true;
                document.getElementById('shade').style.display = 'block';
                this.dataForm = Object.assign({}, params.row)
            },
            handleCancle: function () {
                this.maskShow = false;
                document.getElementById('shade').style.display = 'none';
            },
            //删除前处理
            preHandleDelete: function (data) {
                let params = []
                if (data != null) {
                    params.push(data.row.id)
                }

                if (this.selections.length > 0) {
                    let ids = this.selections.map(item => item.id).toString()
                    let idArray = (ids + '').split(',')
                    for (var i = 0; i < idArray.length; i++) {
                        params.push(idArray[i])
                    }
                }
                this.handleDelete(params)
            },
            // 批量删除
            handleDelete: function (params) {
                this.$confirm('确认删除选中记录吗？', '提示', {
                    type: 'warning'
                }).then(() => {
                    this.loading = true
                    this.$api.quartz.batchDelete(params).then(res => {
                        if (res.code == 1) {
                            this.$message({message: '删除成功', type: 'success'})
                            this.findPage()
                            this.loading = false
                        } else {
                            this.$message({message: '操作失败, ' + res.msg, type: 'error'})
                        }
                    })
                }).catch(() => {
                })
            },
            // 选择切换
            selectionChange: function (selections) {
                console.log(selections)
                this.selections = selections
            },
            // 选择切换
            handleCurrentChange: function (val) {
            },
            rowStyle({row, rowIndex}) {
                return 'height:41px;'
            },
            cellStyle({row, rowIndex}) {
                return 'padding:0'
            }
        },
        mounted() {
            // this.findDeptTree()
            this.findPage()
        }
    }

</script>

<style scoped>
    .add-form {
        position: absolute;
        top: 0;
        right: 0;
        bottom: 10px;
        z-index: 2000;
        width: 40%;
        height: 100%;
        background-color: white;
    }
</style>