package ${package.Service};

import ${package.Entity}.${entity};
import ${package.Mapper}.${table.mapperName};
import ${package.Service}.${table.serviceName};
import ${superServiceClassPackage};
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import java.io.Serializable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
/**
 * <p>
 * $!{table.comment} 服务实现类
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
@Service
@Transactional(rollbackFor = Exception.class)
@CacheConfig(cacheNames = "${entity}")
<#if kotlin>
open class ${table.serviceImplName} : ${superServiceImplClass}<${table.mapperName}, ${entity}>(), ${table.serviceName} {

}
<#else>
public class ${table.serviceName} extends ${superServiceClass}<${table.mapperName}, ${entity}>{
    @Autowired
    private ${table.mapperName} dao;

    @Override
    @CacheEvict(cacheNames="${entity}", allEntries=true)
    public boolean save(${entity} entity) {
        return super.save(entity);
    }

    @Override
    @Cacheable(key = "#id",sync = true)
    public ${entity} getById(Serializable id) {
        return super.getById(id);
    }

    @Override
    @CacheEvict(cacheNames="${entity}", allEntries=true)
    public boolean removeById(Serializable id) {
        return super.removeById(id);
    }

    @CacheEvict(cacheNames="SysDict", allEntries=true)
    @Override
    public boolean removeByIds(Collection<? extends Serializable> idList) {
        return super.removeByIds(idList);
    }

    @Cacheable(key = "#data",sync = true)
    public IPage<${entity}> selectPage(Map<String,Object> data) {
        int currentPage=1;
        if (!Objects.isNull(data.get("page"))) {
            currentPage=(Integer) data.get("page");
        }
        int pageSize=20;
        if (!Objects.isNull(data.get("size"))) {
            pageSize=(Integer) data.get("size");
        }

        ${entity} entity = JSON.parseObject(JSON.toJSONString(data), ${entity}.class);
        Page<${entity}> page = new Page(currentPage,pageSize);
        QueryWrapper<${entity}> wrapper = new QueryWrapper(entity);
        return super.selectPage(page, wrapper);
    }

    @Override
    @Cacheable(value = "${entity}",sync = true)
    public List<${entity}> selectAll() {
        return super.selectAll();
    }


}
</#if>
