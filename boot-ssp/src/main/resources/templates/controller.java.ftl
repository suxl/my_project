package ${package.Controller};

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.beans.factory.annotation.Autowired;
import io.swagger.annotations.ApiOperation;
import com.baomidou.mybatisplus.core.metadata.IPage;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;
import com.cloud.commons.utils.Result;
import org.springframework.security.access.prepost.PreAuthorize;
import com.cloud.commons.utils.DateUtils;
import com.cloud.commons.utils.excel.ExportExcel;

<#if restControllerStyle>
import org.springframework.web.bind.annotation.RestController;
<#else>
import org.springframework.stereotype.Controller;
</#if>
<#if superControllerClassPackage??>
import ${superControllerClassPackage};
</#if>


import ${package.Service}.${table.serviceName};
import ${package.Entity}.${entity};

/**
 * <p>
 * ${table.comment} 前端控制器
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
<#if restControllerStyle>
@RestController
<#else>
@Controller
</#if>
@RequestMapping("<#if package.ModuleName??>/${package.ModuleName}</#if>/<#if controllerMappingHyphenStyle??>${controllerMappingHyphen}<#else>${table.entityPath}</#if>")
<#if kotlin>
class ${table.controllerName}<#if superControllerClass??> : ${superControllerClass}()</#if>
<#else>
	<#if superControllerClass??>
public class ${table.controllerName} extends ${superControllerClass} {
	<#else>
public class ${table.controllerName} {
</#if>

	@Autowired
    public ${table.serviceName} service;

	@ApiImplicitParam(name = "data", value = "${entity}属性:{\"id\": \"\"...}", required = true)
	@ApiOperation(value = "${table.comment},保存/修改", notes = "${table.comment},保存和修改方法")
	@RequestMapping(value = "/save", method = RequestMethod.POST)
    @PreAuthorize("hasAuthority('${table.name?replace('_', ':')}:edit')")
	public Result save(@RequestBody ${entity} data){
    	try {
    		service.save(data);
        	return new Result<>(Result.CODE_SUCCESS,"保存成功");
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
        	return new Result<>(Result.CODE_FAILED,"保存失败");
		}

	}


	@ApiOperation(value = "${table.comment}删除", notes = "通过id删除${table.comment}信息。")
	@RequestMapping(value = "/delete", method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('${table.name?replace('_', ':')}:delete')")
	public Result delete(@RequestParam(value="id") String id) {
		try {
			service.removeById(id);
        	return new Result<>(Result.CODE_SUCCESS,"删除成功");
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
            return new Result<>(Result.CODE_FAILED,"删除失败");
		}

	}

	@ApiImplicitParam(name = "data", value = "需要删除的id集合:[\"id1\",\"id2\"...]", required = true)
	@ApiOperation(value = "批量删除", notes = "批量删除。")
	@RequestMapping(value = "/batchDelete", method = RequestMethod.POST)
	public Result batchDelete(@RequestBody List<String> list) {
		try {
			service.removeByIds(list);
			return new Result<>(Result.CODE_SUCCESS,"删除成功");
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
			return new Result<>(Result.CODE_FAILED,"删除失败");
		}
    }

    @ApiImplicitParam(name = "data", value = "{\"page\": 1,\"size\": 5,\"id\":\"\"...}", required = true)
	@ApiOperation(value = "分页查询${table.comment}list", notes = "分页查询${table.comment}list")
	@RequestMapping(value="/findList",method = RequestMethod.POST)
    @PreAuthorize("hasAuthority('${table.name?replace('_', ':')}:view')")
	public Result findList(@RequestBody Map<String,Object> data) {
        try {
        	IPage<${entity}> selectPage = service.selectPage(data);
        	return new Result<>(Result.CODE_SUCCESS,"",selectPage.getRecords(),selectPage.getTotal());
        } catch (Exception e) {
        	e.printStackTrace();
        	return new Result<>(Result.CODE_FAILED,"查询异常",e.getMessage());
        }
	}

	@ApiOperation(value = "查询${table.comment}所有数据list", notes = "查询${table.comment}所有数据list。")
	@RequestMapping(value="/findAll",method = RequestMethod.POST)
    @PreAuthorize("hasAuthority('${table.name?replace('_', ':')}:view')")
	public Result findAll(){
        return new Result<>(Result.CODE_SUCCESS,"",service.selectAll());
	}


	@ApiOperation(value = "通过id获取${table.comment}实体类", notes = "通过id获取${table.comment}实体类。")
	@RequestMapping(value="/getById",method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('${table.name?replace('_', ':')}:view')")
	public Result getById(@RequestParam(value="id") String id) {
        return new Result<>(Result.CODE_SUCCESS,"",service.getById(id));
	}

	@ApiOperation(value = "通过id获取实体类详细信息", notes = "通过id获取实体类详细信息。")
	@RequestMapping(value="/detail",method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('${table.name?replace('_', ':')}:view')")
	public Result detail(@RequestParam(value="id") String id) {
        return new Result<>(Result.CODE_SUCCESS,"",service.getById(id));
	}


	@ApiOperation(value = "导出${table.comment}excel", notes = "导出${table.comment}excel。")
	@RequestMapping(value="/export",method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('${table.name?replace('_', ':')}:export')")
	public String export(HttpServletRequest request, HttpServletResponse response){
        try {
        String fileName = "${table.comment}" + DateUtils.formatDateTime(new Date()) + ".xlsx";

        List<${entity}> list=service.selectAll();
        ExportExcel exportExcel =new ExportExcel("${table.comment}", ${entity}.class);
        exportExcel.setDatePattern("yyyy-MM-dd HH:mm:ss");
        exportExcel.setDataList(list).write(response, fileName).dispose();

        return null;
        } catch (Exception e) {
        e.printStackTrace();
        }
        return null;
	}


}

</#if>